
#pragma once

#include "stdafx.h"
#include "Virtual1553B.h"


#define VIRTUAL1553B_VALIDATE_BUFFER_LENGTH(Length, BufferSize, Irp, Message)   \
{                                                                               \
    if ((Length) < (BufferSize))                                                \
    {                                                                           \
        VIRTUAL1553B_COMPLETE_RETURN_0(                                         \
        (Irp), STATUS_BUFFER_TOO_SMALL, 0, Message);                            \
    }                                                                           \
}

#define VIRTUAL1553B_VALIDATE_INPUT_BUFFER_LENGTH(Length, BufferSize, Irp)  \
    VIRTUAL1553B_VALIDATE_BUFFER_LENGTH((Length), (BufferSize), (Irp),      \
    "Insufficient input buffer length.")

#define VIRTUAL1553B_VALIDATE_OUTPUT_BUFFER_LENGTH(Length, BufferSize, Irp) \
    VIRTUAL1553B_VALIDATE_BUFFER_LENGTH((Length), (BufferSize), (Irp),      \
    "Insufficient output buffer length.")


#define VIRTUAL1553B_RETURN_STATUS_CODE(OutBuffer, BufferSize, Irp, StatusCode, Message)    \
{                                                                                           \
    (OutBuffer)->Status = (StatusCode);                                                     \
    VIRTUAL1553B_COMPLETE_RETURN_0(                                                         \
    (Irp), STATUS_SUCCESS, (BufferSize), "[Status] " Message);                              \
}

#define VIRTUAL1553B_SUCCESS_RETURN(OutBuffer, BufferSize, Irp)         \
    VIRTUAL1553B_RETURN_STATUS_CODE((OutBuffer), (BufferSize), (Irp),   \
    VIRTUAL1553B_STATUS_SUCCESS,                                        \
    "Success.")

#define VIRTUAL1553B_NO_FREE_TERMINALS_RETURN(OutBuffer, BufferSize, Irp)   \
    VIRTUAL1553B_RETURN_STATUS_CODE((OutBuffer), (BufferSize), (Irp),       \
    VIRTUAL1553B_STATUS_NO_FREE_TERMINALS,                                  \
    "No free terminals.")

#define VIRTUAL1553B_INVALID_TERMINAL_HANDLE_RETURN(OutBuffer, BufferSize, Irp) \
    VIRTUAL1553B_RETURN_STATUS_CODE((OutBuffer), (BufferSize), (Irp),           \
    VIRTUAL1553B_STATUS_INVALID_TERMINAL_HANDLE,                                \
    "Invalid terminal handle.")

#define VIRTUAL1553B_PROCESS_IS_NOT_OWNER_RETURN(OutBuffer, BufferSize, Irp)    \
    VIRTUAL1553B_RETURN_STATUS_CODE((OutBuffer), (BufferSize), (Irp),           \
    VIRTUAL1553B_STATUS_PROCESS_IS_NOT_OWNER,                                   \
    "Process is not owner.")

#define VIRTUAL1553B_TERMINAL_IS_NOT_STOPPED_RETURN(OutBuffer, BufferSize, Irp) \
    VIRTUAL1553B_RETURN_STATUS_CODE((OutBuffer), (BufferSize), (Irp),           \
    VIRTUAL1553B_STATUS_TERMINAL_IS_NOT_STOPPED,                                \
    "Terminal is not stopped.")

#define VIRTUAL1553B_EVENT_HANDLE_ACCESS_DENIED_RETURN(OutBuffer, BufferSize, Irp)  \
    VIRTUAL1553B_RETURN_STATUS_CODE((OutBuffer), (BufferSize), (Irp),               \
    VIRTUAL1553B_STATUS_EVENT_HANDLE_ACCESS_DENIED,                                 \
    "Event handle access denied.")

#define VIRTUAL1553B_INVALID_EVENT_HANDLE_RETURN(OutBuffer, BufferSize, Irp)    \
    VIRTUAL1553B_RETURN_STATUS_CODE((OutBuffer), (BufferSize), (Irp),           \
    VIRTUAL1553B_STATUS_INVALID_EVENT_HANDLE,                                   \
    "Invalid event handle.")

#define VIRTUAL1553B_EVENT_HANDLE_IS_NOT_INSTALLED_RETURN(OutBuffer, BufferSize, Irp)   \
    VIRTUAL1553B_RETURN_STATUS_CODE((OutBuffer), (BufferSize), (Irp),                   \
    VIRTUAL1553B_STATUS_EVENT_HANDLE_IS_NOT_INSTALLED,                                  \
    "Event handle is not installed.")

#define VIRTUAL1553B_TERMINAL_IS_ALREADY_STARTED_RETURN(OutBuffer, BufferSize, Irp) \
    VIRTUAL1553B_RETURN_STATUS_CODE((OutBuffer), (BufferSize), (Irp),               \
    VIRTUAL1553B_STATUS_TERMINAL_IS_ALREADY_STARTED,                                \
    "Terminal is already started.")

#define VIRTUAL1553B_NOT_ENOUGH_MEMORY_RETURN(OutBuffer, BufferSize, Irp)   \
    VIRTUAL1553B_RETURN_STATUS_CODE((OutBuffer), (BufferSize), (Irp),       \
    VIRTUAL1553B_STATUS_NOT_ENOUGH_MEMORY,                                  \
    "Not enough memory.")

#define VIRTUAL1553B_CREATE_WORKER_THREADS_FAILED_RETURN(OutBuffer, BufferSize, Irp)    \
    VIRTUAL1553B_RETURN_STATUS_CODE((OutBuffer), (BufferSize), (Irp),                   \
    VIRTUAL1553B_STATUS_CREATE_WORKER_THREADS_FAILED,                                   \
    "Create worker threads failed.")

#define VIRTUAL1553B_INVALID_REMOTE_TERMINAL_ADDRESS_RETURN(OutBuffer, BufferSize, Irp) \
    VIRTUAL1553B_RETURN_STATUS_CODE((OutBuffer), (BufferSize), (Irp),                   \
    VIRTUAL1553B_STATUS_INVALID_REMOTE_TERMINAL_ADDRESS,                                \
    "Invalid remote terminal address.")

#define VIRTUAL1553B_TERMINAL_IS_NOT_STARTED_RETURN(OutBuffer, BufferSize, Irp) \
    VIRTUAL1553B_RETURN_STATUS_CODE((OutBuffer), (BufferSize), (Irp),           \
    VIRTUAL1553B_STATUS_TERMINAL_IS_NOT_STARTED,                                \
    "Terminal is not started.")

#define VIRTUAL1553B_UNKNOWN_MESSAGE_FORMAT_RETURN(OutBuffer, BufferSize, Irp)  \
    VIRTUAL1553B_RETURN_STATUS_CODE((OutBuffer), (BufferSize), (Irp),           \
    VIRTUAL1553B_STATUS_UNKNOWN_MESSAGE_FORMAT,                                 \
    "Unknown message format.")

#define VIRTUAL1553B_TERMINAL_MODE_MISMATCH_RETURN(OutBuffer, BufferSize, Irp)  \
    VIRTUAL1553B_RETURN_STATUS_CODE((OutBuffer), (BufferSize), (Irp),           \
    VIRTUAL1553B_STATUS_TERMINAL_MODE_MISMATCH,                                 \
    "Terminal mode mismatch.")

#define VIRTUAL1553B_INVALID_TRANSFER_LINE_RETURN(OutBuffer, BufferSize, Irp)   \
    VIRTUAL1553B_RETURN_STATUS_CODE((OutBuffer), (BufferSize), (Irp),           \
    VIRTUAL1553B_STATUS_INVALID_TRANSFER_LINE,                                  \
    "Invalid transfer line.")

#define VIRTUAL1553B_TRANSFER_LINE_IS_BUSY_RETURN(OutBuffer, BufferSize, Irp)   \
    VIRTUAL1553B_RETURN_STATUS_CODE((OutBuffer), (BufferSize), (Irp),           \
    VIRTUAL1553B_STATUS_TRANSFER_LINE_IS_BUSY,                                  \
    "Transfer line is busy.")

#define VIRTUAL1553B_BASE_IS_OUT_OF_RANGE_RETURN(OutBuffer, BufferSize, Irp)    \
    VIRTUAL1553B_RETURN_STATUS_CODE((OutBuffer), (BufferSize), (Irp),           \
    VIRTUAL1553B_STATUS_BASE_IS_OUT_OF_RANGE,                                   \
    "Base is out of range.")

#define VIRTUAL1553B_INVALID_TRANSFER_DIRECTION_RETURN(OutBuffer, BufferSize, Irp)  \
    VIRTUAL1553B_RETURN_STATUS_CODE((OutBuffer), (BufferSize), (Irp),               \
    VIRTUAL1553B_STATUS_INVALID_TRANSFER_DIRECTION,                                 \
    "Invalid transfer direction.")

#define VIRTUAL1553B_INVALID_SUBADDRESS_RETURN(OutBuffer, BufferSize, Irp)  \
    VIRTUAL1553B_RETURN_STATUS_CODE((OutBuffer), (BufferSize), (Irp),       \
    VIRTUAL1553B_STATUS_INVALID_SUBADDRESS,                                 \
    "Invalid subaddress.")

#define VIRTUAL1553B_INVALID_DATA_WORD_COUNT_RETURN(OutBuffer, BufferSize, Irp) \
    VIRTUAL1553B_RETURN_STATUS_CODE((OutBuffer), (BufferSize), (Irp),           \
    VIRTUAL1553B_STATUS_INVALID_DATA_WORD_COUNT,                                \
    "Invalid data word count.")

#define VIRTUAL1553B_REGISTER_REMOTE_TERMINAL_FAILED_RETURN(OutBuffer, BufferSize, Irp) \
    VIRTUAL1553B_RETURN_STATUS_CODE((OutBuffer), (BufferSize), (Irp),                   \
    VIRTUAL1553B_STATUS_REGISTER_REMOTE_TERMINAL_FAILED,                                \
    "Register remote terminal failed.")

#define VIRTUAL1553B_REGISTER_BUS_MONITOR_FAILED_RETURN(OutBuffer, BufferSize, Irp) \
    VIRTUAL1553B_RETURN_STATUS_CODE((OutBuffer), (BufferSize), (Irp),               \
    VIRTUAL1553B_STATUS_REGISTER_BUS_MONITOR_FAILED,                                \
    "Register bus monitor failed.")

#define VIRTUAL1553B_REMAP_REMOTE_TERMINAL_FAILED_RETURN(OutBuffer, BufferSize, Irp)    \
    VIRTUAL1553B_RETURN_STATUS_CODE((OutBuffer), (BufferSize), (Irp),                   \
    VIRTUAL1553B_STATUS_REMAP_REMOTE_TERMINAL_FAILED,                                   \
    "Remap remote terminal failed.")

#define VIRTUAL1553B_TRANSFER_RESULT_BUFFER_IS_EMPTY_RETURN(OutBuffer, BufferSize, Irp) \
    VIRTUAL1553B_RETURN_STATUS_CODE((OutBuffer), (BufferSize), (Irp),                   \
    VIRTUAL1553B_STATUS_TRANSFER_RESULT_BUFFER_IS_EMPTY,                                \
    "Transfer result buffer is empty.")


#define VIRTUAL1553B_VALIDATE_TERMINAL_INDEX(Index, OutBuffer, BufferSize, Irp)                 \
{                                                                                               \
    if ((Index) < VIRTUAL_TERMINAL_INDEX_MINIMUM || (Index) > VIRTUAL_TERMINAL_INDEX_MAXIMUM)   \
    {                                                                                           \
        VIRTUAL1553B_INVALID_TERMINAL_HANDLE_RETURN((OutBuffer), (BufferSize), (Irp));          \
    }                                                                                           \
}

#define VIRTUAL1553B_VALIDATE_TERMINAL_OWNER(VirtualTerminal, OutBuffer, BufferSize, Irp)   \
{                                                                                           \
    if ((VirtualTerminal)->ProcessIdOwner != PsGetCurrentProcessId())                       \
    {                                                                                       \
        ReleaseVirtualTerminal(VirtualTerminal);                                            \
        VIRTUAL1553B_PROCESS_IS_NOT_OWNER_RETURN((OutBuffer), (BufferSize), (Irp));         \
    }                                                                                       \
}

#define VIRTUAL1553B_VALIDATE_TERMINAL_IS_STOPPED(VirtualTerminal, OutBuffer, BufferSize, Irp)  \
{                                                                                               \
    if ((VirtualTerminal)->Mode != UndefinedMode)                                               \
    {                                                                                           \
        ReleaseVirtualTerminal(VirtualTerminal);                                                \
        VIRTUAL1553B_TERMINAL_IS_NOT_STOPPED_RETURN((OutBuffer), (BufferSize), (Irp));          \
    }                                                                                           \
}

#define VIRTUAL1553B_VALIDATE_TERMINAL_IS_NOT_STARTED(VirtualTerminal, OutBuffer, BufferSize, Irp)  \
{                                                                                                   \
    if ((VirtualTerminal)->Mode != UndefinedMode)                                                   \
    {                                                                                               \
        ReleaseVirtualTerminal(VirtualTerminal);                                                    \
        VIRTUAL1553B_TERMINAL_IS_ALREADY_STARTED_RETURN((OutBuffer), (BufferSize), (Irp));          \
    }                                                                                               \
}

#define VIRTUAL1553B_VALIDATE_REMOTE_TERMINAL_ADDRESS(Address, VirtualTerminal, OutBuffer, BufferSize, Irp) \
{                                                                                                           \
    if ((Address) < REMOTE_TERMINAL_ADDRESS_MINIMUM || (Address) > REMOTE_TERMINAL_ADDRESS_MAXIMUM)         \
    {                                                                                                       \
        ReleaseVirtualTerminal(VirtualTerminal);                                                            \
        VIRTUAL1553B_INVALID_REMOTE_TERMINAL_ADDRESS_RETURN((OutBuffer), (BufferSize), (Irp));              \
    }                                                                                                       \
}

#define VIRTUAL1553B_VALIDATE_TERMINAL_IS_STARTED(VirtualTerminal, OutBuffer, BufferSize, Irp)  \
{                                                                                               \
    if ((VirtualTerminal)->Mode == UndefinedMode)                                               \
    {                                                                                           \
        ReleaseVirtualTerminal(VirtualTerminal);                                                \
        VIRTUAL1553B_TERMINAL_IS_NOT_STARTED_RETURN((OutBuffer), (BufferSize), (Irp));          \
    }                                                                                           \
}

#define VIRTUAL1553B_VALIDATE_TERMINAL_MODE(TerminalMode, VirtualTerminal, OutBuffer, BufferSize, Irp)  \
{                                                                                                       \
    if ((VirtualTerminal)->Mode != (TerminalMode))                                                      \
    {                                                                                                   \
        ReleaseVirtualTerminal(VirtualTerminal);                                                        \
        VIRTUAL1553B_TERMINAL_MODE_MISMATCH_RETURN((OutBuffer), (BufferSize), (Irp));                   \
    }                                                                                                   \
}

#define VIRTUAL1553B_VALIDATE_TRANSFER_LINE(TransferLine, VirtualTerminal, OutBuffer, BufferSize, Irp)  \
{                                                                                                       \
    if ((TransferLine) < TransferLineA || (TransferLine) >= TransferLineNumber)                         \
    {                                                                                                   \
        ReleaseVirtualTerminal(VirtualTerminal);                                                        \
        VIRTUAL1553B_INVALID_TRANSFER_LINE_RETURN((OutBuffer), (BufferSize), (Irp));                    \
    }                                                                                                   \
}

#define VIRTUAL1553B_TRY_LOCK_TRANSFER_LINE(Line, VirtualTerminal, OutBuffer, BufferSize, Irp)  \
{                                                                                               \
    if (Virtual1553B_LockTransferLine((Line), (VirtualTerminal)))                               \
    {                                                                                           \
        (Line)->Time.QuadPart = 0;                                                              \
        (Line)->Error = VIRTUAL1553B_ERROR_UNKNOWN_ERROR;                                       \
        (Line)->Format = UnknownMessageFormat;                                                  \
        (Line)->CW1.Raw = INVALID_COMMAND_WORD;                                                 \
        (Line)->CW2.Raw = INVALID_COMMAND_WORD;                                                 \
        (Line)->SW1.Raw = INVALID_STATUS_WORD;                                                  \
        (Line)->SW2.Raw = INVALID_STATUS_WORD;                                                  \
        RtlFillMemory((Line)->Data, DATA_WORD_COUNT_MAXIMUM * sizeof(USHORT), -1);              \
    }                                                                                           \
    else                                                                                        \
    {                                                                                           \
        ReleaseVirtualTerminal(VirtualTerminal);                                                \
        VIRTUAL1553B_TRANSFER_LINE_IS_BUSY_RETURN((OutBuffer), (BufferSize), (Irp));            \
    }                                                                                           \
}

#define VIRTUAL1553B_VALIDATE_BUS_MONITOR_BASE(TargetBase, VirtualTerminal, OutBuffer, BufferSize, Irp) \
{                                                                                                       \
    if ((TargetBase) < 0 || (TargetBase) >= TRANSFER_RECORD_BUFFER_LENGTH)                              \
    {                                                                                                   \
        ReleaseVirtualTerminal(VirtualTerminal);                                                        \
        VIRTUAL1553B_BASE_IS_OUT_OF_RANGE_RETURN((OutBuffer), (BufferSize), (Irp));                     \
    }                                                                                                   \
}

#define VIRTUAL1553B_VALIDATE_TRANSFER_DIRECTION(Direction, VirtualTerminal, OutBuffer, BufferSize, Irp)    \
{                                                                                                           \
    switch (Direction)                                                                                      \
    {                                                                                                       \
    case DirectionReceive:                                                                                  \
    case DirectionTransmit:                                                                                 \
        break;                                                                                              \
    default:                                                                                                \
        ReleaseVirtualTerminal(VirtualTerminal);                                                            \
        VIRTUAL1553B_INVALID_TRANSFER_DIRECTION_RETURN((OutBuffer), (BufferSize), (Irp));                   \
    }                                                                                                       \
}

#define VIRTUAL1553B_VALIDATE_SUBADDRESS(Subaddress, VirtualTerminal, OutBuffer, BufferSize, Irp)   \
{                                                                                                   \
    if ((Subaddress) < SUBADDRESS_MINIMUM || (Subaddress) > SUBADDRESS_MAXIMUM)                     \
    {                                                                                               \
        ReleaseVirtualTerminal(VirtualTerminal);                                                    \
        VIRTUAL1553B_INVALID_SUBADDRESS_RETURN((OutBuffer), (BufferSize), (Irp));                   \
    }                                                                                               \
}

#define VIRTUAL1553B_VALIDATE_DATA_WORD_COUNT(DataWordCount, VirtualTerminal, OutBuffer, BufferSize, Irp)   \
{                                                                                                           \
    if ((DataWordCount) < DATA_WORD_COUNT_MINIMUM || (DataWordCount) > DATA_WORD_COUNT_MAXIMUM)             \
    {                                                                                                       \
        ReleaseVirtualTerminal(VirtualTerminal);                                                            \
        VIRTUAL1553B_INVALID_DATA_WORD_COUNT_RETURN((OutBuffer), (BufferSize), (Irp));                      \
    }                                                                                                       \
}


#define Virtual1553B_InitiateTransfer(VirtualTerminal, TransferLine) \
    KeSetEvent(&(VirtualTerminal)->WorkerThread[(TransferLine)].Context.InitiateTransferEvent, IO_NO_INCREMENT, FALSE)


NTSTATUS Virtual1553B_AllocTerminal(
    IN PVIRTUAL1553B_EXTENSION Extension,
    IN ULONG InputBufferLength,
    IN ULONG OutputBufferLength,
    IN PIRP Irp);

NTSTATUS Virtual1553B_FreeTerminal(
    IN PVIRTUAL1553B_EXTENSION Extension,
    IN ULONG InputBufferLength,
    IN ULONG OutputBufferLength,
    IN PIRP Irp);


NTSTATUS Virtual1553B_InstallEvent(
    IN PVIRTUAL1553B_EXTENSION Extension,
    IN ULONG InputBufferLength,
    IN ULONG OutputBufferLength,
    IN PIRP Irp);

NTSTATUS Virtual1553B_RemoveEvent(
    IN PVIRTUAL1553B_EXTENSION Extension,
    IN ULONG InputBufferLength,
    IN ULONG OutputBufferLength,
    IN PIRP Irp);


NTSTATUS Virtual1553B_GetTerminalMode(
    IN PVIRTUAL1553B_EXTENSION Extension,
    IN ULONG InputBufferLength,
    IN ULONG OutputBufferLength,
    IN PIRP Irp);

NTSTATUS Virtual1553B_StartBusController(
    IN PVIRTUAL1553B_EXTENSION Extension,
    IN ULONG InputBufferLength,
    IN ULONG OutputBufferLength,
    IN PIRP Irp);

NTSTATUS Virtual1553B_StartRemoteTerminal(
    IN PVIRTUAL1553B_EXTENSION Extension,
    IN ULONG InputBufferLength,
    IN ULONG OutputBufferLength,
    IN PIRP Irp);

NTSTATUS Virtual1553B_StartBusMonitor(
    IN PVIRTUAL1553B_EXTENSION Extension,
    IN ULONG InputBufferLength,
    IN ULONG OutputBufferLength,
    IN PIRP Irp);

NTSTATUS Virtual1553B_StopTerminal(
    IN PVIRTUAL1553B_EXTENSION Extension,
    IN ULONG InputBufferLength,
    IN ULONG OutputBufferLength,
    IN PIRP Irp);


NTSTATUS VirtualBC_ExecuteTransfer(
    IN PVIRTUAL1553B_EXTENSION Extension,
    IN ULONG InputBufferLength,
    IN ULONG OutputBufferLength,
    IN PIRP Irp);

NTSTATUS VirtualBC_GetTransferResult(
    IN PVIRTUAL1553B_EXTENSION Extension,
    IN ULONG InputBufferLength,
    IN ULONG OutputBufferLength,
    IN PIRP Irp);


NTSTATUS VirtualRT_SetAddress(
    IN PVIRTUAL1553B_EXTENSION Extension,
    IN ULONG InputBufferLength,
    IN ULONG OutputBufferLength,
    IN PIRP Irp);

NTSTATUS VirtualRT_GetAddress(
    IN PVIRTUAL1553B_EXTENSION Extension,
    IN ULONG InputBufferLength,
    IN ULONG OutputBufferLength,
    IN PIRP Irp);

NTSTATUS VirtualRT_WriteMemory(
    IN PVIRTUAL1553B_EXTENSION Extension,
    IN ULONG InputBufferLength,
    IN ULONG OutputBufferLength,
    IN PIRP Irp);

NTSTATUS VirtualRT_ReadMemory(
    IN PVIRTUAL1553B_EXTENSION Extension,
    IN ULONG InputBufferLength,
    IN ULONG OutputBufferLength,
    IN PIRP Irp);

NTSTATUS VirtualRT_GetTransferResult(
    IN PVIRTUAL1553B_EXTENSION Extension,
    IN ULONG InputBufferLength,
    IN ULONG OutputBufferLength,
    IN PIRP Irp);


NTSTATUS VirtualBM_GetCurrentBase(
    IN PVIRTUAL1553B_EXTENSION Extension,
    IN ULONG InputBufferLength,
    IN ULONG OutputBufferLength,
    IN PIRP Irp);

NTSTATUS VirtualBM_GetBaseCount(
    IN PVIRTUAL1553B_EXTENSION Extension,
    IN ULONG InputBufferLength,
    IN ULONG OutputBufferLength,
    IN PIRP Irp);

NTSTATUS VirtualBM_GetTransferRecord(
    IN PVIRTUAL1553B_EXTENSION Extension,
    IN ULONG InputBufferLength,
    IN ULONG OutputBufferLength,
    IN PIRP Irp);
