
#pragma once

#include <pshpack1.h>


#ifdef __cplusplus
extern "C" {
#endif // __cplusplus


#pragma warning(push)
#pragma warning(disable:4201) // nameless struct/union

typedef union _COMMAND_WORD {
    USHORT Raw;
    struct {
        USHORT DataWordCount : 5;
        USHORT Subaddress : 5;
        USHORT TransmitReceive : 1;
        USHORT RemoteTerminalAddress : 5;
    };
    struct {
        USHORT ModeCode : 5;
        USHORT ModeControl : 5;
    };
} COMMAND_WORD, *PCOMMAND_WORD;

#define INVALID_COMMAND_WORD ((USHORT)0x0000)


typedef union _STATUS_WORD {
    USHORT Raw;
    struct {
        USHORT TerminalFlag : 1;
        USHORT DynamicBusControlAcceptance : 1;
        USHORT SubsystemFlag : 1;
        USHORT Busy : 1;
        USHORT BroadcastCommandReceived : 1;
        USHORT Reserved : 3;
        USHORT ServiceRequest : 1;
        USHORT Instrumentation : 1;
        USHORT MessageError : 1;
        USHORT RemoteTerminalAddress : 5;
    };
    struct {
        USHORT Flags : 11;
    };
} STATUS_WORD, *PSTATUS_WORD;

#define INVALID_STATUS_WORD ((USHORT)0xFFFF)

#pragma warning(pop)


typedef enum _MESSAGE_DATA_MASK {
    MaskDataWordCount               = 0x001F,
    MaskSubaddress                  = 0x03E0,
    MaskTransmitReceive             = 0x0400,
    MaskRemoteTerminalAddress       = 0xF800,
    MaskModeCode                    = 0x001F,
    MaskModeControl                 = 0x03E0,
    MaskModeCommand                 = 0x041F,
    MaskTerminalFlag                = 0x0001,
    MaskDynamicBusControlAcceptance = 0x0002,
    MaskSubsystemFlag               = 0x0004,
    MaskBusy                        = 0x0008,
    MaskBroadcastCommandReceived    = 0x0010,
    MaskReserved                    = 0x00E0,
    MaskServiceRequest              = 0x0100,
    MaskInstrumentation             = 0x0200,
    MaskMessageError                = 0x0400,
    MaskFlags                       = 0x07FF
} MESSAGE_DATA_MASK;


typedef enum _TRANSFER_DIRECTION {
    DirectionReceive  = 0x0000,
    DirectionTransmit = 0x0400
} TRANSFER_DIRECTION;

#define SUBADDRESS_LENGTH (32)


typedef enum _MODE_CONTROL {
    ModeControl0  = 0x0000,
    ModeControl31 = 0x03E0
} MODE_CONTROL;


typedef enum _MODE_CODE {
    CodeDynamicBusControl                   = 0x00,
    CodeSynchronize                         = 0x01,
    CodeTransmitStatusWord                  = 0x02,
    CodeInitiateSelfTest                    = 0x03,
    CodeTransmitterShutdown                 = 0x04,
    CodeOverrideTransmitterShutdown         = 0x05,
    CodeInhibitTerminalFlagBit              = 0x06,
    CodeOverrideInhibitTerminalFlagBit      = 0x07,
    CodeResetRemoteTerminal                 = 0x08,
    CodeTransmitVectorWord                  = 0x10,
    CodeSynchronizeWithDataWord             = 0x11,
    CodeTransmitLastCommandWord             = 0x12,
    CodeTransmitBuiltInTestWord             = 0x13,
    CodeSelectedTransmitterShutdown         = 0x14,
    CodeOverrideSelectedTransmitterShutdown = 0x15,
} MODE_CODE;

#define CODE_RESERVED_WITHOUT_DATA_MINIMUM (0x09)
#define CODE_RESERVED_WITHOUT_DATA_MAXIMUM (0x0F)
#define CODE_RESERVED_WITH_DATA_MINIMUM    (0x16)
#define CODE_RESERVED_WITH_DATA_MAXIMUM    (0x1F)

typedef enum _MODE_COMMAND {
    CommandDynamicBusControl                    = 0x400,
    CommandSynchronize                          = 0x401,
    CommandTransmitStatusWord                   = 0x402,
    CommandInitiateSelfTest                     = 0x403,
    CommandTransmitterShutdown                  = 0x404,
    CommandOverrideTransmitterShutdown          = 0x405,
    CommandInhibitTerminalFlagBit               = 0x406,
    CommandOverrideInhibitTerminalFlagBit       = 0x407,
    CommandResetRemoteTerminal                  = 0x408,
    CommandTransmitVectorWord                   = 0x410,
    CommandSynchronizeWithDataWord              = 0x011,
    CommandTransmitLastCommandWord              = 0x412,
    CommandTransmitBuiltInTestWord              = 0x413,
    CommandSelectedTransmitterShutdown          = 0x014,
    CommandOverrideSelectedTransmitterShutdown  = 0x015
} MODE_COMMAND;

#define COMMAND_RESERVED_WITHOUT_DATA_MINIMUM (0x409)
#define COMMAND_RESERVED_WITHOUT_DATA_MAXIMUM (0x40F)


typedef struct _MESSAGE_DATA_ROUTINE
{

#define REMOTE_TERMINAL_ADDRESS_MINIMUM   (0x00)
#define REMOTE_TERMINAL_ADDRESS_MAXIMUM   (0x1E)
#define BROADCAST_REMOTE_TERMINAL_ADDRESS (0x1F)

#define SUBADDRESS_MINIMUM (0x01)
#define SUBADDRESS_MAXIMUM (0x1E)

#define DATA_WORD_COUNT_MINIMUM (1)
#define DATA_WORD_COUNT_MAXIMUM (32)

    //////////////////////////////////////////////////////////////////////////
    //
    // ������� BuildCommandWord:
    //   ��������� ��������� ����� � ������������ �� ���������� �������
    //   ����������.
    //
    // ���������:
    //   RemoteTerminalAddress:
    //     ����� ���������� ����������.
    //     ��� ������������� ���������� ����� � �������� �������� ���������
    //     �������� ������ ��������� �������� �� ���������
    //     [REMOTE_TERMINAL_ADDRESS_MINIMUM; REMOTE_TERMINAL_ADDRESS_MAXIMUM].
    //     ��� ������������� ���������� ����� � �������� ��������� ���������
    //     ��������, ������ ���������� ���� ���������, ����� ����� ���������
    //     �������� ��������� BROADCAST_REMOTE_TERMINAL_ADDRESS.
    //
    //   TransmitReceive:
    //     ���� "�������� / �����".
    //     �������� ������ ��������� ���� �� �������� ������������
    //     TRANSFER_DIRECTION.
    //
    //   Subaddress:
    //     �������� ���������� ����������.
    //     �������� ������ ��������� �������� �� ���������
    //     [SUBADDRESS_MINIMUM; SUBADDRESS_MAXIMUM].
    //
    //   DataWordCount:
    //     ����� ���� ������.
    //     �������� ������ ��������� �������� �� ���������
    //     [DATA_WORD_COUNT_MINIMUM; DATA_WORD_COUNT_MAXIMUM].
    //
    // ������������ ��������:
    //   ��������� �����.
    //
    // ���������:
    //   �������� ���������� RemoteTerminalAddress, Subaddress � DataWordCount
    //   ��-��������� �������������� ������ 0x001F. �������� �������� ��������
    //   �����, ������������ ����� ���� ������, ������ ��������������� ��
    //   ���������� ������������, �� ����������� ���� 00000, ������� ������
    //   ��������������� ����� 32.
    //
    //////////////////////////////////////////////////////////////////////////
    inline static USHORT BuildCommandWord(
        __in USHORT RemoteTerminalAddress,
        __in TRANSFER_DIRECTION TransmitReceive,
        __in USHORT Subaddress,
        __in USHORT DataWordCount)
    {
        return (USHORT)(
            ((RemoteTerminalAddress & 0x001F) << 11) |
            (TransmitReceive) |
            ((Subaddress & 0x001F) << 5) |
            (DataWordCount & 0x001F)
            );
    }

#define BUILD_COMMAND_WORD(RemoteTerminalAddress, TransmitReceive, Subaddress, DataWordCount) \
    _MESSAGE_DATA_ROUTINE::BuildCommandWord( \
    (RemoteTerminalAddress), (TransmitReceive), (Subaddress), (DataWordCount))

    //////////////////////////////////////////////////////////////////////////
    //
    // ������� BuildCommandWordReceive:
    //   ��������� ��������� ����� �� ����� ��������� ����������� ���� ������
    //   � ������������ �� ���������� ������� ����������.
    //
    // ���������:
    //   RemoteTerminalAddress:
    //     ����� ���������� ����������.
    //     ��� ������������� ���������� ����� � �������� �������� ���������
    //     �������� ������ ��������� �������� �� ���������
    //     [REMOTE_TERMINAL_ADDRESS_MINIMUM; REMOTE_TERMINAL_ADDRESS_MAXIMUM].
    //     ��� ������������� ���������� ����� � �������� ��������� ���������
    //     ��������, ������ ���������� ���� ���������, ����� ����� ���������
    //     �������� ��������� BROADCAST_REMOTE_TERMINAL_ADDRESS.
    //
    //   Subaddress:
    //     �������� ���������� ����������.
    //     �������� ������ ��������� �������� �� ���������
    //     [SUBADDRESS_MINIMUM; SUBADDRESS_MAXIMUM].
    //
    //   DataWordCount:
    //     ����� ���� ������.
    //     �������� ������ ��������� �������� �� ���������
    //     [DATA_WORD_COUNT_MINIMUM; DATA_WORD_COUNT_MAXIMUM].
    //
    // ������������ ��������:
    //   ��������� ����� �� ����� ��������� ����������� ���� ������.
    //
    // ���������:
    //   �������� ���������� RemoteTerminalAddress, Subaddress � DataWordCount
    //   ��-��������� �������������� ������ 0x001F. �������� �������� ��������
    //   �����, ������������ ����� ���� ������, ������ ��������������� ��
    //   ���������� ������������, �� ����������� ���� 00000, ������� ������
    //   ��������������� ����� 32. ���� "�������� / �����" � ��������� �����
    //   ��������� �������� DirectionReceive.
    //
    //////////////////////////////////////////////////////////////////////////
    inline static USHORT BuildCommandWordReceive(
        __in USHORT RemoteTerminalAddress,
        __in USHORT Subaddress,
        __in USHORT DataWordCount)
    {
        return (USHORT)(
            ((RemoteTerminalAddress & 0x001F) << 11) |
            (DirectionReceive) |
            ((Subaddress & 0x001F) << 5) |
            (DataWordCount & 0x001F)
            );
    }

#define BUILD_COMMAND_WORD_RECEIVE(RemoteTerminalAddress, Subaddress, DataWordCount) \
    _MESSAGE_DATA_ROUTINE::BuildCommandWordReceive( \
    (RemoteTerminalAddress), (Subaddress), (DataWordCount))

    //////////////////////////////////////////////////////////////////////////
    //
    // ������� BuildCommandWordTransmit:
    //   ��������� ��������� ����� �� �������� ��������� ����������� ����
    //   ������ � ������������ �� ���������� ������� ����������.
    //
    // ���������:
    //   RemoteTerminalAddress:
    //     ����� ���������� ����������.
    //     �������� ������ ��������� �������� �� ���������
    //     [REMOTE_TERMINAL_ADDRESS_MINIMUM; REMOTE_TERMINAL_ADDRESS_MAXIMUM]
    //     ��� ��� ������������� ���������� ����� � �������� ��������
    //     ���������, ��� � ��� ������������� ���������� ����� � ��������
    //     ��������� ���������.
    //
    //   Subaddress:
    //     �������� ���������� ����������.
    //     �������� ������ ��������� �������� �� ���������
    //     [SUBADDRESS_MINIMUM; SUBADDRESS_MAXIMUM].
    //
    //   DataWordCount:
    //     ����� ���� ������.
    //     �������� ������ ��������� �������� �� ���������
    //     [DATA_WORD_COUNT_MINIMUM; DATA_WORD_COUNT_MAXIMUM].
    //
    // ������������ ��������:
    //   ��������� ����� �� �������� ��������� ����������� ���� ������.
    //
    // ���������:
    //   �������� ���������� RemoteTerminalAddress, Subaddress � DataWordCount
    //   ��-��������� �������������� ������ 0x001F. �������� �������� ��������
    //   �����, ������������ ����� ���� ������, ������ ��������������� ��
    //   ���������� ������������, �� ����������� ���� 00000, ������� ������
    //   ��������������� ����� 32. ���� "�������� / �����" � ��������� �����
    //   ��������� �������� DirectionTransmit.
    //
    //////////////////////////////////////////////////////////////////////////
    inline static USHORT BuildCommandWordTransmit(
        __in USHORT RemoteTerminalAddress,
        __in USHORT Subaddress,
        __in USHORT DataWordCount)
    {
        return (USHORT)(
            ((RemoteTerminalAddress & 0x001F) << 11) |
            (DirectionTransmit) |
            ((Subaddress & 0x001F) << 5) |
            (DataWordCount & 0x001F)
            );
    }

#define BUILD_COMMAND_WORD_TRANSMIT(RemoteTerminalAddress, Subaddress, DataWordCount) \
    _MESSAGE_DATA_ROUTINE::BuildCommandWordTransmit( \
    (RemoteTerminalAddress), (Subaddress), (DataWordCount))

    //////////////////////////////////////////////////////////////////////////
    //
    // ������� BuildCommandWordModeControl:
    //   ��������� ��������� ����� ���������� � ������������ �� ����������
    //   ������� ����������.
    //
    // ���������:
    //   RemoteTerminalAddress:
    //     ����� ���������� ����������.
    //     ��� ������������� ���������� ����� � �������� �������� ���������
    //     �������� ������ ��������� �������� �� ���������
    //     [REMOTE_TERMINAL_ADDRESS_MINIMUM; REMOTE_TERMINAL_ADDRESS_MAXIMUM].
    //     ��� ������������� ���������� ����� � �������� ��������� ���������
    //     ��������, ������ ���������� ���� ���������, ����� ����� ���������
    //     �������� ��������� BROADCAST_REMOTE_TERMINAL_ADDRESS.
    //
    //   ModeControl:
    //     ����� ����������.
    //     �������� ������ ��������� ���� �� �������� ������������
    //     MODE_CONTROL.
    //
    //   ModeCommand:
    //     ������� ����������.
    //     �������� ����� ��������� ���� �� �������� ������������
    //     MODE_COMMAND, � ����� �������� �� ����� ����������������� ������,
    //     ��������, � �������� ���������������� ������ ����������.
    //
    // ������������ ��������:
    //   ��������� ����� ����������.
    //
    // ���������:
    //   �������� ��������� RemoteTerminalAddress ��-��������� ��������������
    //   ������ 0x001F. �������� ��������� Command ��-���������
    //   �������������� ������ 0x041F.
    //
    //////////////////////////////////////////////////////////////////////////
    inline static USHORT BuildCommandWordModeControl(
        __in USHORT RemoteTerminalAddress,
        __in MODE_CONTROL ModeControl,
        __in USHORT ModeCommand
        )
    {
        return (USHORT)(
            ((RemoteTerminalAddress & 0x001F) << 11) |
            (ModeControl) |
            (ModeCommand & 0x041F)
            );
    }

#define BUILD_COMMAND_WORD_MODE_CONTROL(RemoteTerminalAddress, ModeControl, ModeCommand) \
    _MESSAGE_DATA_ROUTINE::BuildCommandWordModeControl( \
    (RemoteTerminalAddress), (ModeControl), (ModeCommand))

    //////////////////////////////////////////////////////////////////////////
    //
    // ������� GetRemoteTerminalAddress:
    //   ��������� �������� ���� "����� ���������� ����������" �� ����������
    //   ��� ��������� �����.
    //
    // ���������:
    //   Word:
    //     ��������� ��� �������� �����.
    //
    // ������������ ��������:
    //   �������� ���� "����� ���������� ����������".
    //
    //////////////////////////////////////////////////////////////////////////
    inline static USHORT GetRemoteTerminalAddress(
        __in USHORT Word)
    {
        return ((Word & MaskRemoteTerminalAddress) >> 11);
    }

#define GET_REMOTE_TERMINAL_ADDRESS(Word) \
    _MESSAGE_DATA_ROUTINE::GetRemoteTerminalAddress(Word)

#define INVALID_REMOTE_TERMINAL_ADDRESS (0x1F)

    //////////////////////////////////////////////////////////////////////////
    //
    // ������� GetSubaddress:
    //   ��������� �������� ���� "�������� ���������� ����������" ��
    //   ���������� �����.
    //
    // ���������:
    //   CommandWord:
    //     ��������� �����.
    //
    // ������������ ��������:
    //   �������� ���� "�������� ���������� ����������".
    //
    //////////////////////////////////////////////////////////////////////////
    inline static USHORT GetSubaddress(
        __in USHORT CommandWord)
    {
        return ((CommandWord & MaskSubaddress) >> 5);
    }

#define GET_SUBADDRESS(CommandWord) \
    _MESSAGE_DATA_ROUTINE::GetSubaddress(CommandWord)

#define INVALID_SUBADDRESS_0  (0x00)
#define INVALID_SUBADDRESS_31 (0x1F)

    //////////////////////////////////////////////////////////////////////////
    //
    // ������� GetDataWordCount:
    //   ��������� �������� ���� "����� ���� ������" �� ���������� �����.
    //
    // ���������:
    //   CommandWord:
    //     ��������� �����.
    //
    // ������������ ��������:
    //   �������� ���� "����� ���� ������".
    //
    // ���������:
    //   �������� �������� �������� �����, ������������ ����� ���� ������,
    //   ������ ��������������� �� ���������� ������������, �� �����������
    //   ���� 00000, ������� ������ ��������������� ����� 32.
    //
    //////////////////////////////////////////////////////////////////////////
    inline static USHORT GetDataWordCount(
        __in USHORT CommandWord)
    {
        USHORT DataWordCount = CommandWord & MaskDataWordCount;
        return (DataWordCount ? DataWordCount : 32);
    }

#define GET_DATA_WORD_COUNT(CommandWord) \
    _MESSAGE_DATA_ROUTINE::GetDataWordCount(CommandWord)

    //////////////////////////////////////////////////////////////////////////
    //
    // ������� GetModeControl:
    //   ��������� �������� ���� "����� ����������" �� ���������� �����
    //   ����������.
    //
    // ���������:
    //   CommandWord:
    //     ��������� ����� ����������.
    //
    // ������������ ��������:
    //   �������� ���� "����� ����������".
    //
    //////////////////////////////////////////////////////////////////////////
    inline static USHORT GetModeControl(
        __in USHORT CommandWord)
    {
        return ((CommandWord & MaskModeControl) >> 5);
    }

#define GET_MODE_CONTROL(CommandWord) \
    _MESSAGE_DATA_ROUTINE::GetModeControl(CommandWord)

#define MODE_CONTROL_0  (0x00)
#define MODE_CONTROL_31 (0x1F)

    //////////////////////////////////////////////////////////////////////////
    //
    // ������� GetModeCode:
    //   ��������� �������� ���� "��� �������" �� ���������� ����� ����������.
    //
    // ���������:
    //   CommandWord:
    //     ��������� ����� ����������.
    //
    // ������������ ��������:
    //   �������� ���� "��� �������".
    //
    //////////////////////////////////////////////////////////////////////////
    inline static USHORT GetModeCode(
        __in USHORT CommandWord)
    {
        return (CommandWord & MaskModeCode);
    }

#define GET_MODE_CODE(CommandWord) \
    _MESSAGE_DATA_ROUTINE::GetModeCode(CommandWord)

    //////////////////////////////////////////////////////////////////////////
    //
    // ������� GetModeCommand:
    //   ��������� �������� ������� ���������� �� ���������� ����� ����������.
    //
    // ���������:
    //   CommandWord:
    //     ��������� ����� ����������.
    //
    // ������������ ��������:
    //   �������� ������� ����������.
    //
    //////////////////////////////////////////////////////////////////////////
    inline static USHORT GetModeCommand(
        __in USHORT CommandWord)
    {
        return (CommandWord & MaskModeCommand);
    }

#define GET_MODE_COMMAND(CommandWord) \
    _MESSAGE_DATA_ROUTINE::GetModeCommand(CommandWord)

} MESSAGE_DATA_ROUTINE;


typedef enum _TERMINAL_MODE {
    UndefinedMode = 0,
    BusControllerMode,
    RemoteTerminalMode,
    BusMonitorMode
} TERMINAL_MODE;


#define USE_DUAL_REDUNDANT_BUS (0)

typedef enum _TRANSFER_LINE_NUMBER {
    TransferLineA = 0,
#if USE_DUAL_REDUNDANT_BUS
    TransferLineB = 1,
#endif // USE_DUAL_REDUNDANT_BUS
    TransferLineNumber,
    InvalidTransferLine = -1
} TRANSFER_LINE_NUMBER;


typedef enum _MESSAGE_FORMAT {
    UnknownMessageFormat = 0,
    TransferBCRT,
    TransferRTBC,
    TransferRTRT,
    ModeCommandWithoutData,
    ModeCommandWithDataTransmit,
    ModeCommandWithDataReceive,
    TransferBCRTBroadcast,
    TransferRTRTBroadcast,
    ModeCommandWithoutDataBroadcast,
    ModeCommandWithDataReceiveBroadcast
} MESSAGE_FORMAT;


#define VALIDATE_UNIQUE_REMOTE_TERMINAL_ADDRESS(CW) (   \
    ((CW) & MaskRemoteTerminalAddress) != 0xF800        \
    )

#define VALIDATE_BROADCAST_REMOTE_TERMINAL_ADDRESS(CW) (    \
    ((CW) & MaskRemoteTerminalAddress) == 0xF800            \
    )

#define VALIDATE_DIRECTION_RECEIVE(CW) (    \
    ((CW) & MaskTransmitReceive) == 0x0000  \
    )

#define VALIDATE_DIRECTION_TRANSMIT(CW) (   \
    ((CW) & MaskTransmitReceive) == 0x0400  \
    )

#define VALIDATE_SUBADDRESS(CW) (           \
    ((CW) & MaskSubaddress) != 0x03E0 &&    \
    ((CW) & MaskSubaddress) != 0x0000       \
    )

#define VALIDATE_MODE_CONTROL(CW) (         \
    ((CW) & MaskModeControl) == 0x03E0 ||   \
    ((CW) & MaskModeControl) == 0x0000      \
    )


#define VALIDATE_TRANSFER_BC_RT(CW) (               \
    VALIDATE_UNIQUE_REMOTE_TERMINAL_ADDRESS(CW) &&  \
    VALIDATE_DIRECTION_RECEIVE(CW) &&               \
    VALIDATE_SUBADDRESS(CW)                         \
    )

#define VALIDATE_TRANSFER_RT_BC(CW) (               \
    VALIDATE_UNIQUE_REMOTE_TERMINAL_ADDRESS(CW) &&  \
    VALIDATE_DIRECTION_TRANSMIT(CW) &&              \
    VALIDATE_SUBADDRESS(CW)                         \
    )

#define VALIDATE_TRANSFER_RT_RT(CW1, CW2) (                                     \
    VALIDATE_UNIQUE_REMOTE_TERMINAL_ADDRESS(CW1) &&                             \
    VALIDATE_DIRECTION_RECEIVE(CW1) &&                                          \
    VALIDATE_SUBADDRESS(CW1) &&                                                 \
    VALIDATE_UNIQUE_REMOTE_TERMINAL_ADDRESS(CW2) &&                             \
    VALIDATE_DIRECTION_TRANSMIT(CW2) &&                                         \
    VALIDATE_SUBADDRESS(CW2) &&                                                 \
    ((CW1) & MaskDataWordCount) == ((CW2) & MaskDataWordCount) &&               \
    ((CW1) & MaskRemoteTerminalAddress) != ((CW2) & MaskRemoteTerminalAddress)  \
    )

#define VALIDATE_MODE_COMMAND_WITHOUT_DATA(CW) (    \
    VALIDATE_UNIQUE_REMOTE_TERMINAL_ADDRESS(CW) &&  \
    VALIDATE_DIRECTION_TRANSMIT(CW) &&              \
    VALIDATE_MODE_CONTROL(CW)                       \
    )

#define VALIDATE_MODE_COMMAND_WITH_DATA_TRANSMIT(CW) (  \
    VALIDATE_UNIQUE_REMOTE_TERMINAL_ADDRESS(CW) &&      \
    VALIDATE_DIRECTION_TRANSMIT(CW) &&                  \
    VALIDATE_MODE_CONTROL(CW)                           \
    )

#define VALIDATE_MODE_COMMAND_WITH_DATA_RECEIVE(CW) (   \
    VALIDATE_UNIQUE_REMOTE_TERMINAL_ADDRESS(CW) &&      \
    VALIDATE_DIRECTION_RECEIVE(CW) &&                   \
    VALIDATE_MODE_CONTROL(CW)                           \
    )

#define VALIDATE_TRANSFER_BC_RT_BROADCAST(CW) (         \
    VALIDATE_BROADCAST_REMOTE_TERMINAL_ADDRESS(CW) &&   \
    VALIDATE_DIRECTION_RECEIVE(CW) &&                   \
    VALIDATE_SUBADDRESS(CW)                             \
    )

#define VALIDATE_TRANSFER_RT_RT_BROADCAST(CW1, CW2) (           \
    VALIDATE_BROADCAST_REMOTE_TERMINAL_ADDRESS(CW1) &&          \
    VALIDATE_DIRECTION_RECEIVE(CW1) &&                          \
    VALIDATE_SUBADDRESS(CW1) &&                                 \
    VALIDATE_UNIQUE_REMOTE_TERMINAL_ADDRESS(CW2) &&             \
    VALIDATE_DIRECTION_TRANSMIT(CW2) &&                         \
    VALIDATE_SUBADDRESS(CW2) &&                                 \
    ((CW1) & MaskDataWordCount) == ((CW2) & MaskDataWordCount)  \
    )

#define VALIDATE_MODE_COMMAND_WITHOUT_DATA_BROADCAST(CW) (  \
    VALIDATE_BROADCAST_REMOTE_TERMINAL_ADDRESS(CW) &&       \
    VALIDATE_DIRECTION_TRANSMIT(CW) &&                      \
    VALIDATE_MODE_CONTROL(CW)                               \
    )

#define VALIDATE_MODE_COMMAND_WITH_DATA_RECEIVE_BROADCAST(CW) ( \
    VALIDATE_BROADCAST_REMOTE_TERMINAL_ADDRESS(CW) &&           \
    VALIDATE_DIRECTION_RECEIVE(CW) &&                           \
    VALIDATE_MODE_CONTROL(CW)                                   \
    )


#ifdef __cplusplus
}
#endif // __cplusplus


#include <poppack.h>
