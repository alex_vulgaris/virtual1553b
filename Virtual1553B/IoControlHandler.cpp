
#include "stdafx.h"
#include "IoControlHandler.h"
#include "WorkerThreads.h"


//////////////////////////////////////////////////////////////////////////
//
// Function:            Virtual1553B_AllocTerminal
//
// I/O control codes:   IOCTL_VIRTUAL1553B_ALLOC_TERMINAL
//
// Input buffer:        -
// Output buffer:       OUTPUT_ALLOC_TERMINAL
//
//
// Status:              VIRTUAL1553B_STATUS_SUCCESS
//                      VIRTUAL1553B_STATUS_NO_FREE_TERMINALS
//
//////////////////////////////////////////////////////////////////////////
NTSTATUS Virtual1553B_AllocTerminal(
    IN PVIRTUAL1553B_EXTENSION Extension,
    IN ULONG InputBufferLength,
    IN ULONG OutputBufferLength,
    IN PIRP Irp)
{
    Virtual1553B_DbgPrintEntry();

    UNREFERENCED_PARAMETER(InputBufferLength);

    ULONG OutBufferSize = sizeof(OUTPUT_ALLOC_TERMINAL);
    VIRTUAL1553B_VALIDATE_OUTPUT_BUFFER_LENGTH(OutputBufferLength, OutBufferSize, Irp);
    POUTPUT_ALLOC_TERMINAL OutBuffer = (POUTPUT_ALLOC_TERMINAL)(Irp->AssociatedIrp.SystemBuffer);

    //////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////

    HANDLE ProcessId = PsGetCurrentProcessId();

    for (LONG_PTR TerminalIndex = 0; TerminalIndex < VIRTUAL_TERMINAL_NUMBER; ++TerminalIndex)
    {
        if (InterlockedCompareExchangePointer(
            &Extension->VirtualTerminal[TerminalIndex].ProcessIdOwner, ProcessId, NULL))
        {
            continue;
        }

        Virtual1553B_DbgPrint_2("Process ID: 0x%p, Terminal index: %ld.",
            (PVOID)(ProcessId), (LONG)(TerminalIndex));

        OutBuffer->TerminalHandle = (TERMINAL_HANDLE)(TerminalIndex);
        VIRTUAL1553B_SUCCESS_RETURN(OutBuffer, OutBufferSize, Irp);
    }

    OutBuffer->TerminalHandle = INVALID_TERMINAL_HANDLE;
    VIRTUAL1553B_NO_FREE_TERMINALS_RETURN(OutBuffer, OutBufferSize, Irp);
}


//////////////////////////////////////////////////////////////////////////
//
// Function:            Virtual1553B_FreeTerminal
//
// I/O control codes:   IOCTL_VIRTUAL1553B_FREE_TERMINAL
//
// Input buffer:        INPUT_FREE_TERMINAL
// Output buffer:       OUTPUT_FREE_TERMINAL
//
//
// Status:              VIRTUAL1553B_STATUS_SUCCESS
//                      VIRTUAL1553B_STATUS_INVALID_TERMINAL_HANDLE
//                      VIRTUAL1553B_STATUS_PROCESS_IS_NOT_OWNER
//                      VIRTUAL1553B_STATUS_TERMINAL_IS_NOT_STOPPED
//
//////////////////////////////////////////////////////////////////////////
NTSTATUS Virtual1553B_FreeTerminal(
    IN PVIRTUAL1553B_EXTENSION Extension,
    IN ULONG InputBufferLength,
    IN ULONG OutputBufferLength,
    IN PIRP Irp)
{
    Virtual1553B_DbgPrintEntry();

    ULONG InBufferSize = sizeof(INPUT_FREE_TERMINAL);
    VIRTUAL1553B_VALIDATE_INPUT_BUFFER_LENGTH(InputBufferLength, InBufferSize, Irp);
    PINPUT_FREE_TERMINAL InBuffer = (PINPUT_FREE_TERMINAL)(Irp->AssociatedIrp.SystemBuffer);

    ULONG OutBufferSize = sizeof(OUTPUT_FREE_TERMINAL);
    VIRTUAL1553B_VALIDATE_OUTPUT_BUFFER_LENGTH(OutputBufferLength, OutBufferSize, Irp);
    POUTPUT_FREE_TERMINAL OutBuffer = (POUTPUT_FREE_TERMINAL)(Irp->AssociatedIrp.SystemBuffer);

    LONG_PTR TerminalIndex = (LONG_PTR)(InBuffer->TerminalHandle);
    VIRTUAL1553B_VALIDATE_TERMINAL_INDEX(TerminalIndex, OutBuffer, OutBufferSize, Irp);
    PVIRTUAL_TERMINAL VirtualTerminal = &Extension->VirtualTerminal[TerminalIndex];

    //////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////

    AcquireVirtualTerminal(VirtualTerminal);
    VIRTUAL1553B_VALIDATE_TERMINAL_OWNER(VirtualTerminal, OutBuffer, OutBufferSize, Irp);
    VIRTUAL1553B_VALIDATE_TERMINAL_IS_STOPPED(VirtualTerminal, OutBuffer, OutBufferSize, Irp);

    PVOID InterruptEvent = InterlockedExchangePointer(&VirtualTerminal->InterruptEvent, NULL);
    VirtualTerminal->ProcessIdOwner = NULL;

    ReleaseVirtualTerminal(VirtualTerminal);

    if (InterruptEvent)
    {
        ObDereferenceObject(InterruptEvent);
    }

    VIRTUAL1553B_SUCCESS_RETURN(OutBuffer, OutBufferSize, Irp);
}


//////////////////////////////////////////////////////////////////////////
//
// Function:            Virtual1553B_InstallEvent
//
// I/O control codes:   IOCTL_VIRTUAL1553B_INSTALL_EVENT
//
// Input buffer:        INPUT_INSTALL_EVENT
// Output buffer:       OUTPUT_INSTALL_EVENT
//
//
// Status:              VIRTUAL1553B_STATUS_SUCCESS
//                      VIRTUAL1553B_STATUS_INVALID_TERMINAL_HANDLE
//                      VIRTUAL1553B_STATUS_PROCESS_IS_NOT_OWNER
//                      VIRTUAL1553B_STATUS_EVENT_HANDLE_ACCESS_DENIED
//                      VIRTUAL1553B_STATUS_INVALID_EVENT_HANDLE
//
//////////////////////////////////////////////////////////////////////////
NTSTATUS Virtual1553B_InstallEvent(
    IN PVIRTUAL1553B_EXTENSION Extension,
    IN ULONG InputBufferLength,
    IN ULONG OutputBufferLength,
    IN PIRP Irp)
{
    Virtual1553B_DbgPrintEntry();

    ULONG InBufferSize = sizeof(INPUT_INSTALL_EVENT);
    VIRTUAL1553B_VALIDATE_INPUT_BUFFER_LENGTH(InputBufferLength, InBufferSize, Irp);
    PINPUT_INSTALL_EVENT InBuffer = (PINPUT_INSTALL_EVENT)(Irp->AssociatedIrp.SystemBuffer);

    ULONG OutBufferSize = sizeof(OUTPUT_INSTALL_EVENT);
    VIRTUAL1553B_VALIDATE_OUTPUT_BUFFER_LENGTH(OutputBufferLength, OutBufferSize, Irp);
    POUTPUT_INSTALL_EVENT OutBuffer = (POUTPUT_INSTALL_EVENT)(Irp->AssociatedIrp.SystemBuffer);

    LONG_PTR TerminalIndex = (LONG_PTR)(InBuffer->TerminalHandle);
    VIRTUAL1553B_VALIDATE_TERMINAL_INDEX(TerminalIndex, OutBuffer, OutBufferSize, Irp);
    PVIRTUAL_TERMINAL VirtualTerminal = &Extension->VirtualTerminal[TerminalIndex];

    //////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////

    AcquireVirtualTerminal(VirtualTerminal);
    VIRTUAL1553B_VALIDATE_TERMINAL_OWNER(VirtualTerminal, OutBuffer, OutBufferSize, Irp);

    NTSTATUS Status = STATUS_SUCCESS;

    PVOID InterruptEvent = NULL;
    Status = ObReferenceObjectByHandle(
        InBuffer->Event,
        EVENT_MODIFY_STATE,
        *ExEventObjectType,
        Irp->RequestorMode,
        &InterruptEvent,
        NULL);

    if (!NT_SUCCESS(Status))
    {
        ReleaseVirtualTerminal(VirtualTerminal);
        switch (Status)
        {
        case STATUS_ACCESS_DENIED:
            VIRTUAL1553B_EVENT_HANDLE_ACCESS_DENIED_RETURN(OutBuffer, OutBufferSize, Irp);
        default:
            VIRTUAL1553B_INVALID_EVENT_HANDLE_RETURN(OutBuffer, OutBufferSize, Irp);
        }
    }

    InterruptEvent = InterlockedExchangePointer(&VirtualTerminal->InterruptEvent, InterruptEvent);

    ReleaseVirtualTerminal(VirtualTerminal);

    if (InterruptEvent)
    {
        ObDereferenceObject(InterruptEvent);
    }

    VIRTUAL1553B_SUCCESS_RETURN(OutBuffer, OutBufferSize, Irp);
}


//////////////////////////////////////////////////////////////////////////
//
// Function:            Virtual1553B_RemoveEvent
//
// I/O control codes:   IOCTL_VIRTUAL1553B_REMOVE_EVENT
//
// Input buffer:        INPUT_REMOVE_EVENT
// Output buffer:       OUTPUT_REMOVE_EVENT
//
//
// Status:              VIRTUAL1553B_STATUS_SUCCESS
//                      VIRTUAL1553B_STATUS_INVALID_TERMINAL_HANDLE
//                      VIRTUAL1553B_STATUS_PROCESS_IS_NOT_OWNER
//                      VIRTUAL1553B_STATUS_EVENT_HANDLE_IS_NOT_INSTALLED
//
//////////////////////////////////////////////////////////////////////////
NTSTATUS Virtual1553B_RemoveEvent(
    IN PVIRTUAL1553B_EXTENSION Extension,
    IN ULONG InputBufferLength,
    IN ULONG OutputBufferLength,
    IN PIRP Irp)
{
    Virtual1553B_DbgPrintEntry();

    ULONG InBufferSize = sizeof(INPUT_REMOVE_EVENT);
    VIRTUAL1553B_VALIDATE_INPUT_BUFFER_LENGTH(InputBufferLength, InBufferSize, Irp);
    PINPUT_REMOVE_EVENT InBuffer = (PINPUT_REMOVE_EVENT)(Irp->AssociatedIrp.SystemBuffer);

    ULONG OutBufferSize = sizeof(OUTPUT_REMOVE_EVENT);
    VIRTUAL1553B_VALIDATE_OUTPUT_BUFFER_LENGTH(OutputBufferLength, OutBufferSize, Irp);
    POUTPUT_REMOVE_EVENT OutBuffer = (POUTPUT_REMOVE_EVENT)(Irp->AssociatedIrp.SystemBuffer);

    LONG_PTR TerminalIndex = (LONG_PTR)(InBuffer->TerminalHandle);
    VIRTUAL1553B_VALIDATE_TERMINAL_INDEX(TerminalIndex, OutBuffer, OutBufferSize, Irp);
    PVIRTUAL_TERMINAL VirtualTerminal = &Extension->VirtualTerminal[TerminalIndex];

    //////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////

    AcquireVirtualTerminal(VirtualTerminal);
    VIRTUAL1553B_VALIDATE_TERMINAL_OWNER(VirtualTerminal, OutBuffer, OutBufferSize, Irp);

    PVOID InterruptEvent = InterlockedExchangePointer(&VirtualTerminal->InterruptEvent, NULL);

    ReleaseVirtualTerminal(VirtualTerminal);

    if (InterruptEvent)
    {
        ObDereferenceObject(InterruptEvent);

        VIRTUAL1553B_SUCCESS_RETURN(OutBuffer, OutBufferSize, Irp);
    }

    VIRTUAL1553B_EVENT_HANDLE_IS_NOT_INSTALLED_RETURN(OutBuffer, OutBufferSize, Irp);
}


//////////////////////////////////////////////////////////////////////////
//
// Function:            Virtual1553B_GetTerminalMode
//
// I/O control codes:   IOCTL_VIRTUAL1553B_GET_TERMINAL_MODE
//
// Input buffer:        INPUT_GET_TERMINAL_MODE
// Output buffer:       OUTPUT_GET_TERMINAL_MODE
//
//
// Status:              VIRTUAL1553B_STATUS_SUCCESS
//                      VIRTUAL1553B_STATUS_INVALID_TERMINAL_HANDLE
//                      VIRTUAL1553B_STATUS_PROCESS_IS_NOT_OWNER
//
//////////////////////////////////////////////////////////////////////////
NTSTATUS Virtual1553B_GetTerminalMode(
    IN PVIRTUAL1553B_EXTENSION Extension,
    IN ULONG InputBufferLength,
    IN ULONG OutputBufferLength,
    IN PIRP Irp)
{
    Virtual1553B_DbgPrintEntry();

    ULONG InBufferSize = sizeof(INPUT_GET_TERMINAL_MODE);
    VIRTUAL1553B_VALIDATE_INPUT_BUFFER_LENGTH(InputBufferLength, InBufferSize, Irp);
    PINPUT_GET_TERMINAL_MODE InBuffer = (PINPUT_GET_TERMINAL_MODE)(Irp->AssociatedIrp.SystemBuffer);

    ULONG OutBufferSize = sizeof(OUTPUT_GET_TERMINAL_MODE);
    VIRTUAL1553B_VALIDATE_OUTPUT_BUFFER_LENGTH(OutputBufferLength, OutBufferSize, Irp);
    POUTPUT_GET_TERMINAL_MODE OutBuffer = (POUTPUT_GET_TERMINAL_MODE)(Irp->AssociatedIrp.SystemBuffer);

    OutBuffer->TerminalMode = UndefinedMode;

    LONG_PTR TerminalIndex = (LONG_PTR)(InBuffer->TerminalHandle);
    VIRTUAL1553B_VALIDATE_TERMINAL_INDEX(TerminalIndex, OutBuffer, OutBufferSize, Irp);
    PVIRTUAL_TERMINAL VirtualTerminal = &Extension->VirtualTerminal[TerminalIndex];

    //////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////

    AcquireVirtualTerminal(VirtualTerminal);
    VIRTUAL1553B_VALIDATE_TERMINAL_OWNER(VirtualTerminal, OutBuffer, OutBufferSize, Irp);

    OutBuffer->TerminalMode = VirtualTerminal->Mode;

    ReleaseVirtualTerminal(VirtualTerminal);
    VIRTUAL1553B_SUCCESS_RETURN(OutBuffer, OutBufferSize, Irp);
}


//////////////////////////////////////////////////////////////////////////
//
// Function:            Virtual1553B_StartBusController
//
// I/O control codes:   IOCTL_VIRTUAL1553B_START_BUS_CONTROLLER
//
// Input buffer:        INPUT_START_BUS_CONTROLLER
// Output buffer:       OUTPUT_START_BUS_CONTROLLER
//
//
// Status:              VIRTUAL1553B_STATUS_SUCCESS
//                      VIRTUAL1553B_STATUS_INVALID_TERMINAL_HANDLE
//                      VIRTUAL1553B_STATUS_PROCESS_IS_NOT_OWNER
//                      VIRTUAL1553B_STATUS_TERMINAL_IS_ALREADY_STARTED
//                      VIRTUAL1553B_STATUS_NOT_ENOUGH_MEMORY
//                      VIRTUAL1553B_STATUS_CREATE_WORKER_THREADS_FAILED
//
//////////////////////////////////////////////////////////////////////////
NTSTATUS Virtual1553B_StartBusController(
    IN PVIRTUAL1553B_EXTENSION Extension,
    IN ULONG InputBufferLength,
    IN ULONG OutputBufferLength,
    IN PIRP Irp)
{
    Virtual1553B_DbgPrintEntry();

    ULONG InBufferSize = sizeof(INPUT_START_BUS_CONTROLLER);
    VIRTUAL1553B_VALIDATE_INPUT_BUFFER_LENGTH(InputBufferLength, InBufferSize, Irp);
    PINPUT_START_BUS_CONTROLLER InBuffer = (PINPUT_START_BUS_CONTROLLER)(Irp->AssociatedIrp.SystemBuffer);

    ULONG OutBufferSize = sizeof(OUTPUT_START_BUS_CONTROLLER);
    VIRTUAL1553B_VALIDATE_OUTPUT_BUFFER_LENGTH(OutputBufferLength, OutBufferSize, Irp);
    POUTPUT_START_BUS_CONTROLLER OutBuffer = (POUTPUT_START_BUS_CONTROLLER)(Irp->AssociatedIrp.SystemBuffer);

    LONG_PTR TerminalIndex = (LONG_PTR)(InBuffer->TerminalHandle);
    VIRTUAL1553B_VALIDATE_TERMINAL_INDEX(TerminalIndex, OutBuffer, OutBufferSize, Irp);
    PVIRTUAL_TERMINAL VirtualTerminal = &Extension->VirtualTerminal[TerminalIndex];

    //////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////

    AcquireVirtualTerminal(VirtualTerminal);
    VIRTUAL1553B_VALIDATE_TERMINAL_OWNER(VirtualTerminal, OutBuffer, OutBufferSize, Irp);
    VIRTUAL1553B_VALIDATE_TERMINAL_IS_NOT_STARTED(VirtualTerminal, OutBuffer, OutBufferSize, Irp);

    PVIRTUAL_BUS_CONTROLLER BusController = (PVIRTUAL_BUS_CONTROLLER)ExAllocatePoolWithTag(
        NonPagedPool, sizeof(VIRTUAL_BUS_CONTROLLER), 0);
    if (!BusController)
    {
        ReleaseVirtualTerminal(VirtualTerminal);
        VIRTUAL1553B_NOT_ENOUGH_MEMORY_RETURN(OutBuffer, OutBufferSize, Irp);
    }

    RtlZeroMemory(BusController, sizeof(VIRTUAL_BUS_CONTROLLER));

    VirtualTerminal->BusController = BusController;
    VirtualTerminal->Mode = BusControllerMode;

    if (!Virtual1553B_CreateWorkerThreads(VirtualTerminal, &Extension->Bus, &Extension->TerminalMap, VirtualBC_WorkerThread))
    {
        VirtualTerminal->Mode = UndefinedMode;
        VirtualTerminal->BusController = NULL;

        ReleaseVirtualTerminal(VirtualTerminal);
        ExFreePoolWithTag(BusController, 0);
        VIRTUAL1553B_CREATE_WORKER_THREADS_FAILED_RETURN(OutBuffer, OutBufferSize, Irp);
    }

    ReleaseVirtualTerminal(VirtualTerminal);
    VIRTUAL1553B_SUCCESS_RETURN(OutBuffer, OutBufferSize, Irp);
}


//////////////////////////////////////////////////////////////////////////
//
// Function:            Virtual1553B_StartRemoteTerminal
//
// I/O control codes:   IOCTL_VIRTUAL1553B_START_REMOTE_TERMINAL
//
// Input buffer:        INPUT_START_REMOTE_TERMINAL
// Output buffer:       OUTPUT_START_REMOTE_TERMINAL
//
//
// Status:              VIRTUAL1553B_STATUS_SUCCESS
//                      VIRTUAL1553B_STATUS_INVALID_TERMINAL_HANDLE
//                      VIRTUAL1553B_STATUS_PROCESS_IS_NOT_OWNER
//                      VIRTUAL1553B_STATUS_TERMINAL_IS_ALREADY_STARTED
//                      VIRTUAL1553B_STATUS_INVALID_REMOTE_TERMINAL_ADDRESS
//                      VIRTUAL1553B_STATUS_NOT_ENOUGH_MEMORY
//                      VIRTUAL1553B_STATUS_REGISTER_REMOTE_TERMINAL_FAILED
//
//////////////////////////////////////////////////////////////////////////
NTSTATUS Virtual1553B_StartRemoteTerminal(
    IN PVIRTUAL1553B_EXTENSION Extension,
    IN ULONG InputBufferLength,
    IN ULONG OutputBufferLength,
    IN PIRP Irp)
{
    Virtual1553B_DbgPrintEntry();

    ULONG InBufferSize = sizeof(INPUT_START_REMOTE_TERMINAL);
    VIRTUAL1553B_VALIDATE_INPUT_BUFFER_LENGTH(InputBufferLength, InBufferSize, Irp);
    PINPUT_START_REMOTE_TERMINAL InBuffer = (PINPUT_START_REMOTE_TERMINAL)(Irp->AssociatedIrp.SystemBuffer);

    ULONG OutBufferSize = sizeof(OUTPUT_START_REMOTE_TERMINAL);
    VIRTUAL1553B_VALIDATE_OUTPUT_BUFFER_LENGTH(OutputBufferLength, OutBufferSize, Irp);
    POUTPUT_START_REMOTE_TERMINAL OutBuffer = (POUTPUT_START_REMOTE_TERMINAL)(Irp->AssociatedIrp.SystemBuffer);

    LONG_PTR TerminalIndex = (LONG_PTR)(InBuffer->TerminalHandle);
    VIRTUAL1553B_VALIDATE_TERMINAL_INDEX(TerminalIndex, OutBuffer, OutBufferSize, Irp);
    PVIRTUAL_TERMINAL VirtualTerminal = &Extension->VirtualTerminal[TerminalIndex];

    //////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////

    AcquireVirtualTerminal(VirtualTerminal);
    VIRTUAL1553B_VALIDATE_TERMINAL_OWNER(VirtualTerminal, OutBuffer, OutBufferSize, Irp);
    VIRTUAL1553B_VALIDATE_TERMINAL_IS_NOT_STARTED(VirtualTerminal, OutBuffer, OutBufferSize, Irp);
    VIRTUAL1553B_VALIDATE_REMOTE_TERMINAL_ADDRESS(InBuffer->Address, VirtualTerminal, OutBuffer, OutBufferSize, Irp);

    PVIRTUAL_REMOTE_TERMINAL RemoteTerminal = (PVIRTUAL_REMOTE_TERMINAL)ExAllocatePoolWithTag(
        NonPagedPool, sizeof(VIRTUAL_REMOTE_TERMINAL), 0);
    if (!RemoteTerminal)
    {
        ReleaseVirtualTerminal(VirtualTerminal);
        VIRTUAL1553B_NOT_ENOUGH_MEMORY_RETURN(OutBuffer, OutBufferSize, Irp);
    }

    RtlZeroMemory(RemoteTerminal, sizeof(VIRTUAL_REMOTE_TERMINAL));

    VirtualTerminal->RemoteTerminal = RemoteTerminal;
    VirtualTerminal->Mode = RemoteTerminalMode;

    if (!Virtual1553B_RegisterRemoteTerminal(&Extension->TerminalMap, VirtualTerminal, InBuffer->Address, FALSE))
    {
        VirtualTerminal->Mode = UndefinedMode;
        VirtualTerminal->RemoteTerminal = NULL;

        ReleaseVirtualTerminal(VirtualTerminal);
        ExFreePoolWithTag(RemoteTerminal, 0);
        VIRTUAL1553B_REGISTER_REMOTE_TERMINAL_FAILED_RETURN(OutBuffer, OutBufferSize, Irp);
    }

    ReleaseVirtualTerminal(VirtualTerminal);
    VIRTUAL1553B_SUCCESS_RETURN(OutBuffer, OutBufferSize, Irp);
}


//////////////////////////////////////////////////////////////////////////
//
// Function:            Virtual1553B_StartBusMonitor
//
// I/O control codes:   IOCTL_VIRTUAL1553B_START_BUS_MONITOR
//
// Input buffer:        INPUT_START_BUS_MONITOR
// Output buffer:       OUTPUT_START_BUS_MONITOR
//
//
// Status:              VIRTUAL1553B_STATUS_SUCCESS
//                      VIRTUAL1553B_STATUS_INVALID_TERMINAL_HANDLE
//                      VIRTUAL1553B_STATUS_PROCESS_IS_NOT_OWNER
//                      VIRTUAL1553B_STATUS_TERMINAL_IS_ALREADY_STARTED
//                      VIRTUAL1553B_STATUS_NOT_ENOUGH_MEMORY
//                      VIRTUAL1553B_STATUS_REGISTER_BUS_MONITOR_FAILED
//
//////////////////////////////////////////////////////////////////////////
NTSTATUS Virtual1553B_StartBusMonitor(
    IN PVIRTUAL1553B_EXTENSION Extension,
    IN ULONG InputBufferLength,
    IN ULONG OutputBufferLength,
    IN PIRP Irp)
{
    Virtual1553B_DbgPrintEntry();

    ULONG InBufferSize = sizeof(INPUT_START_BUS_MONITOR);
    VIRTUAL1553B_VALIDATE_INPUT_BUFFER_LENGTH(InputBufferLength, InBufferSize, Irp);
    PINPUT_START_BUS_MONITOR InBuffer = (PINPUT_START_BUS_MONITOR)(Irp->AssociatedIrp.SystemBuffer);

    ULONG OutBufferSize = sizeof(OUTPUT_START_BUS_MONITOR);
    VIRTUAL1553B_VALIDATE_OUTPUT_BUFFER_LENGTH(OutputBufferLength, OutBufferSize, Irp);
    POUTPUT_START_BUS_MONITOR OutBuffer = (POUTPUT_START_BUS_MONITOR)(Irp->AssociatedIrp.SystemBuffer);

    LONG_PTR TerminalIndex = (LONG_PTR)(InBuffer->TerminalHandle);
    VIRTUAL1553B_VALIDATE_TERMINAL_INDEX(TerminalIndex, OutBuffer, OutBufferSize, Irp);
    PVIRTUAL_TERMINAL VirtualTerminal = &Extension->VirtualTerminal[TerminalIndex];

    //////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////

    AcquireVirtualTerminal(VirtualTerminal);
    VIRTUAL1553B_VALIDATE_TERMINAL_OWNER(VirtualTerminal, OutBuffer, OutBufferSize, Irp);
    VIRTUAL1553B_VALIDATE_TERMINAL_IS_NOT_STARTED(VirtualTerminal, OutBuffer, OutBufferSize, Irp);

    PVIRTUAL_BUS_MONITOR BusMonitor = (PVIRTUAL_BUS_MONITOR)ExAllocatePoolWithTag(
        NonPagedPool, sizeof(VIRTUAL_BUS_MONITOR), 0);
    if (!BusMonitor)
    {
        ReleaseVirtualTerminal(VirtualTerminal);
        VIRTUAL1553B_NOT_ENOUGH_MEMORY_RETURN(OutBuffer, OutBufferSize, Irp);
    }

    RtlZeroMemory(BusMonitor, sizeof(VIRTUAL_BUS_MONITOR));

    VirtualTerminal->BusMonitor = BusMonitor;
    VirtualTerminal->Mode = BusMonitorMode;

    if (!Virtual1553B_RegisterBusMonitor(&Extension->TerminalMap, VirtualTerminal))
    {
        VirtualTerminal->Mode = UndefinedMode;
        VirtualTerminal->BusMonitor = NULL;

        ReleaseVirtualTerminal(VirtualTerminal);
        ExFreePoolWithTag(BusMonitor, 0);
        VIRTUAL1553B_REGISTER_BUS_MONITOR_FAILED_RETURN(OutBuffer, OutBufferSize, Irp);
    }

    ReleaseVirtualTerminal(VirtualTerminal);
    VIRTUAL1553B_SUCCESS_RETURN(OutBuffer, OutBufferSize, Irp);
}


//////////////////////////////////////////////////////////////////////////
//
// Function:            Virtual1553B_StopTerminal
//
// I/O control codes:   IOCTL_VIRTUAL1553B_STOP_TERMINAL
//
// Input buffer:        INPUT_STOP_TERMINAL
// Output buffer:       OUTPUT_STOP_TERMINAL
//
//
// Status:              VIRTUAL1553B_STATUS_SUCCESS
//                      VIRTUAL1553B_STATUS_INVALID_TERMINAL_HANDLE
//                      VIRTUAL1553B_STATUS_PROCESS_IS_NOT_OWNER
//                      VIRTUAL1553B_STATUS_TERMINAL_IS_NOT_STARTED
//
//////////////////////////////////////////////////////////////////////////
NTSTATUS Virtual1553B_StopTerminal(
    IN PVIRTUAL1553B_EXTENSION Extension,
    IN ULONG InputBufferLength,
    IN ULONG OutputBufferLength,
    IN PIRP Irp)
{
    Virtual1553B_DbgPrintEntry();

    ULONG InBufferSize = sizeof(INPUT_STOP_TERMINAL);
    VIRTUAL1553B_VALIDATE_INPUT_BUFFER_LENGTH(InputBufferLength, InBufferSize, Irp);
    PINPUT_STOP_TERMINAL InBuffer = (PINPUT_STOP_TERMINAL)(Irp->AssociatedIrp.SystemBuffer);

    ULONG OutBufferSize = sizeof(OUTPUT_STOP_TERMINAL);
    VIRTUAL1553B_VALIDATE_OUTPUT_BUFFER_LENGTH(OutputBufferLength, OutBufferSize, Irp);
    POUTPUT_STOP_TERMINAL OutBuffer = (POUTPUT_STOP_TERMINAL)(Irp->AssociatedIrp.SystemBuffer);

    LONG_PTR TerminalIndex = (LONG_PTR)(InBuffer->TerminalHandle);
    VIRTUAL1553B_VALIDATE_TERMINAL_INDEX(TerminalIndex, OutBuffer, OutBufferSize, Irp);
    PVIRTUAL_TERMINAL VirtualTerminal = &Extension->VirtualTerminal[TerminalIndex];

    //////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////

    AcquireVirtualTerminal(VirtualTerminal);
    VIRTUAL1553B_VALIDATE_TERMINAL_OWNER(VirtualTerminal, OutBuffer, OutBufferSize, Irp);
    VIRTUAL1553B_VALIDATE_TERMINAL_IS_STARTED(VirtualTerminal, OutBuffer, OutBufferSize, Irp);

    switch (VirtualTerminal->Mode)
    {
    case BusControllerMode:
        Virtual1553B_CloseWorkerThreads(VirtualTerminal);
        break;

    case RemoteTerminalMode:
        Virtual1553B_RevokeRemoteTerminal(&Extension->TerminalMap, VirtualTerminal);
        break;

    case BusMonitorMode:
        Virtual1553B_RevokeBusMonitor(&Extension->TerminalMap, VirtualTerminal);
        break;
    }

    VirtualTerminal->Mode = UndefinedMode;
    PVOID TerminalPointer = InterlockedExchangePointer(&VirtualTerminal->TerminalPointer, NULL);

    ReleaseVirtualTerminal(VirtualTerminal);
    ExFreePoolWithTag(TerminalPointer, 0);
    VIRTUAL1553B_SUCCESS_RETURN(OutBuffer, OutBufferSize, Irp);
}


//////////////////////////////////////////////////////////////////////////
//
// Function:            VirtualBC_ExecuteTransfer
//
// I/O control codes:   IOCTL_VIRTUAL1553B_BC_EXECUTE_TRANSFER
//
// Input buffer:        INPUT_BC_EXECUTE_TRANSFER
// Output buffer:       OUTPUT_BC_EXECUTE_TRANSFER
//
//
// Status:              VIRTUAL1553B_STATUS_SUCCESS
//                      VIRTUAL1553B_STATUS_INVALID_TERMINAL_HANDLE
//                      VIRTUAL1553B_STATUS_PROCESS_IS_NOT_OWNER
//                      VIRTUAL1553B_STATUS_TERMINAL_MODE_MISMATCH
//                      VIRTUAL1553B_STATUS_INVALID_TRANSFER_LINE
//                      VIRTUAL1553B_STATUS_TRANSFER_LINE_IS_BUSY
//                      VIRTUAL1553B_STATUS_UNKNOWN_MESSAGE_FORMAT
//
//////////////////////////////////////////////////////////////////////////
NTSTATUS VirtualBC_ExecuteTransfer(
    IN PVIRTUAL1553B_EXTENSION Extension,
    IN ULONG InputBufferLength,
    IN ULONG OutputBufferLength,
    IN PIRP Irp)
{
    Virtual1553B_DbgPrintEntry();

    ULONG InBufferSize = sizeof(INPUT_BC_EXECUTE_TRANSFER);
    VIRTUAL1553B_VALIDATE_INPUT_BUFFER_LENGTH(InputBufferLength, InBufferSize, Irp);
    PINPUT_BC_EXECUTE_TRANSFER InBuffer = (PINPUT_BC_EXECUTE_TRANSFER)(Irp->AssociatedIrp.SystemBuffer);

    ULONG OutBufferSize = sizeof(OUTPUT_BC_EXECUTE_TRANSFER);
    VIRTUAL1553B_VALIDATE_OUTPUT_BUFFER_LENGTH(OutputBufferLength, OutBufferSize, Irp);
    POUTPUT_BC_EXECUTE_TRANSFER OutBuffer = (POUTPUT_BC_EXECUTE_TRANSFER)(Irp->AssociatedIrp.SystemBuffer);

    LONG_PTR TerminalIndex = (LONG_PTR)(InBuffer->TerminalHandle);
    VIRTUAL1553B_VALIDATE_TERMINAL_INDEX(TerminalIndex, OutBuffer, OutBufferSize, Irp);
    PVIRTUAL_TERMINAL VirtualTerminal = &Extension->VirtualTerminal[TerminalIndex];

    //////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////

    AcquireVirtualTerminal(VirtualTerminal);
    VIRTUAL1553B_VALIDATE_TERMINAL_OWNER(VirtualTerminal, OutBuffer, OutBufferSize, Irp);
    VIRTUAL1553B_VALIDATE_TERMINAL_MODE(BusControllerMode, VirtualTerminal, OutBuffer, OutBufferSize, Irp);
    VIRTUAL1553B_VALIDATE_TRANSFER_LINE(InBuffer->TransferLine, VirtualTerminal, OutBuffer, OutBufferSize, Irp);

    PVIRTUAL1553B_TRANSFER_LINE Line = &Extension->Bus.Line[InBuffer->TransferLine];

    switch (InBuffer->ExecuteTransferBuffer.Format)
    {
    case TransferBCRT:
    case TransferBCRTBroadcast:
        VIRTUAL1553B_TRY_LOCK_TRANSFER_LINE(Line, VirtualTerminal, OutBuffer, OutBufferSize, Irp);
        Line->CW1 = InBuffer->ExecuteTransferBuffer.CW1;
        RtlCopyMemory(Line->Data, InBuffer->ExecuteTransferBuffer.Data, GET_DATA_WORD_COUNT(Line->CW1.Raw) * sizeof(USHORT));
        break;

    case TransferRTBC:
        VIRTUAL1553B_TRY_LOCK_TRANSFER_LINE(Line, VirtualTerminal, OutBuffer, OutBufferSize, Irp);
        Line->CW1 = InBuffer->ExecuteTransferBuffer.CW1;
        break;

    case TransferRTRT:
    case TransferRTRTBroadcast:
        VIRTUAL1553B_TRY_LOCK_TRANSFER_LINE(Line, VirtualTerminal, OutBuffer, OutBufferSize, Irp);
        Line->CW1 = InBuffer->ExecuteTransferBuffer.CW1;
        Line->CW2 = InBuffer->ExecuteTransferBuffer.CW2;
        break;

    case ModeCommandWithoutData:
    case ModeCommandWithoutDataBroadcast:
        VIRTUAL1553B_TRY_LOCK_TRANSFER_LINE(Line, VirtualTerminal, OutBuffer, OutBufferSize, Irp);
        Line->CW1 = InBuffer->ExecuteTransferBuffer.CW1;
        break;

    case ModeCommandWithDataReceive:
    case ModeCommandWithDataReceiveBroadcast:
        VIRTUAL1553B_TRY_LOCK_TRANSFER_LINE(Line, VirtualTerminal, OutBuffer, OutBufferSize, Irp);
        Line->CW1 = InBuffer->ExecuteTransferBuffer.CW1;
        Line->Data[0] = InBuffer->ExecuteTransferBuffer.Data[0];
        break;

    case ModeCommandWithDataTransmit:
        VIRTUAL1553B_TRY_LOCK_TRANSFER_LINE(Line, VirtualTerminal, OutBuffer, OutBufferSize, Irp);
        Line->CW1 = InBuffer->ExecuteTransferBuffer.CW1;
        break;

    default:
        ReleaseVirtualTerminal(VirtualTerminal);
        VIRTUAL1553B_UNKNOWN_MESSAGE_FORMAT_RETURN(OutBuffer, OutBufferSize, Irp);
    }

    Line->Format = InBuffer->ExecuteTransferBuffer.Format;
    Line->Time = KeQueryPerformanceCounter(NULL);

    Virtual1553B_InitiateTransfer(VirtualTerminal, InBuffer->TransferLine);

    ReleaseVirtualTerminal(VirtualTerminal);
    VIRTUAL1553B_SUCCESS_RETURN(OutBuffer, OutBufferSize, Irp);
}


//////////////////////////////////////////////////////////////////////////
//
// Function:            VirtualBC_GetTransferResult
//
// I/O control codes:   IOCTL_VIRTUAL1553B_BC_GET_TRANSFER_RESULT
//
// Input buffer:        INPUT_BC_GET_TRANSFER_RESULT
// Output buffer:       OUTPUT_BC_GET_TRANSFER_RESULT
//
//
// Status:              VIRTUAL1553B_STATUS_SUCCESS
//                      VIRTUAL1553B_STATUS_INVALID_TERMINAL_HANDLE
//                      VIRTUAL1553B_STATUS_PROCESS_IS_NOT_OWNER
//                      VIRTUAL1553B_STATUS_TERMINAL_MODE_MISMATCH
//
//////////////////////////////////////////////////////////////////////////
NTSTATUS VirtualBC_GetTransferResult(
    IN PVIRTUAL1553B_EXTENSION Extension,
    IN ULONG InputBufferLength,
    IN ULONG OutputBufferLength,
    IN PIRP Irp)
{
    Virtual1553B_DbgPrintEntry();

    ULONG InBufferSize = sizeof(INPUT_BC_GET_TRANSFER_RESULT);
    VIRTUAL1553B_VALIDATE_INPUT_BUFFER_LENGTH(InputBufferLength, InBufferSize, Irp);
    PINPUT_BC_GET_TRANSFER_RESULT InBuffer = (PINPUT_BC_GET_TRANSFER_RESULT)(Irp->AssociatedIrp.SystemBuffer);

    ULONG OutBufferSize = sizeof(OUTPUT_BC_GET_TRANSFER_RESULT);
    VIRTUAL1553B_VALIDATE_OUTPUT_BUFFER_LENGTH(OutputBufferLength, OutBufferSize, Irp);
    POUTPUT_BC_GET_TRANSFER_RESULT OutBuffer = (POUTPUT_BC_GET_TRANSFER_RESULT)(Irp->AssociatedIrp.SystemBuffer);

    LONG_PTR TerminalIndex = (LONG_PTR)(InBuffer->TerminalHandle);
    VIRTUAL1553B_VALIDATE_TERMINAL_INDEX(TerminalIndex, OutBuffer, OutBufferSize, Irp);
    PVIRTUAL_TERMINAL VirtualTerminal = &Extension->VirtualTerminal[TerminalIndex];

    //////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////

    AcquireVirtualTerminal(VirtualTerminal);
    VIRTUAL1553B_VALIDATE_TERMINAL_OWNER(VirtualTerminal, OutBuffer, OutBufferSize, Irp);
    VIRTUAL1553B_VALIDATE_TERMINAL_MODE(BusControllerMode, VirtualTerminal, OutBuffer, OutBufferSize, Irp);

    OutBuffer->TransferResult = VirtualTerminal->BusController->TransferResult;

    ReleaseVirtualTerminal(VirtualTerminal);
    VIRTUAL1553B_SUCCESS_RETURN(OutBuffer, OutBufferSize, Irp);
}


//////////////////////////////////////////////////////////////////////////
//
// Function:            VirtualRT_SetAddress
//
// I/O control codes:   IOCTL_VIRTUAL1553B_RT_SET_ADDRESS
//
// Input buffer:        INPUT_RT_SET_ADDRESS
// Output buffer:       OUTPUT_RT_SET_ADDRESS
//
//
// Status:              VIRTUAL1553B_STATUS_SUCCESS
//                      VIRTUAL1553B_STATUS_INVALID_TERMINAL_HANDLE
//                      VIRTUAL1553B_STATUS_PROCESS_IS_NOT_OWNER
//                      VIRTUAL1553B_STATUS_TERMINAL_MODE_MISMATCH
//                      VIRTUAL1553B_STATUS_INVALID_REMOTE_TERMINAL_ADDRESS
//                      VIRTUAL1553B_STATUS_REMAP_REMOTE_TERMINAL_FAILED
//
//////////////////////////////////////////////////////////////////////////
NTSTATUS VirtualRT_SetAddress(
    IN PVIRTUAL1553B_EXTENSION Extension,
    IN ULONG InputBufferLength,
    IN ULONG OutputBufferLength,
    IN PIRP Irp)
{
    Virtual1553B_DbgPrintEntry();

    ULONG InBufferSize = sizeof(INPUT_RT_SET_ADDRESS);
    VIRTUAL1553B_VALIDATE_INPUT_BUFFER_LENGTH(InputBufferLength, InBufferSize, Irp);
    PINPUT_RT_SET_ADDRESS InBuffer = (PINPUT_RT_SET_ADDRESS)(Irp->AssociatedIrp.SystemBuffer);

    ULONG OutBufferSize = sizeof(OUTPUT_RT_SET_ADDRESS);
    VIRTUAL1553B_VALIDATE_OUTPUT_BUFFER_LENGTH(OutputBufferLength, OutBufferSize, Irp);
    POUTPUT_RT_SET_ADDRESS OutBuffer = (POUTPUT_RT_SET_ADDRESS)(Irp->AssociatedIrp.SystemBuffer);

    LONG_PTR TerminalIndex = (LONG_PTR)(InBuffer->TerminalHandle);
    VIRTUAL1553B_VALIDATE_TERMINAL_INDEX(TerminalIndex, OutBuffer, OutBufferSize, Irp);
    PVIRTUAL_TERMINAL VirtualTerminal = &Extension->VirtualTerminal[TerminalIndex];

    //////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////

    AcquireVirtualTerminal(VirtualTerminal);
    VIRTUAL1553B_VALIDATE_TERMINAL_OWNER(VirtualTerminal, OutBuffer, OutBufferSize, Irp);
    VIRTUAL1553B_VALIDATE_TERMINAL_MODE(RemoteTerminalMode, VirtualTerminal, OutBuffer, OutBufferSize, Irp);
    VIRTUAL1553B_VALIDATE_REMOTE_TERMINAL_ADDRESS(InBuffer->Address, VirtualTerminal, OutBuffer, OutBufferSize, Irp);

    if (!Virtual1553B_RegisterRemoteTerminal(&Extension->TerminalMap, VirtualTerminal, InBuffer->Address, TRUE))
    {
        ReleaseVirtualTerminal(VirtualTerminal);
        VIRTUAL1553B_REMAP_REMOTE_TERMINAL_FAILED_RETURN(OutBuffer, OutBufferSize, Irp);
    }

    ReleaseVirtualTerminal(VirtualTerminal);
    VIRTUAL1553B_SUCCESS_RETURN(OutBuffer, OutBufferSize, Irp);
}


//////////////////////////////////////////////////////////////////////////
//
// Function:            VirtualRT_GetAddress
//
// I/O control codes:   IOCTL_VIRTUAL1553B_RT_GET_ADDRESS
//
// Input buffer:        INPUT_RT_GET_ADDRESS
// Output buffer:       OUTPUT_RT_GET_ADDRESS
//
//
// Status:              VIRTUAL1553B_STATUS_SUCCESS
//                      VIRTUAL1553B_STATUS_INVALID_TERMINAL_HANDLE
//                      VIRTUAL1553B_STATUS_PROCESS_IS_NOT_OWNER
//                      VIRTUAL1553B_STATUS_TERMINAL_MODE_MISMATCH
//
//////////////////////////////////////////////////////////////////////////
NTSTATUS VirtualRT_GetAddress(
    IN PVIRTUAL1553B_EXTENSION Extension,
    IN ULONG InputBufferLength,
    IN ULONG OutputBufferLength,
    IN PIRP Irp)
{
    Virtual1553B_DbgPrintEntry();

    ULONG InBufferSize = sizeof(INPUT_RT_GET_ADDRESS);
    VIRTUAL1553B_VALIDATE_INPUT_BUFFER_LENGTH(InputBufferLength, InBufferSize, Irp);
    PINPUT_RT_GET_ADDRESS InBuffer = (PINPUT_RT_GET_ADDRESS)(Irp->AssociatedIrp.SystemBuffer);

    ULONG OutBufferSize = sizeof(OUTPUT_RT_GET_ADDRESS);
    VIRTUAL1553B_VALIDATE_OUTPUT_BUFFER_LENGTH(OutputBufferLength, OutBufferSize, Irp);
    POUTPUT_RT_GET_ADDRESS OutBuffer = (POUTPUT_RT_GET_ADDRESS)(Irp->AssociatedIrp.SystemBuffer);

    LONG_PTR TerminalIndex = (LONG_PTR)(InBuffer->TerminalHandle);
    VIRTUAL1553B_VALIDATE_TERMINAL_INDEX(TerminalIndex, OutBuffer, OutBufferSize, Irp);
    PVIRTUAL_TERMINAL VirtualTerminal = &Extension->VirtualTerminal[TerminalIndex];

    //////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////

    AcquireVirtualTerminal(VirtualTerminal);
    VIRTUAL1553B_VALIDATE_TERMINAL_OWNER(VirtualTerminal, OutBuffer, OutBufferSize, Irp);
    VIRTUAL1553B_VALIDATE_TERMINAL_MODE(RemoteTerminalMode, VirtualTerminal, OutBuffer, OutBufferSize, Irp);

    OutBuffer->Address = VirtualTerminal->RemoteTerminal->Address;

    ReleaseVirtualTerminal(VirtualTerminal);
    VIRTUAL1553B_SUCCESS_RETURN(OutBuffer, OutBufferSize, Irp);
}


//////////////////////////////////////////////////////////////////////////
//
// Function:            VirtualRT_WriteMemory
//
// I/O control codes:   IOCTL_VIRTUAL1553B_RT_WRITE_MEMORY
//
// Input buffer:        INPUT_RT_WRITE_MEMORY
// Output buffer:       OUTPUT_RT_WRITE_MEMORY
//
//
// Status:              VIRTUAL1553B_STATUS_SUCCESS
//                      VIRTUAL1553B_STATUS_INVALID_TERMINAL_HANDLE
//                      VIRTUAL1553B_STATUS_PROCESS_IS_NOT_OWNER
//                      VIRTUAL1553B_STATUS_TERMINAL_MODE_MISMATCH
//                      VIRTUAL1553B_STATUS_INVALID_TRANSFER_DIRECTION
//                      VIRTUAL1553B_STATUS_INVALID_SUBADDRESS
//                      VIRTUAL1553B_STATUS_INVALID_DATA_WORD_COUNT
//
//////////////////////////////////////////////////////////////////////////
NTSTATUS VirtualRT_WriteMemory(
    IN PVIRTUAL1553B_EXTENSION Extension,
    IN ULONG InputBufferLength,
    IN ULONG OutputBufferLength,
    IN PIRP Irp)
{
    Virtual1553B_DbgPrintEntry();

    ULONG InBufferSize = sizeof(INPUT_RT_WRITE_MEMORY);
    VIRTUAL1553B_VALIDATE_INPUT_BUFFER_LENGTH(InputBufferLength, InBufferSize, Irp);
    POUTPUT_RT_WRITE_MEMORY OutBuffer = (POUTPUT_RT_WRITE_MEMORY)(Irp->AssociatedIrp.SystemBuffer);

    ULONG OutBufferSize = sizeof(OUTPUT_RT_WRITE_MEMORY);
    VIRTUAL1553B_VALIDATE_OUTPUT_BUFFER_LENGTH(OutputBufferLength, OutBufferSize, Irp);
    PINPUT_RT_WRITE_MEMORY InBuffer = (PINPUT_RT_WRITE_MEMORY)(Irp->AssociatedIrp.SystemBuffer);

    LONG_PTR TerminalIndex = (LONG_PTR)(InBuffer->TerminalHandle);
    VIRTUAL1553B_VALIDATE_TERMINAL_INDEX(TerminalIndex, OutBuffer, OutBufferSize, Irp);
    PVIRTUAL_TERMINAL VirtualTerminal = &Extension->VirtualTerminal[TerminalIndex];

    //////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////

    AcquireVirtualTerminal(VirtualTerminal);
    VIRTUAL1553B_VALIDATE_TERMINAL_OWNER(VirtualTerminal, OutBuffer, OutBufferSize, Irp);
    VIRTUAL1553B_VALIDATE_TERMINAL_MODE(RemoteTerminalMode, VirtualTerminal, OutBuffer, OutBufferSize, Irp);
    VIRTUAL1553B_VALIDATE_TRANSFER_DIRECTION(InBuffer->Direction, VirtualTerminal, OutBuffer, OutBufferSize, Irp);
    VIRTUAL1553B_VALIDATE_SUBADDRESS(InBuffer->Subaddress, VirtualTerminal, OutBuffer, OutBufferSize, Irp);
    VIRTUAL1553B_VALIDATE_DATA_WORD_COUNT(InBuffer->WordCountToWrite, VirtualTerminal, OutBuffer, OutBufferSize, Irp);

    switch (InBuffer->Direction)
    {
    case DirectionReceive:
        RtlCopyMemory(GetReceiveBuffer(VirtualTerminal->RemoteTerminal, InBuffer->Subaddress),
            InBuffer->Buffer, InBuffer->WordCountToWrite * sizeof(USHORT));
        break;
    case DirectionTransmit:
        RtlCopyMemory(GetTransmitBuffer(VirtualTerminal->RemoteTerminal, InBuffer->Subaddress),
            InBuffer->Buffer, InBuffer->WordCountToWrite * sizeof(USHORT));
        break;
    }

    OutBuffer->WordCountWritten = InBuffer->WordCountToWrite;

    ReleaseVirtualTerminal(VirtualTerminal);
    VIRTUAL1553B_SUCCESS_RETURN(OutBuffer, OutBufferSize, Irp);
}


//////////////////////////////////////////////////////////////////////////
//
// Function:            VirtualRT_ReadMemory
//
// I/O control codes:   IOCTL_VIRTUAL1553B_RT_READ_MEMORY
//
// Input buffer:        INPUT_RT_READ_MEMORY
// Output buffer:       OUTPUT_RT_READ_MEMORY
//
//
// Status:              VIRTUAL1553B_STATUS_SUCCESS
//                      VIRTUAL1553B_STATUS_INVALID_TERMINAL_HANDLE
//                      VIRTUAL1553B_STATUS_PROCESS_IS_NOT_OWNER
//                      VIRTUAL1553B_STATUS_TERMINAL_MODE_MISMATCH
//                      VIRTUAL1553B_STATUS_INVALID_TRANSFER_DIRECTION
//                      VIRTUAL1553B_STATUS_INVALID_SUBADDRESS
//                      VIRTUAL1553B_STATUS_INVALID_DATA_WORD_COUNT
//
//////////////////////////////////////////////////////////////////////////
NTSTATUS VirtualRT_ReadMemory(
    IN PVIRTUAL1553B_EXTENSION Extension,
    IN ULONG InputBufferLength,
    IN ULONG OutputBufferLength,
    IN PIRP Irp)
{
    Virtual1553B_DbgPrintEntry();

    ULONG InBufferSize = sizeof(INPUT_RT_READ_MEMORY);
    VIRTUAL1553B_VALIDATE_INPUT_BUFFER_LENGTH(InputBufferLength, InBufferSize, Irp);
    PINPUT_RT_READ_MEMORY InBuffer = (PINPUT_RT_READ_MEMORY)(Irp->AssociatedIrp.SystemBuffer);

    ULONG OutBufferSize = sizeof(OUTPUT_RT_READ_MEMORY);
    VIRTUAL1553B_VALIDATE_OUTPUT_BUFFER_LENGTH(OutputBufferLength, OutBufferSize, Irp);
    POUTPUT_RT_READ_MEMORY OutBuffer = (POUTPUT_RT_READ_MEMORY)(Irp->AssociatedIrp.SystemBuffer);

    LONG_PTR TerminalIndex = (LONG_PTR)(InBuffer->TerminalHandle);
    VIRTUAL1553B_VALIDATE_TERMINAL_INDEX(TerminalIndex, OutBuffer, OutBufferSize, Irp);
    PVIRTUAL_TERMINAL VirtualTerminal = &Extension->VirtualTerminal[TerminalIndex];

    //////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////

    AcquireVirtualTerminal(VirtualTerminal);
    VIRTUAL1553B_VALIDATE_TERMINAL_OWNER(VirtualTerminal, OutBuffer, OutBufferSize, Irp);
    VIRTUAL1553B_VALIDATE_TERMINAL_MODE(RemoteTerminalMode, VirtualTerminal, OutBuffer, OutBufferSize, Irp);
    VIRTUAL1553B_VALIDATE_TRANSFER_DIRECTION(InBuffer->Direction, VirtualTerminal, OutBuffer, OutBufferSize, Irp);
    VIRTUAL1553B_VALIDATE_SUBADDRESS(InBuffer->Subaddress, VirtualTerminal, OutBuffer, OutBufferSize, Irp);
    VIRTUAL1553B_VALIDATE_DATA_WORD_COUNT(InBuffer->WordCountToRead, VirtualTerminal, OutBuffer, OutBufferSize, Irp);

    switch (InBuffer->Direction)
    {
    case DirectionReceive:
        RtlCopyMemory(OutBuffer->Buffer,
            GetReceiveBuffer(VirtualTerminal->RemoteTerminal, InBuffer->Subaddress),
            InBuffer->WordCountToRead * sizeof(USHORT));
        break;
    case DirectionTransmit:
        RtlCopyMemory(OutBuffer->Buffer,
            GetTransmitBuffer(VirtualTerminal->RemoteTerminal, InBuffer->Subaddress),
            InBuffer->WordCountToRead * sizeof(USHORT));
        break;
    }

    OutBuffer->WordCountRead = InBuffer->WordCountToRead;

    ReleaseVirtualTerminal(VirtualTerminal);
    VIRTUAL1553B_SUCCESS_RETURN(OutBuffer, OutBufferSize, Irp);
}


//////////////////////////////////////////////////////////////////////////
//
// Function:            VirtualRT_GetTransferResult
//
// I/O control codes:   IOCTL_VIRTUAL1553B_RT_GET_TRANSFER_RESULT
//
// Input buffer:        INPUT_RT_GET_TRANSFER_RESULT
// Output buffer:       OUTPUT_RT_GET_TRANSFER_RESULT
//
//
// Status:              VIRTUAL1553B_STATUS_SUCCESS
//                      VIRTUAL1553B_STATUS_INVALID_TERMINAL_HANDLE
//                      VIRTUAL1553B_STATUS_PROCESS_IS_NOT_OWNER
//                      VIRTUAL1553B_STATUS_TERMINAL_MODE_MISMATCH
//                      VIRTUAL1553B_STATUS_TRANSFER_RESULT_BUFFER_IS_EMPTY
//
//////////////////////////////////////////////////////////////////////////
NTSTATUS VirtualRT_GetTransferResult(
    IN PVIRTUAL1553B_EXTENSION Extension,
    IN ULONG InputBufferLength,
    IN ULONG OutputBufferLength,
    IN PIRP Irp)
{
    Virtual1553B_DbgPrintEntry();

    ULONG InBufferSize = sizeof(INPUT_RT_GET_TRANSFER_RESULT);
    VIRTUAL1553B_VALIDATE_INPUT_BUFFER_LENGTH(InputBufferLength, InBufferSize, Irp);
    PINPUT_RT_GET_TRANSFER_RESULT InBuffer = (PINPUT_RT_GET_TRANSFER_RESULT)(Irp->AssociatedIrp.SystemBuffer);

    ULONG OutBufferSize = sizeof(OUTPUT_RT_GET_TRANSFER_RESULT);
    VIRTUAL1553B_VALIDATE_OUTPUT_BUFFER_LENGTH(OutputBufferLength, OutBufferSize, Irp);
    POUTPUT_RT_GET_TRANSFER_RESULT OutBuffer = (POUTPUT_RT_GET_TRANSFER_RESULT)(Irp->AssociatedIrp.SystemBuffer);

    LONG_PTR TerminalIndex = (LONG_PTR)(InBuffer->TerminalHandle);
    VIRTUAL1553B_VALIDATE_TERMINAL_INDEX(TerminalIndex, OutBuffer, OutBufferSize, Irp);
    PVIRTUAL_TERMINAL VirtualTerminal = &Extension->VirtualTerminal[TerminalIndex];

    //////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////

    AcquireVirtualTerminal(VirtualTerminal);
    VIRTUAL1553B_VALIDATE_TERMINAL_OWNER(VirtualTerminal, OutBuffer, OutBufferSize, Irp);
    VIRTUAL1553B_VALIDATE_TERMINAL_MODE(RemoteTerminalMode, VirtualTerminal, OutBuffer, OutBufferSize, Irp);

    PVIRTUAL_REMOTE_TERMINAL RemoteTerminal = VirtualTerminal->RemoteTerminal;

    if (RemoteTerminal->Head == RemoteTerminal->Tail)
    {
        ReleaseVirtualTerminal(VirtualTerminal);
        VIRTUAL1553B_TRANSFER_RESULT_BUFFER_IS_EMPTY_RETURN(OutBuffer, OutBufferSize, Irp);
    }

    OutBuffer->TransferResult = RemoteTerminal->TransferResultBuffer[RemoteTerminal->Tail];

    RemoteTerminal->Tail = (RemoteTerminal->Tail + 1) % TRANSFER_RESULT_BUFFER_LENGTH;

    ReleaseVirtualTerminal(VirtualTerminal);
    VIRTUAL1553B_SUCCESS_RETURN(OutBuffer, OutBufferSize, Irp);
}


//////////////////////////////////////////////////////////////////////////
//
// Function:            VirtualBM_GetCurrentBase
//
// I/O control codes:   IOCTL_VIRTUAL1553B_BM_GET_CURRENT_BASE
//
// Input buffer:        INPUT_BM_GET_CURRENT_BASE
// Output buffer:       OUTPUT_BM_GET_CURRENT_BASE
//
//
// Status:              VIRTUAL1553B_STATUS_SUCCESS
//                      VIRTUAL1553B_STATUS_INVALID_TERMINAL_HANDLE
//                      VIRTUAL1553B_STATUS_PROCESS_IS_NOT_OWNER
//                      VIRTUAL1553B_STATUS_TERMINAL_MODE_MISMATCH
//
//////////////////////////////////////////////////////////////////////////
NTSTATUS VirtualBM_GetCurrentBase(
    IN PVIRTUAL1553B_EXTENSION Extension,
    IN ULONG InputBufferLength,
    IN ULONG OutputBufferLength,
    IN PIRP Irp)
{
    Virtual1553B_DbgPrintEntry();

    ULONG InBufferSize = sizeof(INPUT_BM_GET_CURRENT_BASE);
    VIRTUAL1553B_VALIDATE_INPUT_BUFFER_LENGTH(InputBufferLength, InBufferSize, Irp);
    PINPUT_BM_GET_CURRENT_BASE InBuffer = (PINPUT_BM_GET_CURRENT_BASE)(Irp->AssociatedIrp.SystemBuffer);

    ULONG OutBufferSize = sizeof(OUTPUT_BM_GET_CURRENT_BASE);
    VIRTUAL1553B_VALIDATE_OUTPUT_BUFFER_LENGTH(OutputBufferLength, OutBufferSize, Irp);
    POUTPUT_BM_GET_CURRENT_BASE OutBuffer = (POUTPUT_BM_GET_CURRENT_BASE)(Irp->AssociatedIrp.SystemBuffer);

    LONG_PTR TerminalIndex = (LONG_PTR)(InBuffer->TerminalHandle);
    VIRTUAL1553B_VALIDATE_TERMINAL_INDEX(TerminalIndex, OutBuffer, OutBufferSize, Irp);
    PVIRTUAL_TERMINAL VirtualTerminal = &Extension->VirtualTerminal[TerminalIndex];

    //////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////

    AcquireVirtualTerminal(VirtualTerminal);
    VIRTUAL1553B_VALIDATE_TERMINAL_OWNER(VirtualTerminal, OutBuffer, OutBufferSize, Irp);
    VIRTUAL1553B_VALIDATE_TERMINAL_MODE(BusMonitorMode, VirtualTerminal, OutBuffer, OutBufferSize, Irp);

    OutBuffer->CurrentBase = VirtualTerminal->BusMonitor->CurrentBase;

    ReleaseVirtualTerminal(VirtualTerminal);
    VIRTUAL1553B_SUCCESS_RETURN(OutBuffer, OutBufferSize, Irp);
}


//////////////////////////////////////////////////////////////////////////
//
// Function:            VirtualBM_GetBaseCount
//
// I/O control codes:   IOCTL_VIRTUAL1553B_BM_GET_BASE_COUNT
//
// Input buffer:        INPUT_BM_GET_BASE_COUNT
// Output buffer:       OUTPUT_BM_GET_BASE_COUNT
//
//
// Status:              VIRTUAL1553B_STATUS_SUCCESS
//                      VIRTUAL1553B_STATUS_INVALID_TERMINAL_HANDLE
//                      VIRTUAL1553B_STATUS_PROCESS_IS_NOT_OWNER
//                      VIRTUAL1553B_STATUS_TERMINAL_MODE_MISMATCH
//
//////////////////////////////////////////////////////////////////////////
NTSTATUS VirtualBM_GetBaseCount(
    IN PVIRTUAL1553B_EXTENSION Extension,
    IN ULONG InputBufferLength,
    IN ULONG OutputBufferLength,
    IN PIRP Irp)
{
    Virtual1553B_DbgPrintEntry();

    ULONG InBufferSize = sizeof(INPUT_BM_GET_BASE_COUNT);
    VIRTUAL1553B_VALIDATE_INPUT_BUFFER_LENGTH(InputBufferLength, InBufferSize, Irp);
    PINPUT_BM_GET_BASE_COUNT InBuffer = (PINPUT_BM_GET_BASE_COUNT)(Irp->AssociatedIrp.SystemBuffer);

    ULONG OutBufferSize = sizeof(OUTPUT_BM_GET_BASE_COUNT);
    VIRTUAL1553B_VALIDATE_OUTPUT_BUFFER_LENGTH(OutputBufferLength, OutBufferSize, Irp);
    POUTPUT_BM_GET_BASE_COUNT OutBuffer = (POUTPUT_BM_GET_BASE_COUNT)(Irp->AssociatedIrp.SystemBuffer);

    LONG_PTR TerminalIndex = (LONG_PTR)(InBuffer->TerminalHandle);
    VIRTUAL1553B_VALIDATE_TERMINAL_INDEX(TerminalIndex, OutBuffer, OutBufferSize, Irp);
    PVIRTUAL_TERMINAL VirtualTerminal = &Extension->VirtualTerminal[TerminalIndex];

    //////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////

    AcquireVirtualTerminal(VirtualTerminal);
    VIRTUAL1553B_VALIDATE_TERMINAL_OWNER(VirtualTerminal, OutBuffer, OutBufferSize, Irp);
    VIRTUAL1553B_VALIDATE_TERMINAL_MODE(BusMonitorMode, VirtualTerminal, OutBuffer, OutBufferSize, Irp);

    OutBuffer->BaseCount = TRANSFER_RECORD_BUFFER_LENGTH;

    ReleaseVirtualTerminal(VirtualTerminal);
    VIRTUAL1553B_SUCCESS_RETURN(OutBuffer, OutBufferSize, Irp);
}


//////////////////////////////////////////////////////////////////////////
//
// Function:            VirtualBM_GetTransferRecord
//
// I/O control codes:   IOCTL_VIRTUAL1553B_BM_GET_TRANSFER_RECORD
//
// Input buffer:        INPUT_BM_GET_TRANSFER_RECORD
// Output buffer:       OUTPUT_BM_GET_TRANSFER_RECORD
//
//
// Status:              VIRTUAL1553B_STATUS_SUCCESS
//                      VIRTUAL1553B_STATUS_INVALID_TERMINAL_HANDLE
//                      VIRTUAL1553B_STATUS_PROCESS_IS_NOT_OWNER
//                      VIRTUAL1553B_STATUS_TERMINAL_MODE_MISMATCH
//                      VIRTUAL1553B_STATUS_BASE_IS_OUT_OF_RANGE
//
//////////////////////////////////////////////////////////////////////////
NTSTATUS VirtualBM_GetTransferRecord(
    IN PVIRTUAL1553B_EXTENSION Extension,
    IN ULONG InputBufferLength,
    IN ULONG OutputBufferLength,
    IN PIRP Irp)
{
    Virtual1553B_DbgPrintEntry();

    ULONG InBufferSize = sizeof(INPUT_BM_GET_TRANSFER_RECORD);
    VIRTUAL1553B_VALIDATE_INPUT_BUFFER_LENGTH(InputBufferLength, InBufferSize, Irp);
    PINPUT_BM_GET_TRANSFER_RECORD InBuffer = (PINPUT_BM_GET_TRANSFER_RECORD)(Irp->AssociatedIrp.SystemBuffer);

    ULONG OutBufferSize = sizeof(OUTPUT_BM_GET_TRANSFER_RECORD);
    VIRTUAL1553B_VALIDATE_OUTPUT_BUFFER_LENGTH(OutputBufferLength, OutBufferSize, Irp);
    POUTPUT_BM_GET_TRANSFER_RECORD OutBuffer = (POUTPUT_BM_GET_TRANSFER_RECORD)(Irp->AssociatedIrp.SystemBuffer);

    LONG_PTR TerminalIndex = (LONG_PTR)(InBuffer->TerminalHandle);
    VIRTUAL1553B_VALIDATE_TERMINAL_INDEX(TerminalIndex, OutBuffer, OutBufferSize, Irp);
    PVIRTUAL_TERMINAL VirtualTerminal = &Extension->VirtualTerminal[TerminalIndex];

    //////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////

    AcquireVirtualTerminal(VirtualTerminal);
    VIRTUAL1553B_VALIDATE_TERMINAL_OWNER(VirtualTerminal, OutBuffer, OutBufferSize, Irp);
    VIRTUAL1553B_VALIDATE_TERMINAL_MODE(BusMonitorMode, VirtualTerminal, OutBuffer, OutBufferSize, Irp);
    VIRTUAL1553B_VALIDATE_BUS_MONITOR_BASE(InBuffer->TargetBase, VirtualTerminal, OutBuffer, OutBufferSize, Irp);

    OutBuffer->TransferRecord = VirtualTerminal->BusMonitor->TransferRecordBuffer[InBuffer->TargetBase];

    ReleaseVirtualTerminal(VirtualTerminal);
    VIRTUAL1553B_SUCCESS_RETURN(OutBuffer, OutBufferSize, Irp);
}
