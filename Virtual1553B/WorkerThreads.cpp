
#include "stdafx.h"
#include "WorkerThreads.h"


BOOLEAN Virtual1553B_CreateWorkerThreads(
    IN PVIRTUAL_TERMINAL VirtualTerminal,
    IN PVIRTUAL1553B_DATA_BUS Bus,
    IN PVIRTUAL_TERMINAL_MAP TerminalMap,
    IN PVIRTUAL1553B_WORKER_ROUTINE WorkerRoutine)
{
    Virtual1553B_DbgPrintEntry();

    PKEVENT StartEvent = (PKEVENT)ExAllocatePoolWithTag(NonPagedPool, sizeof(KEVENT), 0);
    if (!StartEvent)
    {
        Virtual1553B_DbgPrintExit_0("ExAllocatePoolWithTag() failed.");
        return FALSE;
    }

    PKEVENT TerminateEvent = (PKEVENT)ExAllocatePoolWithTag(NonPagedPool, sizeof(KEVENT), 0);
    if (!TerminateEvent)
    {
        ExFreePoolWithTag(StartEvent, 0);

        Virtual1553B_DbgPrintExit_0("ExAllocatePoolWithTag() failed.");
        return FALSE;
    }

    PVIRTUAL1553B_WORKER_THREAD WorkerThread = (PVIRTUAL1553B_WORKER_THREAD)ExAllocatePoolWithTag(
        NonPagedPool, sizeof(VIRTUAL1553B_WORKER_THREAD) * TransferLineNumber, 0);
    if (!WorkerThread)
    {
        ExFreePoolWithTag(TerminateEvent, 0);
        ExFreePoolWithTag(StartEvent, 0);

        Virtual1553B_DbgPrintExit_0("ExAllocatePoolWithTag() failed.");
        return FALSE;
    }

    RtlZeroMemory(StartEvent, sizeof(KEVENT));
    RtlZeroMemory(TerminateEvent, sizeof(KEVENT));
    RtlZeroMemory(WorkerThread, sizeof(VIRTUAL1553B_WORKER_THREAD) * TransferLineNumber);

    KeInitializeEvent(StartEvent, NotificationEvent, FALSE);
    KeInitializeEvent(TerminateEvent, NotificationEvent, FALSE);

    VirtualTerminal->StartEvent = StartEvent;
    VirtualTerminal->TerminateEvent = TerminateEvent;
    VirtualTerminal->WorkerThread = WorkerThread;

    for (USHORT j = 0; j < TransferLineNumber; ++j)
    {
        WorkerThread[j].Context.VirtualTerminal = VirtualTerminal;
        WorkerThread[j].Context.Line = &Bus->Line[j];
        WorkerThread[j].Context.TerminalMap = TerminalMap;

        KeInitializeEvent(&WorkerThread[j].Context.InitiateTransferEvent, NotificationEvent, FALSE);

        NTSTATUS Status = STATUS_SUCCESS;

        HANDLE ThreadHandle = NULL;
        Status = PsCreateSystemThread(
            &ThreadHandle,
            THREAD_ALL_ACCESS,
            NULL,
            NULL,
            NULL,
            (PKSTART_ROUTINE)(WorkerRoutine),
            &WorkerThread[j].Context);

        PKTHREAD ThreadObject = NULL;

        if (!NT_SUCCESS(Status))
        {
            KeSetEvent(TerminateEvent, IO_NO_INCREMENT, FALSE);

            for (USHORT i = 0; i < j; ++i)
            {
                ThreadObject = (PKTHREAD)InterlockedExchangePointer(&WorkerThread[i].ThreadObject, NULL);
                KeWaitForSingleObject(ThreadObject, Executive, KernelMode, FALSE, NULL);
                ObDereferenceObject(ThreadObject);
            }

            VirtualTerminal->WorkerThread = NULL;
            VirtualTerminal->TerminateEvent = NULL;
            VirtualTerminal->StartEvent = NULL;

            ExFreePoolWithTag(WorkerThread, 0);
            ExFreePoolWithTag(TerminateEvent, 0);
            ExFreePoolWithTag(StartEvent, 0);

            Virtual1553B_DbgPrintExit_1("PsCreateSystemThread() failed. Error: %08X.", Status);
            return FALSE;
        }

        ObReferenceObjectByHandle(
            ThreadHandle,
            THREAD_ALL_ACCESS,
            *PsThreadType,
            KernelMode,
            (PVOID *)(&ThreadObject),
            NULL);
        ZwClose(ThreadHandle);
        WorkerThread[j].ThreadObject = ThreadObject;
    }

    KeSetEvent(VirtualTerminal->StartEvent, IO_NO_INCREMENT, FALSE);

    Virtual1553B_DbgPrintExit_0("Success.");
    return TRUE;
}


VOID Virtual1553B_CloseWorkerThreads(
    IN PVIRTUAL_TERMINAL VirtualTerminal)
{
    Virtual1553B_DbgPrintEntry();

    KeSetEvent(VirtualTerminal->TerminateEvent, IO_NO_INCREMENT, FALSE);

    PVOID ThreadObject = NULL;
    for (USHORT i = 0; i < TransferLineNumber; ++i)
    {
        ThreadObject = InterlockedExchangePointer(&VirtualTerminal->WorkerThread[i].ThreadObject, NULL);
        KeWaitForSingleObject(ThreadObject, Executive, KernelMode, FALSE, NULL);
        ObDereferenceObject(ThreadObject);
    }

    PVOID WorkerThread = InterlockedExchangePointer(&VirtualTerminal->WorkerThread, NULL);
    PVOID TerminateEvent = InterlockedExchangePointer(&VirtualTerminal->TerminateEvent, NULL);
    PVOID StartEvent = InterlockedExchangePointer(&VirtualTerminal->StartEvent, NULL);

    ExFreePoolWithTag(WorkerThread, 0);
    ExFreePoolWithTag(TerminateEvent, 0);
    ExFreePoolWithTag(StartEvent, 0);

    Virtual1553B_DbgPrintExit();
}


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////


inline static VOID MapRemoteTerminal(
    IN PVIRTUAL_TERMINAL_MAP TerminalMap,
    IN PVIRTUAL_TERMINAL VirtualTerminal,
    IN ULONG Index)
{
    TerminalMap->RemoteTerminal[Index] = VirtualTerminal;
    BitTestAndSet((LONG *)(&TerminalMap->RemoteTerminalBitmap), (LONG)(Index));
    VirtualTerminal->BitmapIndex = Index;
}


inline static VOID UnmapRemoteTerminal(
    IN PVIRTUAL_TERMINAL_MAP TerminalMap,
    IN PVIRTUAL_TERMINAL VirtualTerminal)
{
    TerminalMap->RemoteTerminal[VirtualTerminal->BitmapIndex] = NULL;
    BitTestAndReset((LONG *)(&TerminalMap->RemoteTerminalBitmap), (LONG)(VirtualTerminal->BitmapIndex));
    VirtualTerminal->BitmapIndex = (ULONG)(-1);
}


inline static VOID MapBusMonitor(
    IN PVIRTUAL_TERMINAL_MAP TerminalMap,
    IN PVIRTUAL_TERMINAL VirtualTerminal,
    IN ULONG Index)
{
    TerminalMap->BusMonitor[Index] = VirtualTerminal;
    BitTestAndSet((LONG *)(&TerminalMap->BusMonitorBitmap), (LONG)(Index));
    VirtualTerminal->BitmapIndex = Index;
}


inline static VOID UnmapBusMonitor(
    IN PVIRTUAL_TERMINAL_MAP TerminalMap,
    IN PVIRTUAL_TERMINAL VirtualTerminal)
{
    TerminalMap->BusMonitor[VirtualTerminal->BitmapIndex] = NULL;
    BitTestAndReset((LONG *)(&TerminalMap->BusMonitorBitmap), (LONG)(VirtualTerminal->BitmapIndex));
    VirtualTerminal->BitmapIndex = (ULONG)(-1);
}


BOOLEAN Virtual1553B_RegisterRemoteTerminal(
    IN PVIRTUAL_TERMINAL_MAP TerminalMap,
    IN PVIRTUAL_TERMINAL VirtualTerminal,
    IN USHORT Address,
    IN BOOLEAN Remap)
{
    Virtual1553B_DbgPrintEntry();

    if ((Address < REMOTE_TERMINAL_ADDRESS_MINIMUM) || (Address > REMOTE_TERMINAL_ADDRESS_MAXIMUM))
    {
        Virtual1553B_DbgPrintExit_0("Failure: Invalid remote terminal address.");
        return FALSE;
    }

    AcquireTerminalMap(TerminalMap);

    if (TerminalMap->RemoteTerminal[Address])
    {
        ReleaseTerminalMap(TerminalMap);

        Virtual1553B_DbgPrintExit_0("Failure: Remote terminal address is already used.");
        return FALSE;
    }

    if (Remap)
    {
        UnmapRemoteTerminal(TerminalMap, VirtualTerminal);
    }

    MapRemoteTerminal(TerminalMap, VirtualTerminal, Address);

    VirtualTerminal->RemoteTerminal->Address = Address;
    VirtualTerminal->RemoteTerminal->StatusWord.RemoteTerminalAddress = Address;

    ReleaseTerminalMap(TerminalMap);

    Virtual1553B_DbgPrintExit_0("Success.");
    return TRUE;
}


VOID Virtual1553B_RevokeRemoteTerminal(
    IN PVIRTUAL_TERMINAL_MAP TerminalMap,
    IN PVIRTUAL_TERMINAL VirtualTerminal)
{
    Virtual1553B_DbgPrintEntry();

    AcquireTerminalMap(TerminalMap);
    UnmapRemoteTerminal(TerminalMap, VirtualTerminal);
    ReleaseTerminalMap(TerminalMap);

    Virtual1553B_DbgPrintExit();
}


BOOLEAN Virtual1553B_RegisterBusMonitor(
    IN PVIRTUAL_TERMINAL_MAP TerminalMap,
    IN PVIRTUAL_TERMINAL VirtualTerminal)
{
    Virtual1553B_DbgPrintEntry();

    AcquireTerminalMap(TerminalMap);

    ULONG InvertedBitmap = ~TerminalMap->BusMonitorBitmap;
    ULONG Index = (ULONG)(-1);

    if (BitScanForward(&Index, InvertedBitmap))
    {
        MapBusMonitor(TerminalMap, VirtualTerminal, Index);

        ReleaseTerminalMap(TerminalMap);

        Virtual1553B_DbgPrintExit_0("Success.");
        return TRUE;
    }

    ReleaseTerminalMap(TerminalMap);

    Virtual1553B_DbgPrintExit_0("Failure.");
    return FALSE;
}


VOID Virtual1553B_RevokeBusMonitor(
    IN PVIRTUAL_TERMINAL_MAP TerminalMap,
    IN PVIRTUAL_TERMINAL VirtualTerminal)
{
    Virtual1553B_DbgPrintEntry();

    AcquireTerminalMap(TerminalMap);
    UnmapBusMonitor(TerminalMap, VirtualTerminal);
    ReleaseTerminalMap(TerminalMap);

    Virtual1553B_DbgPrintExit();
}


VOID Virtual1553B_VisitRemoteTerminal(
    IN PVIRTUAL_TERMINAL_MAP TerminalMap,
    IN PVIRTUAL1553B_TRANSFER_LINE Line,
    IN PVIRTUAL1553B_VISITOR RemoteTerminalVisitor,
    IN USHORT Address)
{
    Virtual1553B_DbgPrintEntry();

    AcquireTerminalMap(TerminalMap);

    if (Address == BROADCAST_REMOTE_TERMINAL_ADDRESS)
    {
        ULONG Bitmap = TerminalMap->RemoteTerminalBitmap;
        ULONG Index = (ULONG)(-1);

        while (Bitmap && BitScanForward(&Index, Bitmap))
        {
            RemoteTerminalVisitor(TerminalMap->RemoteTerminal[Index], Line);

            BitTestAndReset((LONG *)(&Bitmap), (LONG)(Index));
        }
    }
    else
    {
        if (TerminalMap->RemoteTerminal[Address])
        {
            RemoteTerminalVisitor(TerminalMap->RemoteTerminal[Address], Line);
        }
    }

    ReleaseTerminalMap(TerminalMap);

    Virtual1553B_DbgPrintExit();
}


VOID Virtual1553B_VisitBusMonitors(
    IN PVIRTUAL_TERMINAL_MAP TerminalMap,
    IN PVIRTUAL1553B_TRANSFER_LINE Line,
    IN PVIRTUAL1553B_VISITOR BusMonitorVisitor)
{
    Virtual1553B_DbgPrintEntry();

    AcquireTerminalMap(TerminalMap);

    ULONG Bitmap = TerminalMap->BusMonitorBitmap;
    ULONG Index = (ULONG)(-1);

    while (Bitmap && BitScanForward(&Index, Bitmap))
    {
        BusMonitorVisitor(TerminalMap->BusMonitor[Index], Line);

        BitTestAndReset((LONG *)(&Bitmap), (LONG)(Index));
    }

    ReleaseTerminalMap(TerminalMap);

    Virtual1553B_DbgPrintExit();
}


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////


VOID VirtualBC_WorkerThread(IN PVIRTUAL1553B_CONTEXT Context)
{
    Virtual1553B_DbgPrintEntry();

    KeSetPriorityThread(KeGetCurrentThread(), LOW_REALTIME_PRIORITY);

    NTSTATUS Status = STATUS_SUCCESS;

    PVIRTUAL_TERMINAL VirtualTerminal = Context->VirtualTerminal;
    PVIRTUAL1553B_TRANSFER_LINE Line = Context->Line;

    PVOID WaitObjects[MaximumEventObject] = { 0 };
    WaitObjects[SynqEventObject] = VirtualTerminal->StartEvent;
    WaitObjects[TerminateEventObject] = VirtualTerminal->TerminateEvent;

    KWAIT_BLOCK WaitBlockArray[MaximumEventObject] = { 0 };

    Status = KeWaitForMultipleObjects(
        MaximumEventObject,
        WaitObjects,
        WaitAny,
        Executive,
        KernelMode,
        FALSE,
        NULL,
        WaitBlockArray);

    switch (Status)
    {
    case SynqEventObject:
        break;

    default:
        Virtual1553B_UnlockTransferLine(Line, VirtualTerminal);

        Virtual1553B_DbgPrintExit();
        PsTerminateSystemThread(STATUS_SUCCESS);
    }

    PBC_TRANSFER_RESULT TransferResult = &VirtualTerminal->BusController->TransferResult;

    WaitObjects[SynqEventObject] = &Context->InitiateTransferEvent;

    for ( ; ; )
    {
        Status = KeWaitForMultipleObjects(
            MaximumEventObject,
            WaitObjects,
            WaitAny,
            Executive,
            KernelMode,
            FALSE,
            NULL,
            WaitBlockArray);

        switch (Status)
        {
        case SynqEventObject:
            if (!KeResetEvent(&Context->InitiateTransferEvent))
            {
                continue;
            }
            break;

        default:
            Virtual1553B_UnlockTransferLine(Line, VirtualTerminal);

            Virtual1553B_DbgPrintExit();
            PsTerminateSystemThread(STATUS_SUCCESS);
        }

        switch (Line->Format)
        {
        case TransferBCRT:
            VirtualBC_TransferBCRT(Context);
            break;

        case TransferBCRTBroadcast:
            VirtualBC_TransferBCRTBroadcast(Context);
            break;

        case TransferRTBC:
            VirtualBC_TransferRTBC(Context);
            break;

        case TransferRTRT:
            VirtualBC_TransferRTRT(Context);
            break;

        case TransferRTRTBroadcast:
            VirtualBC_TransferRTRTBroadcast(Context);
            break;

        case ModeCommandWithoutData:
            VirtualBC_ModeCommandWithoutData(Context);
            break;

        case ModeCommandWithoutDataBroadcast:
            VirtualBC_ModeCommandWithoutDataBroadcast(Context);
            break;

        case ModeCommandWithDataReceive:
            VirtualBC_ModeCommandWithDataReceive(Context);
            break;

        case ModeCommandWithDataReceiveBroadcast:
            VirtualBC_ModeCommandWithDataReceiveBroadcast(Context);
            break;

        case ModeCommandWithDataTransmit:
            VirtualBC_ModeCommandWithDataTransmit(Context);
            break;

        default:
            Virtual1553B_DbgPrint_1("Unknown message format: %hu.", Line->Format);
            break;
        }

        TransferResult->Time = Line->Time;
        TransferResult->Error = Line->Error;
        TransferResult->SW1 = Line->SW1;
        TransferResult->SW2 = Line->SW2;
        RtlCopyMemory(TransferResult->Data, Line->Data, DATA_WORD_COUNT_MAXIMUM * sizeof(USHORT));

        Virtual1553B_VisitBusMonitors(Context->TerminalMap, Line, VirtualBM_SaveTransferResult);

        Virtual1553B_UnlockTransferLine(Line, VirtualTerminal);
        Virtual1553B_RaiseInterrupt(VirtualTerminal);
    }
}


VOID VirtualBC_TransferBCRT(IN PVIRTUAL1553B_CONTEXT Context)
{
    Virtual1553B_DbgPrintEntry();

    PVIRTUAL1553B_TRANSFER_LINE Line = Context->Line;

    KeStallExecutionProcessor(INTERVAL_TRANSFER_BC_RT_COMMAND(GET_DATA_WORD_COUNT(Line->CW1.Raw)));
    Virtual1553B_VisitRemoteTerminal(Context->TerminalMap, Line, VirtualRT_TransferBCRT, Line->CW1.RemoteTerminalAddress);

    if (Line->SW1.Raw == INVALID_STATUS_WORD)
    {
        Line->Error = VIRTUAL1553B_ERROR_NO_FIRST_RESPONSE;

        Virtual1553B_DbgPrintExit_0("Failure: No first response.");
        return;
    }

    if (Line->SW1.MessageError)
    {
        Line->Error = VIRTUAL1553B_ERROR_MESSAGE_ERROR;
        KeStallExecutionProcessor(INTERVAL_STATUS_WORD);

        Virtual1553B_DbgPrintExit_0("Failure: Message error.");
    }
    else
    {
        Line->Error = VIRTUAL1553B_ERROR_SUCCESS;
        KeStallExecutionProcessor(INTERVAL_TRANSFER_BC_RT_RESPONSE);

        Virtual1553B_DbgPrintExit_0("Success.");
    }
}


VOID VirtualBC_TransferBCRTBroadcast(IN PVIRTUAL1553B_CONTEXT Context)
{
    Virtual1553B_DbgPrintEntry();

    PVIRTUAL1553B_TRANSFER_LINE Line = Context->Line;

    KeStallExecutionProcessor(INTERVAL_TRANSFER_BC_RT_BROADCAST_COMMAND(GET_DATA_WORD_COUNT(Line->CW1.Raw)));
    Virtual1553B_VisitRemoteTerminal(Context->TerminalMap, Line, VirtualRT_TransferBCRTBroadcast, Line->CW1.RemoteTerminalAddress);
    Line->Error = VIRTUAL1553B_ERROR_SUCCESS;

    Virtual1553B_DbgPrintExit_0("Success.");
}


VOID VirtualBC_TransferRTBC(IN PVIRTUAL1553B_CONTEXT Context)
{
    Virtual1553B_DbgPrintEntry();

    PVIRTUAL1553B_TRANSFER_LINE Line = Context->Line;

    KeStallExecutionProcessor(INTERVAL_TRANSFER_RT_BC_COMMAND);
    Virtual1553B_VisitRemoteTerminal(Context->TerminalMap, Line, VirtualRT_TransferRTBC, Line->CW1.RemoteTerminalAddress);

    if (Line->SW1.Raw == INVALID_STATUS_WORD)
    {
        Line->Error = VIRTUAL1553B_ERROR_NO_FIRST_RESPONSE;

        Virtual1553B_DbgPrintExit_0("Failure: No first response.");
        return;
    }

    if (Line->SW1.MessageError)
    {
        Line->Error = VIRTUAL1553B_ERROR_MESSAGE_ERROR;
        KeStallExecutionProcessor(INTERVAL_STATUS_WORD);

        Virtual1553B_DbgPrintExit_0("Failure: Message error.");
    }
    else
    {
        Line->Error = VIRTUAL1553B_ERROR_SUCCESS;
        KeStallExecutionProcessor(INTERVAL_TRANSFER_RT_BC_RESPONSE(GET_DATA_WORD_COUNT(Line->CW1.Raw)));

        Virtual1553B_DbgPrintExit_0("Success.");
    }
}


VOID VirtualBC_TransferRTRT(IN PVIRTUAL1553B_CONTEXT Context)
{
    Virtual1553B_DbgPrintEntry();

    PVIRTUAL1553B_TRANSFER_LINE Line = Context->Line;

    KeStallExecutionProcessor(INTERVAL_TRANSFER_RT_RT_COMMAND);
    Virtual1553B_VisitRemoteTerminal(Context->TerminalMap, Line, VirtualRT_TransferRTRTFirst, Line->CW2.RemoteTerminalAddress);
    Virtual1553B_VisitRemoteTerminal(Context->TerminalMap, Line, VirtualRT_TransferRTRTSecond, Line->CW1.RemoteTerminalAddress);

    if (Line->SW1.Raw == INVALID_STATUS_WORD)
    {
        Line->Error = VIRTUAL1553B_ERROR_NO_FIRST_RESPONSE;

        Virtual1553B_DbgPrintExit_0("Failure: No first response.");
        return;
    }

    if (Line->SW1.MessageError)
    {
        Line->Error = VIRTUAL1553B_ERROR_MESSAGE_ERROR;
        KeStallExecutionProcessor(INTERVAL_STATUS_WORD);

        Virtual1553B_DbgPrintExit_0("Failure: Message error in the first response.");
        return;
    }
    else
    {
        KeStallExecutionProcessor(INTERVAL_TRANSFER_RT_RT_RESPONSE_1(GET_DATA_WORD_COUNT(Line->CW2.Raw)));
    }

    if (Line->SW2.Raw == INVALID_STATUS_WORD)
    {
        Line->Error = VIRTUAL1553B_ERROR_NO_SECOND_RESPONSE;

        Virtual1553B_DbgPrintExit_0("Failure: No second response.");
        return;
    }

    if (Line->SW2.MessageError)
    {
        Line->Error = VIRTUAL1553B_ERROR_MESSAGE_ERROR;
        KeStallExecutionProcessor(INTERVAL_STATUS_WORD);

        Virtual1553B_DbgPrintExit_0("Failure: Message error in the second response.");
    }
    else
    {
        Line->Error = VIRTUAL1553B_ERROR_SUCCESS;
        KeStallExecutionProcessor(INTERVAL_TRANSFER_RT_RT_RESPONSE_2);

        Virtual1553B_DbgPrintExit_0("Success.");
    }
}


VOID VirtualBC_TransferRTRTBroadcast(IN PVIRTUAL1553B_CONTEXT Context)
{
    Virtual1553B_DbgPrintEntry();

    PVIRTUAL1553B_TRANSFER_LINE Line = Context->Line;

    KeStallExecutionProcessor(INTERVAL_TRANSFER_RT_RT_BROADCAST_COMMAND);
    Virtual1553B_VisitRemoteTerminal(Context->TerminalMap, Line, VirtualRT_TransferRTRTBroadcastFirst, Line->CW2.RemoteTerminalAddress);
    Virtual1553B_VisitRemoteTerminal(Context->TerminalMap, Line, VirtualRT_TransferRTRTBroadcastSecond, Line->CW1.RemoteTerminalAddress);

    if (Line->SW1.Raw == INVALID_STATUS_WORD)
    {
        Line->Error = VIRTUAL1553B_ERROR_NO_FIRST_RESPONSE;

        Virtual1553B_DbgPrintExit_0("Failure: No first response.");
        return;
    }

    if (Line->SW1.MessageError)
    {
        Line->Error = VIRTUAL1553B_ERROR_MESSAGE_ERROR;
        KeStallExecutionProcessor(INTERVAL_STATUS_WORD);

        Virtual1553B_DbgPrintExit_0("Failure: Message error in the first response.");
    }
    else
    {
        Line->Error = VIRTUAL1553B_ERROR_SUCCESS;
        KeStallExecutionProcessor(INTERVAL_TRANSFER_RT_RT_BROADCAST_RESPONSE(GET_DATA_WORD_COUNT(Line->CW2.Raw)));

        Virtual1553B_DbgPrintExit_0("Success.");
    }
}


VOID VirtualBC_ModeCommandWithoutData(IN PVIRTUAL1553B_CONTEXT Context)
{
    Virtual1553B_DbgPrintEntry();

    PVIRTUAL1553B_TRANSFER_LINE Line = Context->Line;

    KeStallExecutionProcessor(INTERVAL_MODE_COMMAND_WITHOUT_DATA_COMMAND);
    Virtual1553B_VisitRemoteTerminal(Context->TerminalMap, Line, VirtualRT_ModeCommandWithoutData, Line->CW1.RemoteTerminalAddress);

    if (Line->SW1.Raw == INVALID_STATUS_WORD)
    {
        Line->Error = VIRTUAL1553B_ERROR_NO_FIRST_RESPONSE;

        Virtual1553B_DbgPrintExit_0("Failure: No first response.");
        return;
    }

    USHORT ModeCommand = GET_MODE_COMMAND(Line->CW1.Raw);

    if (Line->SW1.MessageError && (ModeCommand != CommandTransmitStatusWord))
    {
        Line->Error = VIRTUAL1553B_ERROR_MESSAGE_ERROR;
        KeStallExecutionProcessor(INTERVAL_STATUS_WORD);

        Virtual1553B_DbgPrintExit_0("Failure: Message error.");
    }
    else
    {
        Line->Error = VIRTUAL1553B_ERROR_SUCCESS;
        KeStallExecutionProcessor(INTERVAL_MODE_COMMAND_WITHOUT_DATA_RESPONSE);

        Virtual1553B_DbgPrintExit_0("Success.");
    }
}


VOID VirtualBC_ModeCommandWithoutDataBroadcast(IN PVIRTUAL1553B_CONTEXT Context)
{
    Virtual1553B_DbgPrintEntry();

    PVIRTUAL1553B_TRANSFER_LINE Line = Context->Line;

    KeStallExecutionProcessor(INTERVAL_MODE_COMMAND_WITHOUT_DATA_BROADCAST_COMMAND);
    Virtual1553B_VisitRemoteTerminal(Context->TerminalMap, Line, VirtualRT_ModeCommandWithoutDataBroadcast, Line->CW1.RemoteTerminalAddress);
    Line->Error = VIRTUAL1553B_ERROR_SUCCESS;

    Virtual1553B_DbgPrintExit_0("Success.");
}


VOID VirtualBC_ModeCommandWithDataReceive(IN PVIRTUAL1553B_CONTEXT Context)
{
    Virtual1553B_DbgPrintEntry();

    PVIRTUAL1553B_TRANSFER_LINE Line = Context->Line;

    KeStallExecutionProcessor(INTERVAL_MODE_COMMAND_WITH_DATA_RECEIVE_COMMAND);
    Virtual1553B_VisitRemoteTerminal(Context->TerminalMap, Line, VirtualRT_ModeCommandWithDataReceive, Line->CW1.RemoteTerminalAddress);

    if (Line->SW1.Raw == INVALID_STATUS_WORD)
    {
        Line->Error = VIRTUAL1553B_ERROR_NO_FIRST_RESPONSE;

        Virtual1553B_DbgPrintExit_0("Failure: No first response.");
        return;
    }

    if (Line->SW1.MessageError)
    {
        Line->Error = VIRTUAL1553B_ERROR_MESSAGE_ERROR;
        KeStallExecutionProcessor(INTERVAL_STATUS_WORD);

        Virtual1553B_DbgPrintExit_0("Failure: Message error.");
    }
    else
    {
        Line->Error = VIRTUAL1553B_ERROR_SUCCESS;
        KeStallExecutionProcessor(INTERVAL_MODE_COMMAND_WITH_DATA_RECEIVE_RESPONSE);

        Virtual1553B_DbgPrintExit_0("Success.");
    }
}


VOID VirtualBC_ModeCommandWithDataReceiveBroadcast(IN PVIRTUAL1553B_CONTEXT Context)
{
    Virtual1553B_DbgPrintEntry();

    PVIRTUAL1553B_TRANSFER_LINE Line = Context->Line;

    KeStallExecutionProcessor(INTERVAL_MODE_COMMAND_WITH_DATA_RECEIVE_BROADCAST_COMMAND);
    Virtual1553B_VisitRemoteTerminal(Context->TerminalMap, Line, VirtualRT_ModeCommandWithDataReceiveBroadcast, Line->CW1.RemoteTerminalAddress);
    Line->Error = VIRTUAL1553B_ERROR_SUCCESS;

    Virtual1553B_DbgPrintExit_0("Success.");
}


VOID VirtualBC_ModeCommandWithDataTransmit(IN PVIRTUAL1553B_CONTEXT Context)
{
    Virtual1553B_DbgPrintEntry();

    PVIRTUAL1553B_TRANSFER_LINE Line = Context->Line;

    KeStallExecutionProcessor(INTERVAL_MODE_COMMAND_WITH_DATA_TRANSMIT_COMMAND);
    Virtual1553B_VisitRemoteTerminal(Context->TerminalMap, Line, VirtualRT_ModeCommandWithDataTransmit, Line->CW1.RemoteTerminalAddress);

    if (Line->SW1.Raw == INVALID_STATUS_WORD)
    {
        Line->Error = VIRTUAL1553B_ERROR_NO_FIRST_RESPONSE;

        Virtual1553B_DbgPrintExit_0("Failure: No first response.");
        return;
    }

    USHORT ModeCommand = GET_MODE_COMMAND(Line->CW1.Raw);

    if (Line->SW1.MessageError && (ModeCommand != CommandTransmitLastCommandWord))
    {
        Line->Error = VIRTUAL1553B_ERROR_MESSAGE_ERROR;
        KeStallExecutionProcessor(INTERVAL_STATUS_WORD);

        Virtual1553B_DbgPrintExit_0("Failure: Message error.");
    }
    else
    {
        Line->Error = VIRTUAL1553B_ERROR_SUCCESS;
        KeStallExecutionProcessor(INTERVAL_MODE_COMMAND_WITH_DATA_TRANSMIT_RESPONSE);

        Virtual1553B_DbgPrintExit_0("Success.");
    }
}


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////


inline static VOID ValidMessageReceived(
    IN PVIRTUAL_REMOTE_TERMINAL RemoteTerminal,
    IN COMMAND_WORD CommandWord,
    IN BOOLEAN BroadcastCommandReceived)
{
    RemoteTerminal->StatusWord.MessageError = 0;
    RemoteTerminal->StatusWord.BroadcastCommandReceived = BroadcastCommandReceived;
    RemoteTerminal->LastCommandWord = CommandWord;
}

inline static VOID InvalidMessageReceived(
    IN PVIRTUAL_REMOTE_TERMINAL RemoteTerminal,
    IN COMMAND_WORD CommandWord)
{
    RemoteTerminal->StatusWord.MessageError = 1;
    RemoteTerminal->StatusWord.BroadcastCommandReceived = 0;
    RemoteTerminal->LastCommandWord = CommandWord;
}

inline static VOID SubmitTransferResult(
    IN PVIRTUAL_TERMINAL VirtualTerminal,
    IN PVIRTUAL1553B_TRANSFER_LINE Line,
    IN USHORT Error,
    IN COMMAND_WORD CW)
{
    Virtual1553B_DbgPrintEntry();

    PVIRTUAL_REMOTE_TERMINAL RemoteTerminal = VirtualTerminal->RemoteTerminal;
    PRT_TRANSFER_RESULT TransferResult = &RemoteTerminal->TransferResultBuffer[RemoteTerminal->Head];

    TransferResult->Time = Line->Time;
    TransferResult->Error = Error;
    TransferResult->Format = Line->Format;
    TransferResult->CW = CW;
    RtlCopyMemory(TransferResult->Data, Line->Data, DATA_WORD_COUNT_MAXIMUM * sizeof(USHORT));

    RemoteTerminal->Head = (RemoteTerminal->Head + 1) % TRANSFER_RESULT_BUFFER_LENGTH;

    Virtual1553B_RaiseInterrupt(VirtualTerminal);

    Virtual1553B_DbgPrintExit_2("The transfer result was saved. Head: %hu, Tail: %hu.", RemoteTerminal->Head, RemoteTerminal->Tail);
}


VOID VirtualRT_TransferBCRT(
    IN PVIRTUAL_TERMINAL VirtualTerminal,
    IN PVIRTUAL1553B_TRANSFER_LINE Line)
{
    Virtual1553B_DbgPrintEntry();

    PVIRTUAL_REMOTE_TERMINAL RemoteTerminal = VirtualTerminal->RemoteTerminal;

    Virtual1553B_DbgPrint_1("Remote terminal address: %hu.", RemoteTerminal->Address);

    if (Line->CW1.RemoteTerminalAddress != RemoteTerminal->Address)
    {
        Virtual1553B_DbgPrintExit();
        return;
    }

    if (VALIDATE_TRANSFER_BC_RT(Line->CW1.Raw))
    {
        ValidMessageReceived(RemoteTerminal, Line->CW1, FALSE);
        Line->SW1 = RemoteTerminal->StatusWord;

        USHORT DataWordCount = GET_DATA_WORD_COUNT(Line->CW1.Raw);
        RtlCopyMemory(GetReceiveBuffer(RemoteTerminal, Line->CW1.Subaddress), Line->Data, DataWordCount * sizeof(USHORT));
        if (Line->CW1.Subaddress == SUBADDRESS_MAXIMUM)
        {
            RtlCopyMemory(GetTransmitBuffer(RemoteTerminal, Line->CW1.Subaddress), Line->Data, DataWordCount * sizeof(USHORT));
        }

        SubmitTransferResult(VirtualTerminal, Line, VIRTUAL1553B_ERROR_SUCCESS, Line->CW1);

        Virtual1553B_DbgPrintExit_0("Success.");
    }
    else
    {
        InvalidMessageReceived(RemoteTerminal, Line->CW1);
        Line->SW1 = RemoteTerminal->StatusWord;

        SubmitTransferResult(VirtualTerminal, Line, VIRTUAL1553B_ERROR_MESSAGE_ERROR, Line->CW1);

        Virtual1553B_DbgPrintExit_0("Failure: Validate Transfer BC-RT.");
    }
}


VOID VirtualRT_TransferBCRTBroadcast(
    IN PVIRTUAL_TERMINAL VirtualTerminal,
    IN PVIRTUAL1553B_TRANSFER_LINE Line)
{
    Virtual1553B_DbgPrintEntry();

    PVIRTUAL_REMOTE_TERMINAL RemoteTerminal = VirtualTerminal->RemoteTerminal;

    Virtual1553B_DbgPrint_1("Remote terminal address: %hu.", RemoteTerminal->Address);

    if (Line->CW1.RemoteTerminalAddress != BROADCAST_REMOTE_TERMINAL_ADDRESS)
    {
        Virtual1553B_DbgPrintExit();
        return;
    }

    if (VALIDATE_TRANSFER_BC_RT_BROADCAST(Line->CW1.Raw))
    {
        ValidMessageReceived(RemoteTerminal, Line->CW1, TRUE);

        USHORT DataWordCount = GET_DATA_WORD_COUNT(Line->CW1.Raw);
        RtlCopyMemory(GetReceiveBuffer(RemoteTerminal, Line->CW1.Subaddress), Line->Data, DataWordCount * sizeof(USHORT));
        if (Line->CW1.Subaddress == SUBADDRESS_MAXIMUM)
        {
            RtlCopyMemory(GetTransmitBuffer(RemoteTerminal, Line->CW1.Subaddress), Line->Data, DataWordCount * sizeof(USHORT));
        }

        SubmitTransferResult(VirtualTerminal, Line, VIRTUAL1553B_ERROR_SUCCESS, Line->CW1);

        Virtual1553B_DbgPrintExit_0("Success.");
    }
    else
    {
        InvalidMessageReceived(RemoteTerminal, Line->CW1);

        SubmitTransferResult(VirtualTerminal, Line, VIRTUAL1553B_ERROR_MESSAGE_ERROR, Line->CW1);

        Virtual1553B_DbgPrintExit_0("Failure: Validate Transfer BC-RT Broadcast.");
    }
}


VOID VirtualRT_TransferRTBC(
    IN PVIRTUAL_TERMINAL VirtualTerminal,
    IN PVIRTUAL1553B_TRANSFER_LINE Line)
{
    Virtual1553B_DbgPrintEntry();

    PVIRTUAL_REMOTE_TERMINAL RemoteTerminal = VirtualTerminal->RemoteTerminal;

    Virtual1553B_DbgPrint_1("Remote terminal address: %hu.", RemoteTerminal->Address);

    if (Line->CW1.RemoteTerminalAddress != RemoteTerminal->Address)
    {
        Virtual1553B_DbgPrintExit();
        return;
    }

    if (VALIDATE_TRANSFER_RT_BC(Line->CW1.Raw))
    {
        ValidMessageReceived(RemoteTerminal, Line->CW1, FALSE);
        Line->SW1 = RemoteTerminal->StatusWord;

        USHORT DataWordCount = GET_DATA_WORD_COUNT(Line->CW1.Raw);
        RtlCopyMemory(Line->Data, GetTransmitBuffer(RemoteTerminal, Line->CW1.Subaddress), DataWordCount * sizeof(USHORT));

        SubmitTransferResult(VirtualTerminal, Line, VIRTUAL1553B_ERROR_SUCCESS, Line->CW1);

        Virtual1553B_DbgPrintExit_0("Success.");
    }
    else
    {
        InvalidMessageReceived(RemoteTerminal, Line->CW1);
        Line->SW1 = RemoteTerminal->StatusWord;

        SubmitTransferResult(VirtualTerminal, Line, VIRTUAL1553B_ERROR_MESSAGE_ERROR, Line->CW1);

        Virtual1553B_DbgPrintExit_0("Failure: Validate Transfer RT-BC.");
    }
}


VOID VirtualRT_TransferRTRTFirst(
    IN PVIRTUAL_TERMINAL VirtualTerminal,
    IN PVIRTUAL1553B_TRANSFER_LINE Line)
{
    Virtual1553B_DbgPrintEntry();

    PVIRTUAL_REMOTE_TERMINAL RemoteTerminal = VirtualTerminal->RemoteTerminal;

    Virtual1553B_DbgPrint_1("Remote terminal address: %hu.", RemoteTerminal->Address);

    if (Line->CW2.RemoteTerminalAddress != RemoteTerminal->Address)
    {
        Virtual1553B_DbgPrintExit();
        return;
    }

    if (VALIDATE_TRANSFER_RT_RT(Line->CW1.Raw, Line->CW2.Raw))
    {
        ValidMessageReceived(RemoteTerminal, Line->CW2, FALSE);
        Line->SW1 = RemoteTerminal->StatusWord;

        USHORT DataWordCount = GET_DATA_WORD_COUNT(Line->CW2.Raw);
        RtlCopyMemory(Line->Data, GetTransmitBuffer(RemoteTerminal, Line->CW2.Subaddress), DataWordCount * sizeof(USHORT));

        SubmitTransferResult(VirtualTerminal, Line, VIRTUAL1553B_ERROR_SUCCESS, Line->CW2);

        Virtual1553B_DbgPrintExit_0("Success.");
    }
    else
    {
        InvalidMessageReceived(RemoteTerminal, Line->CW2);
        Line->SW1 = RemoteTerminal->StatusWord;

        SubmitTransferResult(VirtualTerminal, Line, VIRTUAL1553B_ERROR_MESSAGE_ERROR, Line->CW2);

        Virtual1553B_DbgPrintExit_0("Failure: Validate Transfer RT-RT.");
    }
}


VOID VirtualRT_TransferRTRTSecond(
    IN PVIRTUAL_TERMINAL VirtualTerminal,
    IN PVIRTUAL1553B_TRANSFER_LINE Line)
{
    Virtual1553B_DbgPrintEntry();

    PVIRTUAL_REMOTE_TERMINAL RemoteTerminal = VirtualTerminal->RemoteTerminal;

    Virtual1553B_DbgPrint_1("Remote terminal address: %hu.", RemoteTerminal->Address);

    if (Line->CW2.RemoteTerminalAddress == RemoteTerminal->Address)
    {
        Virtual1553B_DbgPrintExit();
        return;
    }

    if (Line->CW1.RemoteTerminalAddress != RemoteTerminal->Address)
    {
        Virtual1553B_DbgPrintExit();
        return;
    }

    if (VALIDATE_TRANSFER_RT_RT(Line->CW1.Raw, Line->CW2.Raw))
    {
        if (Line->SW1.Raw == INVALID_STATUS_WORD)
        {
            InvalidMessageReceived(RemoteTerminal, Line->CW1);

            SubmitTransferResult(VirtualTerminal, Line, VIRTUAL1553B_ERROR_MESSAGE_ERROR, Line->CW1);

            Virtual1553B_DbgPrintExit_0("Failure: No response from the transmitting remote terminal.");
            return;
        }

        if (Line->SW1.MessageError)
        {
            InvalidMessageReceived(RemoteTerminal, Line->CW1);

            SubmitTransferResult(VirtualTerminal, Line, VIRTUAL1553B_ERROR_MESSAGE_ERROR, Line->CW1);

            Virtual1553B_DbgPrintExit_0("Failure: Message error in the response from the transmitting remote terminal.");
            return;
        }

        ValidMessageReceived(RemoteTerminal, Line->CW1, FALSE);
        Line->SW2 = RemoteTerminal->StatusWord;

        USHORT DataWordCount = GET_DATA_WORD_COUNT(Line->CW1.Raw);
        RtlCopyMemory(GetReceiveBuffer(RemoteTerminal, Line->CW1.Subaddress), Line->Data, DataWordCount * sizeof(USHORT));
        if (Line->CW1.Subaddress == SUBADDRESS_MAXIMUM)
        {
            RtlCopyMemory(GetTransmitBuffer(RemoteTerminal, Line->CW1.Subaddress), Line->Data, DataWordCount * sizeof(USHORT));
        }

        SubmitTransferResult(VirtualTerminal, Line, VIRTUAL1553B_ERROR_SUCCESS, Line->CW1);

        Virtual1553B_DbgPrintExit_0("Success.");
    }
    else
    {
        InvalidMessageReceived(RemoteTerminal, Line->CW1);

        SubmitTransferResult(VirtualTerminal, Line, VIRTUAL1553B_ERROR_MESSAGE_ERROR, Line->CW1);

        Virtual1553B_DbgPrintExit_0("Failure: Validate Transfer RT-RT.");
    }
}


VOID VirtualRT_TransferRTRTBroadcastFirst(
    IN PVIRTUAL_TERMINAL VirtualTerminal,
    IN PVIRTUAL1553B_TRANSFER_LINE Line)
{
    Virtual1553B_DbgPrintEntry();

    PVIRTUAL_REMOTE_TERMINAL RemoteTerminal = VirtualTerminal->RemoteTerminal;

    Virtual1553B_DbgPrint_1("Remote terminal address: %hu.", RemoteTerminal->Address);

    if (Line->CW2.RemoteTerminalAddress != RemoteTerminal->Address)
    {
        Virtual1553B_DbgPrintExit();
        return;
    }

    if (VALIDATE_TRANSFER_RT_RT_BROADCAST(Line->CW1.Raw, Line->CW2.Raw))
    {
        ValidMessageReceived(RemoteTerminal, Line->CW2, FALSE);
        Line->SW1 = RemoteTerminal->StatusWord;

        USHORT DataWordCount = GET_DATA_WORD_COUNT(Line->CW2.Raw);
        RtlCopyMemory(Line->Data, GetTransmitBuffer(RemoteTerminal, Line->CW2.Subaddress), DataWordCount * sizeof(USHORT));

        SubmitTransferResult(VirtualTerminal, Line, VIRTUAL1553B_ERROR_SUCCESS, Line->CW2);

        Virtual1553B_DbgPrintExit_0("Success.");
    }
    else
    {
        InvalidMessageReceived(RemoteTerminal, Line->CW2);
        Line->SW1 = RemoteTerminal->StatusWord;

        SubmitTransferResult(VirtualTerminal, Line, VIRTUAL1553B_ERROR_MESSAGE_ERROR, Line->CW2);

        Virtual1553B_DbgPrintExit_0("Failure: Validate Transfer RT-RT Broadcast.");
    }
}


VOID VirtualRT_TransferRTRTBroadcastSecond(
    IN PVIRTUAL_TERMINAL VirtualTerminal,
    IN PVIRTUAL1553B_TRANSFER_LINE Line)
{
    Virtual1553B_DbgPrintEntry();

    PVIRTUAL_REMOTE_TERMINAL RemoteTerminal = VirtualTerminal->RemoteTerminal;

    Virtual1553B_DbgPrint_1("Remote terminal address: %hu.", RemoteTerminal->Address);

    if (Line->CW2.RemoteTerminalAddress == RemoteTerminal->Address)
    {
        Virtual1553B_DbgPrintExit();
        return;
    }

    if (Line->CW1.RemoteTerminalAddress != BROADCAST_REMOTE_TERMINAL_ADDRESS)
    {
        Virtual1553B_DbgPrintExit();
        return;
    }

    if (VALIDATE_TRANSFER_RT_RT_BROADCAST(Line->CW1.Raw, Line->CW2.Raw))
    {
        if (Line->SW1.Raw == INVALID_STATUS_WORD)
        {
            InvalidMessageReceived(RemoteTerminal, Line->CW1);

            SubmitTransferResult(VirtualTerminal, Line, VIRTUAL1553B_ERROR_MESSAGE_ERROR, Line->CW1);

            Virtual1553B_DbgPrintExit_0("Failure: No response from the transmitting remote terminal.");
            return;
        }

        if (Line->SW1.MessageError)
        {
            InvalidMessageReceived(RemoteTerminal, Line->CW1);

            SubmitTransferResult(VirtualTerminal, Line, VIRTUAL1553B_ERROR_MESSAGE_ERROR, Line->CW1);

            Virtual1553B_DbgPrintExit_0("Failure: Message error in the response from the transmitting remote terminal.");
            return;
        }

        ValidMessageReceived(RemoteTerminal, Line->CW1, FALSE);

        USHORT DataWordCount = GET_DATA_WORD_COUNT(Line->CW1.Raw);
        RtlCopyMemory(GetReceiveBuffer(RemoteTerminal, Line->CW1.Subaddress), Line->Data, DataWordCount * sizeof(USHORT));
        if (Line->CW1.Subaddress == SUBADDRESS_MAXIMUM)
        {
            RtlCopyMemory(GetTransmitBuffer(RemoteTerminal, Line->CW1.Subaddress), Line->Data, DataWordCount * sizeof(USHORT));
        }

        SubmitTransferResult(VirtualTerminal, Line, VIRTUAL1553B_ERROR_SUCCESS, Line->CW1);

        Virtual1553B_DbgPrintExit_0("Success.");
    }
    else
    {
        InvalidMessageReceived(RemoteTerminal, Line->CW1);

        SubmitTransferResult(VirtualTerminal, Line, VIRTUAL1553B_ERROR_MESSAGE_ERROR, Line->CW1);

        Virtual1553B_DbgPrintExit_0("Failure: Validate Transfer RT-RT Broadcast.");
    }
}


VOID VirtualRT_ModeCommandWithoutData(
    IN PVIRTUAL_TERMINAL VirtualTerminal,
    IN PVIRTUAL1553B_TRANSFER_LINE Line)
{
    Virtual1553B_DbgPrintEntry();

    PVIRTUAL_REMOTE_TERMINAL RemoteTerminal = VirtualTerminal->RemoteTerminal;

    Virtual1553B_DbgPrint_1("Remote terminal address: %hu.", RemoteTerminal->Address);

    if (Line->CW1.RemoteTerminalAddress != RemoteTerminal->Address)
    {
        Virtual1553B_DbgPrintExit();
        return;
    }

    if (VALIDATE_MODE_COMMAND_WITHOUT_DATA(Line->CW1.Raw))
    {
        USHORT ModeCode = GET_MODE_CODE(Line->CW1.Raw);

        switch (ModeCode)
        {
        case CodeDynamicBusControl:
        case CodeSynchronize:
        case CodeInitiateSelfTest:
        case CodeTransmitterShutdown:
        case CodeOverrideTransmitterShutdown:
        case CodeInhibitTerminalFlagBit:
        case CodeOverrideInhibitTerminalFlagBit:
        case CodeResetRemoteTerminal:
            ValidMessageReceived(RemoteTerminal, Line->CW1, FALSE);
            Line->SW1 = RemoteTerminal->StatusWord;
            break;

        case CodeTransmitStatusWord:
            RemoteTerminal->LastCommandWord = Line->CW1;
            Line->SW1 = RemoteTerminal->StatusWord;
            break;

        default:
            if ((ModeCode >= CODE_RESERVED_WITHOUT_DATA_MINIMUM) &&
                (ModeCode <= CODE_RESERVED_WITHOUT_DATA_MAXIMUM))
            {
                ValidMessageReceived(RemoteTerminal, Line->CW1, FALSE);
                Line->SW1 = RemoteTerminal->StatusWord;
            }
            else
            {
                InvalidMessageReceived(RemoteTerminal, Line->CW1);
                Line->SW1 = RemoteTerminal->StatusWord;

                SubmitTransferResult(VirtualTerminal, Line, VIRTUAL1553B_ERROR_MESSAGE_ERROR, Line->CW1);

                Virtual1553B_DbgPrintExit_0("Failure: Invalid Mode Code.");
                return;
            }
            break;
        }

        SubmitTransferResult(VirtualTerminal, Line, VIRTUAL1553B_ERROR_SUCCESS, Line->CW1);

        Virtual1553B_DbgPrintExit_0("Success.");
    }
    else
    {
        InvalidMessageReceived(RemoteTerminal, Line->CW1);
        Line->SW1 = RemoteTerminal->StatusWord;

        SubmitTransferResult(VirtualTerminal, Line, VIRTUAL1553B_ERROR_MESSAGE_ERROR, Line->CW1);

        Virtual1553B_DbgPrintExit_0("Failure: Validate Mode Command Without Data.");
    }
}


VOID VirtualRT_ModeCommandWithoutDataBroadcast(
    IN PVIRTUAL_TERMINAL VirtualTerminal,
    IN PVIRTUAL1553B_TRANSFER_LINE Line)
{
    Virtual1553B_DbgPrintEntry();

    PVIRTUAL_REMOTE_TERMINAL RemoteTerminal = VirtualTerminal->RemoteTerminal;

    Virtual1553B_DbgPrint_1("Remote terminal address: %hu.", RemoteTerminal->Address);

    if (Line->CW1.RemoteTerminalAddress != BROADCAST_REMOTE_TERMINAL_ADDRESS)
    {
        Virtual1553B_DbgPrintExit();
        return;
    }

    if (VALIDATE_MODE_COMMAND_WITHOUT_DATA_BROADCAST(Line->CW1.Raw))
    {
        USHORT ModeCode = GET_MODE_CODE(Line->CW1.Raw);

        switch (ModeCode)
        {
        case CodeSynchronize:
        case CodeInitiateSelfTest:
        case CodeTransmitterShutdown:
        case CodeOverrideTransmitterShutdown:
        case CodeInhibitTerminalFlagBit:
        case CodeOverrideInhibitTerminalFlagBit:
        case CodeResetRemoteTerminal:
            ValidMessageReceived(RemoteTerminal, Line->CW1, TRUE);
            break;

        default:
            if ((ModeCode >= CODE_RESERVED_WITHOUT_DATA_MINIMUM) &&
                (ModeCode <= CODE_RESERVED_WITHOUT_DATA_MAXIMUM))
            {
                ValidMessageReceived(RemoteTerminal, Line->CW1, TRUE);
            }
            else
            {
                InvalidMessageReceived(RemoteTerminal, Line->CW1);

                SubmitTransferResult(VirtualTerminal, Line, VIRTUAL1553B_ERROR_MESSAGE_ERROR, Line->CW1);

                Virtual1553B_DbgPrintExit_0("Failure: Invalid Mode Code.");
                return;
            }
            break;
        }

        SubmitTransferResult(VirtualTerminal, Line, VIRTUAL1553B_ERROR_SUCCESS, Line->CW1);

        Virtual1553B_DbgPrintExit_0("Success.");
    }
    else
    {
        InvalidMessageReceived(RemoteTerminal, Line->CW1);

        SubmitTransferResult(VirtualTerminal, Line, VIRTUAL1553B_ERROR_MESSAGE_ERROR, Line->CW1);

        Virtual1553B_DbgPrintExit_0("Failure: Validate Mode Command Without Data Broadcast.");
    }
}


VOID VirtualRT_ModeCommandWithDataReceive(
    IN PVIRTUAL_TERMINAL VirtualTerminal,
    IN PVIRTUAL1553B_TRANSFER_LINE Line)
{
    Virtual1553B_DbgPrintEntry();

    PVIRTUAL_REMOTE_TERMINAL RemoteTerminal = VirtualTerminal->RemoteTerminal;

    Virtual1553B_DbgPrint_1("Remote terminal address: %hu.", RemoteTerminal->Address);

    if (Line->CW1.RemoteTerminalAddress != RemoteTerminal->Address)
    {
        Virtual1553B_DbgPrintExit();
        return;
    }

    if (VALIDATE_MODE_COMMAND_WITH_DATA_RECEIVE(Line->CW1.Raw))
    {
        USHORT ModeCode = GET_MODE_CODE(Line->CW1.Raw);

        switch (ModeCode)
        {
        case CodeSynchronizeWithDataWord:
        case CodeSelectedTransmitterShutdown:
        case CodeOverrideSelectedTransmitterShutdown:
            ValidMessageReceived(RemoteTerminal, Line->CW1, FALSE);
            Line->SW1 = RemoteTerminal->StatusWord;
            break;

        default:
            if ((ModeCode >= CODE_RESERVED_WITH_DATA_MINIMUM) &&
                (ModeCode <= CODE_RESERVED_WITH_DATA_MAXIMUM))
            {
                ValidMessageReceived(RemoteTerminal, Line->CW1, FALSE);
                Line->SW1 = RemoteTerminal->StatusWord;
            }
            else
            {
                InvalidMessageReceived(RemoteTerminal, Line->CW1);
                Line->SW1 = RemoteTerminal->StatusWord;

                SubmitTransferResult(VirtualTerminal, Line, VIRTUAL1553B_ERROR_MESSAGE_ERROR, Line->CW1);

                Virtual1553B_DbgPrintExit_0("Failure: Invalid Mode Code.");
                return;
            }
            break;
        }

        SubmitTransferResult(VirtualTerminal, Line, VIRTUAL1553B_ERROR_SUCCESS, Line->CW1);

        Virtual1553B_DbgPrintExit_0("Success.");
    }
    else
    {
        InvalidMessageReceived(RemoteTerminal, Line->CW1);
        Line->SW1 = RemoteTerminal->StatusWord;

        SubmitTransferResult(VirtualTerminal, Line, VIRTUAL1553B_ERROR_MESSAGE_ERROR, Line->CW1);

        Virtual1553B_DbgPrintExit_0("Failure: Validate Mode Command With Data Receive.");
    }
}


VOID VirtualRT_ModeCommandWithDataReceiveBroadcast(
    IN PVIRTUAL_TERMINAL VirtualTerminal,
    IN PVIRTUAL1553B_TRANSFER_LINE Line)
{
    Virtual1553B_DbgPrintEntry();

    PVIRTUAL_REMOTE_TERMINAL RemoteTerminal = VirtualTerminal->RemoteTerminal;

    Virtual1553B_DbgPrint_1("Remote terminal address: %hu.", RemoteTerminal->Address);

    if (Line->CW1.RemoteTerminalAddress != BROADCAST_REMOTE_TERMINAL_ADDRESS)
    {
        Virtual1553B_DbgPrintExit();
        return;
    }

    if (VALIDATE_MODE_COMMAND_WITH_DATA_RECEIVE_BROADCAST(Line->CW1.Raw))
    {
        USHORT ModeCode = GET_MODE_CODE(Line->CW1.Raw);

        switch (ModeCode)
        {
        case CodeSynchronizeWithDataWord:
        case CodeSelectedTransmitterShutdown:
        case CodeOverrideSelectedTransmitterShutdown:
            ValidMessageReceived(RemoteTerminal, Line->CW1, TRUE);
            break;

        default:
            if ((ModeCode >= CODE_RESERVED_WITH_DATA_MINIMUM) &&
                (ModeCode <= CODE_RESERVED_WITH_DATA_MAXIMUM))
            {
                ValidMessageReceived(RemoteTerminal, Line->CW1, TRUE);
            }
            else
            {
                InvalidMessageReceived(RemoteTerminal, Line->CW1);

                SubmitTransferResult(VirtualTerminal, Line, VIRTUAL1553B_ERROR_MESSAGE_ERROR, Line->CW1);

                Virtual1553B_DbgPrintExit_0("Failure: Invalid Mode Code.");
                return;
            }
            break;
        }

        SubmitTransferResult(VirtualTerminal, Line, VIRTUAL1553B_ERROR_SUCCESS, Line->CW1);

        Virtual1553B_DbgPrintExit_0("Success.");
    }
    else
    {
        InvalidMessageReceived(RemoteTerminal, Line->CW1);

        SubmitTransferResult(VirtualTerminal, Line, VIRTUAL1553B_ERROR_MESSAGE_ERROR, Line->CW1);

        Virtual1553B_DbgPrintExit_0("Failure: Validate Mode Command With Data Receive Broadcast.");
    }
}


VOID VirtualRT_ModeCommandWithDataTransmit(
    IN PVIRTUAL_TERMINAL VirtualTerminal,
    IN PVIRTUAL1553B_TRANSFER_LINE Line)
{
    Virtual1553B_DbgPrintEntry();

    PVIRTUAL_REMOTE_TERMINAL RemoteTerminal = VirtualTerminal->RemoteTerminal;

    Virtual1553B_DbgPrint_1("Remote terminal address: %hu.", RemoteTerminal->Address);

    if (Line->CW1.RemoteTerminalAddress != RemoteTerminal->Address)
    {
        Virtual1553B_DbgPrintExit();
        return;
    }

    if (VALIDATE_MODE_COMMAND_WITH_DATA_TRANSMIT(Line->CW1.Raw))
    {
        USHORT ModeCode = GET_MODE_CODE(Line->CW1.Raw);

        switch (ModeCode)
        {
        case CodeTransmitVectorWord:
            ValidMessageReceived(RemoteTerminal, Line->CW1, FALSE);
            Line->SW1 = RemoteTerminal->StatusWord;
            Line->Data[0] = RemoteTerminal->VectorWord;
            break;

        case CodeTransmitLastCommandWord:
            Line->SW1 = RemoteTerminal->StatusWord;
            Line->Data[0] = RemoteTerminal->LastCommandWord.Raw;
            break;

        case CodeTransmitBuiltInTestWord:
            ValidMessageReceived(RemoteTerminal, Line->CW1, FALSE);
            Line->SW1 = RemoteTerminal->StatusWord;
            Line->Data[0] = RemoteTerminal->BuiltInTestWord;
            break;

        default:
            if ((ModeCode >= CODE_RESERVED_WITH_DATA_MINIMUM) &&
                (ModeCode <= CODE_RESERVED_WITH_DATA_MAXIMUM))
            {
                ValidMessageReceived(RemoteTerminal, Line->CW1, FALSE);
                Line->SW1 = RemoteTerminal->StatusWord;
                Line->Data[0] = 0;
            }
            else
            {
                InvalidMessageReceived(RemoteTerminal, Line->CW1);
                Line->SW1 = RemoteTerminal->StatusWord;

                SubmitTransferResult(VirtualTerminal, Line, VIRTUAL1553B_ERROR_MESSAGE_ERROR, Line->CW1);

                Virtual1553B_DbgPrintExit_0("Failure: Invalid Mode Code.");
                return;
            }
            break;
        }

        SubmitTransferResult(VirtualTerminal, Line, VIRTUAL1553B_ERROR_SUCCESS, Line->CW1);

        Virtual1553B_DbgPrintExit_0("Success.");
    }
    else
    {
        InvalidMessageReceived(RemoteTerminal, Line->CW1);
        Line->SW1 = RemoteTerminal->StatusWord;

        SubmitTransferResult(VirtualTerminal, Line, VIRTUAL1553B_ERROR_MESSAGE_ERROR, Line->CW1);

        Virtual1553B_DbgPrintExit_0("Failure: Validate Mode Command With Data Transmit.");
    }
}


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////


VOID VirtualBM_SaveTransferResult(
    IN PVIRTUAL_TERMINAL VirtualTerminal,
    IN PVIRTUAL1553B_TRANSFER_LINE Line)
{
    Virtual1553B_DbgPrintEntry();

    PVIRTUAL_BUS_MONITOR BusMonitor = VirtualTerminal->BusMonitor;
    PTRANSFER_RECORD TransferRecord = &BusMonitor->TransferRecordBuffer[BusMonitor->CurrentBase];

    TransferRecord->Time = Line->Time;
    TransferRecord->Error = Line->Error;
    TransferRecord->TransferLine = Line->TransferLine;
    TransferRecord->Format = Line->Format;
    TransferRecord->CW1 = Line->CW1;
    TransferRecord->CW2 = Line->CW2;
    TransferRecord->SW1 = Line->SW1;
    TransferRecord->SW2 = Line->SW2;
    RtlCopyMemory(TransferRecord->Data, Line->Data, DATA_WORD_COUNT_MAXIMUM * sizeof(USHORT));

    BusMonitor->CurrentBase = (BusMonitor->CurrentBase + 1) % TRANSFER_RECORD_BUFFER_LENGTH;

    Virtual1553B_RaiseInterrupt(VirtualTerminal);

    Virtual1553B_DbgPrintExit_1("The transfer result was saved. Current base: %hu.", BusMonitor->CurrentBase);
}
