
#pragma once


#define QUOTE(arg) #arg
#define STRINGIFY(arg) QUOTE(arg)


#define VIRTUAL1553B_DBG_FILTER "[Virtual1553B]"

#define VIRTUAL1553B_DBG_STRING(String) \
    VIRTUAL1553B_DBG_FILTER " [" __FUNCTION__ "] [LINE: " STRINGIFY(__LINE__) "] " String "\n"


#define Virtual1553B_DbgPrint_0(Message) \
    KdPrint((VIRTUAL1553B_DBG_STRING(Message)))

#define Virtual1553B_DbgPrint_1(Format, Arg) \
    KdPrint((VIRTUAL1553B_DBG_STRING(Format), (Arg)))

#define Virtual1553B_DbgPrint_2(Format, Arg1, Arg2) \
    KdPrint((VIRTUAL1553B_DBG_STRING(Format), (Arg1), (Arg2)))

#define Virtual1553B_DbgPrintSeparator() \
    KdPrint((VIRTUAL1553B_DBG_FILTER "\n"))


#define VIRTUAL1553B_DBG_ENTRY "[Entry]"
#define VIRTUAL1553B_DBG_EXIT "[Exit]"


#define Virtual1553B_DbgPrintEntry() \
    Virtual1553B_DbgPrint_0(VIRTUAL1553B_DBG_ENTRY)

#define Virtual1553B_DbgPrintExit() \
    Virtual1553B_DbgPrint_0(VIRTUAL1553B_DBG_EXIT)

#define Virtual1553B_DbgPrintExit_0(Message) \
    Virtual1553B_DbgPrint_0(VIRTUAL1553B_DBG_EXIT " " Message)

#define Virtual1553B_DbgPrintExit_1(Format, Arg) \
    Virtual1553B_DbgPrint_1(VIRTUAL1553B_DBG_EXIT " " Format, (Arg))

#define Virtual1553B_DbgPrintExit_2(Format, Arg1, Arg2) \
    Virtual1553B_DbgPrint_2(VIRTUAL1553B_DBG_EXIT " " Format, (Arg1), (Arg2))
