
#pragma once

#include "stdafx.h"
#include "Virtual1553B.h"


#define DURATION_COMMAND_WORD   (20)
#define DURATION_STATUS_WORD    (20)
#define DURATION_DATA_WORD      (20)


#define INTERVAL_STATUS_WORD    \
    DURATION_STATUS_WORD


#define INTERVAL_TRANSFER_BC_RT_COMMAND(DataWordCount) (    \
    DURATION_COMMAND_WORD +                                 \
    DURATION_DATA_WORD * (DataWordCount))

#define INTERVAL_TRANSFER_BC_RT_RESPONSE    \
    DURATION_STATUS_WORD


#define INTERVAL_TRANSFER_RT_BC_COMMAND \
    DURATION_COMMAND_WORD

#define INTERVAL_TRANSFER_RT_BC_RESPONSE(DataWordCount) (   \
    DURATION_STATUS_WORD +                                  \
    DURATION_DATA_WORD * (DataWordCount))


#define INTERVAL_TRANSFER_RT_RT_COMMAND (   \
    DURATION_COMMAND_WORD +                 \
    DURATION_COMMAND_WORD)

#define INTERVAL_TRANSFER_RT_RT_RESPONSE_1(DataWordCount) ( \
    DURATION_STATUS_WORD +                                  \
    DURATION_DATA_WORD * (DataWordCount))

#define INTERVAL_TRANSFER_RT_RT_RESPONSE_2  \
    DURATION_STATUS_WORD


#define INTERVAL_MODE_COMMAND_WITHOUT_DATA_COMMAND  \
    DURATION_COMMAND_WORD

#define INTERVAL_MODE_COMMAND_WITHOUT_DATA_RESPONSE \
    DURATION_STATUS_WORD


#define INTERVAL_MODE_COMMAND_WITH_DATA_TRANSMIT_COMMAND    \
    DURATION_COMMAND_WORD

#define INTERVAL_MODE_COMMAND_WITH_DATA_TRANSMIT_RESPONSE ( \
    DURATION_STATUS_WORD +                                  \
    DURATION_DATA_WORD)


#define INTERVAL_MODE_COMMAND_WITH_DATA_RECEIVE_COMMAND (   \
    DURATION_COMMAND_WORD +                                 \
    DURATION_DATA_WORD)

#define INTERVAL_MODE_COMMAND_WITH_DATA_RECEIVE_RESPONSE    \
    DURATION_STATUS_WORD


#define INTERVAL_TRANSFER_BC_RT_BROADCAST_COMMAND(DataWordCount) (  \
    DURATION_COMMAND_WORD +                                         \
    DURATION_DATA_WORD * (DataWordCount))


#define INTERVAL_TRANSFER_RT_RT_BROADCAST_COMMAND ( \
    DURATION_COMMAND_WORD +                         \
    DURATION_COMMAND_WORD)

#define INTERVAL_TRANSFER_RT_RT_BROADCAST_RESPONSE(DataWordCount) ( \
    DURATION_STATUS_WORD +                                          \
    DURATION_DATA_WORD * (DataWordCount))


#define INTERVAL_MODE_COMMAND_WITHOUT_DATA_BROADCAST_COMMAND    \
    DURATION_COMMAND_WORD


#define INTERVAL_MODE_COMMAND_WITH_DATA_RECEIVE_BROADCAST_COMMAND ( \
    DURATION_COMMAND_WORD +                                         \
    DURATION_DATA_WORD)


typedef enum _VIRTUAL1553B_EVENT_OBJECT {
    SynqEventObject = 0,
    TerminateEventObject,
    MaximumEventObject
} VIRTUAL1553B_EVENT_OBJECT;


#define Virtual1553B_RaiseInterrupt(VirtualTerminal)                            \
{                                                                               \
    if ((VirtualTerminal)->InterruptEvent)                                      \
    {                                                                           \
        KeSetEvent((VirtualTerminal)->InterruptEvent, IO_NO_INCREMENT, FALSE);  \
    }                                                                           \
}


BOOLEAN Virtual1553B_CreateWorkerThreads(
    IN PVIRTUAL_TERMINAL VirtualTerminal,
    IN PVIRTUAL1553B_DATA_BUS Bus,
    IN PVIRTUAL_TERMINAL_MAP TerminalMap,
    IN PVIRTUAL1553B_WORKER_ROUTINE WorkerRoutine);

VOID Virtual1553B_CloseWorkerThreads(
    IN PVIRTUAL_TERMINAL VirtualTerminal);


#define AcquireTerminalMap(TerminalMap) \
    ExAcquireFastMutex(&(TerminalMap)->SynqMutex)

#define ReleaseTerminalMap(TerminalMap) \
    ExReleaseFastMutex(&(TerminalMap)->SynqMutex)


BOOLEAN Virtual1553B_RegisterRemoteTerminal(
    IN PVIRTUAL_TERMINAL_MAP TerminalMap,
    IN PVIRTUAL_TERMINAL VirtualTerminal,
    IN USHORT Address,
    IN BOOLEAN Remap);

VOID Virtual1553B_RevokeRemoteTerminal(
    IN PVIRTUAL_TERMINAL_MAP TerminalMap,
    IN PVIRTUAL_TERMINAL VirtualTerminal);

BOOLEAN Virtual1553B_RegisterBusMonitor(
    IN PVIRTUAL_TERMINAL_MAP TerminalMap,
    IN PVIRTUAL_TERMINAL VirtualTerminal);

VOID Virtual1553B_RevokeBusMonitor(
    IN PVIRTUAL_TERMINAL_MAP TerminalMap,
    IN PVIRTUAL_TERMINAL VirtualTerminal);


typedef VOID VIRTUAL1553B_VISITOR(
    IN PVIRTUAL_TERMINAL VirtualTerminal,
    IN PVIRTUAL1553B_TRANSFER_LINE Line);

typedef VIRTUAL1553B_VISITOR *PVIRTUAL1553B_VISITOR;


VOID Virtual1553B_VisitRemoteTerminal(
    IN PVIRTUAL_TERMINAL_MAP TerminalMap,
    IN PVIRTUAL1553B_TRANSFER_LINE Line,
    IN PVIRTUAL1553B_VISITOR RemoteTerminalVisitor,
    IN USHORT Address);

VOID Virtual1553B_VisitBusMonitors(
    IN PVIRTUAL_TERMINAL_MAP TerminalMap,
    IN PVIRTUAL1553B_TRANSFER_LINE Line,
    IN PVIRTUAL1553B_VISITOR BusMonitorVisitor);


VOID VirtualBC_WorkerThread(IN PVIRTUAL1553B_CONTEXT Context);

VOID VirtualBC_TransferBCRT(IN PVIRTUAL1553B_CONTEXT Context);
VOID VirtualBC_TransferBCRTBroadcast(IN PVIRTUAL1553B_CONTEXT Context);
VOID VirtualBC_TransferRTBC(IN PVIRTUAL1553B_CONTEXT Context);
VOID VirtualBC_TransferRTRT(IN PVIRTUAL1553B_CONTEXT Context);
VOID VirtualBC_TransferRTRTBroadcast(IN PVIRTUAL1553B_CONTEXT Context);
VOID VirtualBC_ModeCommandWithoutData(IN PVIRTUAL1553B_CONTEXT Context);
VOID VirtualBC_ModeCommandWithoutDataBroadcast(IN PVIRTUAL1553B_CONTEXT Context);
VOID VirtualBC_ModeCommandWithDataReceive(IN PVIRTUAL1553B_CONTEXT Context);
VOID VirtualBC_ModeCommandWithDataReceiveBroadcast(IN PVIRTUAL1553B_CONTEXT Context);
VOID VirtualBC_ModeCommandWithDataTransmit(IN PVIRTUAL1553B_CONTEXT Context);


VOID VirtualRT_TransferBCRT(
    IN PVIRTUAL_TERMINAL VirtualTerminal,
    IN PVIRTUAL1553B_TRANSFER_LINE Line);

VOID VirtualRT_TransferBCRTBroadcast(
    IN PVIRTUAL_TERMINAL VirtualTerminal,
    IN PVIRTUAL1553B_TRANSFER_LINE Line);

VOID VirtualRT_TransferRTBC(
    IN PVIRTUAL_TERMINAL VirtualTerminal,
    IN PVIRTUAL1553B_TRANSFER_LINE Line);

VOID VirtualRT_TransferRTRTFirst(
    IN PVIRTUAL_TERMINAL VirtualTerminal,
    IN PVIRTUAL1553B_TRANSFER_LINE Line);

VOID VirtualRT_TransferRTRTSecond(
    IN PVIRTUAL_TERMINAL VirtualTerminal,
    IN PVIRTUAL1553B_TRANSFER_LINE Line);

VOID VirtualRT_TransferRTRTBroadcastFirst(
    IN PVIRTUAL_TERMINAL VirtualTerminal,
    IN PVIRTUAL1553B_TRANSFER_LINE Line);

VOID VirtualRT_TransferRTRTBroadcastSecond(
    IN PVIRTUAL_TERMINAL VirtualTerminal,
    IN PVIRTUAL1553B_TRANSFER_LINE Line);

VOID VirtualRT_ModeCommandWithoutData(
    IN PVIRTUAL_TERMINAL VirtualTerminal,
    IN PVIRTUAL1553B_TRANSFER_LINE Line);

VOID VirtualRT_ModeCommandWithoutDataBroadcast(
    IN PVIRTUAL_TERMINAL VirtualTerminal,
    IN PVIRTUAL1553B_TRANSFER_LINE Line);

VOID VirtualRT_ModeCommandWithDataReceive(
    IN PVIRTUAL_TERMINAL VirtualTerminal,
    IN PVIRTUAL1553B_TRANSFER_LINE Line);

VOID VirtualRT_ModeCommandWithDataReceiveBroadcast(
    IN PVIRTUAL_TERMINAL VirtualTerminal,
    IN PVIRTUAL1553B_TRANSFER_LINE Line);

VOID VirtualRT_ModeCommandWithDataTransmit(
    IN PVIRTUAL_TERMINAL VirtualTerminal,
    IN PVIRTUAL1553B_TRANSFER_LINE Line);


VOID VirtualBM_SaveTransferResult(
    IN PVIRTUAL_TERMINAL VirtualTerminal,
    IN PVIRTUAL1553B_TRANSFER_LINE Line);
