
#pragma once

#include "Status.h"
#include "VirtualTerminalDef.h"

#include <pshpack1.h>


#ifdef __cplusplus
extern "C" {
#endif // __cplusplus


typedef enum _VIRTUAL1553B_IOCTL_FUNCTION {

    FUNCTION_ALLOC_TERMINAL = 0,
    FUNCTION_FREE_TERMINAL,

    FUNCTION_INSTALL_EVENT,
    FUNCTION_REMOVE_EVENT,

    FUNCTION_GET_TERMINAL_MODE,
    FUNCTION_START_BUS_CONTROLLER,
    FUNCTION_START_REMOTE_TERMINAL,
    FUNCTION_START_BUS_MONITOR,
    FUNCTION_STOP_TERMINAL,

    FUNCTION_BC_EXECUTE_TRANSFER,
    FUNCTION_BC_GET_TRANSFER_RESULT,

    FUNCTION_RT_SET_ADDRESS,
    FUNCTION_RT_GET_ADDRESS,
    FUNCTION_RT_WRITE_MEMORY,
    FUNCTION_RT_READ_MEMORY,
    FUNCTION_RT_GET_TRANSFER_RESULT,

    FUNCTION_BM_GET_CURRENT_BASE,
    FUNCTION_BM_GET_BASE_COUNT,
    FUNCTION_BM_GET_TRANSFER_RECORD,

    VIRTUAL1553B_FUNCTION_NUMBER

} VIRTUAL1553B_IOCTL_FUNCTION;


#define VIRTUAL1553B_FUNCTION_BASE (0x800)

#define VIRTUAL1553B_FUNCTION_FROM_IOCTL(CtrlCode) \
    (((CtrlCode) >> 2) & 0x7FF)


#define IOCTL_VIRTUAL1553B_ALLOC_TERMINAL \
    CTL_CODE(FILE_DEVICE_UNKNOWN, \
    VIRTUAL1553B_FUNCTION_BASE + FUNCTION_ALLOC_TERMINAL, \
    METHOD_BUFFERED, FILE_ANY_ACCESS)

typedef struct _OUTPUT_ALLOC_TERMINAL {
    VIRTUAL1553B_STATUS Status;
    TERMINAL_HANDLE TerminalHandle;
} OUTPUT_ALLOC_TERMINAL, *POUTPUT_ALLOC_TERMINAL;


#define IOCTL_VIRTUAL1553B_FREE_TERMINAL \
    CTL_CODE(FILE_DEVICE_UNKNOWN, \
    VIRTUAL1553B_FUNCTION_BASE + FUNCTION_FREE_TERMINAL, \
    METHOD_BUFFERED, FILE_ANY_ACCESS)

typedef struct _INPUT_FREE_TERMINAL {
    TERMINAL_HANDLE TerminalHandle;
} INPUT_FREE_TERMINAL, *PINPUT_FREE_TERMINAL;

typedef struct _OUTPUT_FREE_TERMINAL {
    VIRTUAL1553B_STATUS Status;
} OUTPUT_FREE_TERMINAL, *POUTPUT_FREE_TERMINAL;


#define IOCTL_VIRTUAL1553B_INSTALL_EVENT \
    CTL_CODE(FILE_DEVICE_UNKNOWN, \
    VIRTUAL1553B_FUNCTION_BASE + FUNCTION_INSTALL_EVENT, \
    METHOD_BUFFERED, FILE_ANY_ACCESS)

typedef struct _INPUT_INSTALL_EVENT {
    TERMINAL_HANDLE TerminalHandle;
    HANDLE Event;
} INPUT_INSTALL_EVENT, *PINPUT_INSTALL_EVENT;

typedef struct _OUTPUT_INSTALL_EVENT {
    VIRTUAL1553B_STATUS Status;
} OUTPUT_INSTALL_EVENT, *POUTPUT_INSTALL_EVENT;


#define IOCTL_VIRTUAL1553B_REMOVE_EVENT \
    CTL_CODE(FILE_DEVICE_UNKNOWN, \
    VIRTUAL1553B_FUNCTION_BASE + FUNCTION_REMOVE_EVENT, \
    METHOD_BUFFERED, FILE_ANY_ACCESS)

typedef struct _INPUT_REMOVE_EVENT {
    TERMINAL_HANDLE TerminalHandle;
} INPUT_REMOVE_EVENT, *PINPUT_REMOVE_EVENT;

typedef struct _OUTPUT_REMOVE_EVENT {
    VIRTUAL1553B_STATUS Status;
} OUTPUT_REMOVE_EVENT, *POUTPUT_REMOVE_EVENT;


#define IOCTL_VIRTUAL1553B_GET_TERMINAL_MODE \
    CTL_CODE(FILE_DEVICE_UNKNOWN, \
    VIRTUAL1553B_FUNCTION_BASE + FUNCTION_GET_TERMINAL_MODE, \
    METHOD_BUFFERED, FILE_ANY_ACCESS)

typedef struct _INPUT_GET_TERMINAL_MODE {
    TERMINAL_HANDLE TerminalHandle;
} INPUT_GET_TERMINAL_MODE, *PINPUT_GET_TERMINAL_MODE;

typedef struct _OUTPUT_GET_TERMINAL_MODE {
    VIRTUAL1553B_STATUS Status;
    USHORT TerminalMode;
} OUTPUT_GET_TERMINAL_MODE, *POUTPUT_GET_TERMINAL_MODE;


#define IOCTL_VIRTUAL1553B_START_BUS_CONTROLLER \
    CTL_CODE(FILE_DEVICE_UNKNOWN, \
    VIRTUAL1553B_FUNCTION_BASE + FUNCTION_START_BUS_CONTROLLER, \
    METHOD_BUFFERED, FILE_ANY_ACCESS)

typedef struct _INPUT_START_BUS_CONTROLLER {
    TERMINAL_HANDLE TerminalHandle;
} INPUT_START_BUS_CONTROLLER, *PINPUT_START_BUS_CONTROLLER;

typedef struct _OUTPUT_START_BUS_CONTROLLER {
    VIRTUAL1553B_STATUS Status;
} OUTPUT_START_BUS_CONTROLLER, *POUTPUT_START_BUS_CONTROLLER;


#define IOCTL_VIRTUAL1553B_START_REMOTE_TERMINAL \
    CTL_CODE(FILE_DEVICE_UNKNOWN, \
    VIRTUAL1553B_FUNCTION_BASE + FUNCTION_START_REMOTE_TERMINAL, \
    METHOD_BUFFERED, FILE_ANY_ACCESS)

typedef struct _INPUT_START_REMOTE_TERMINAL {
    TERMINAL_HANDLE TerminalHandle;
    USHORT Address;
} INPUT_START_REMOTE_TERMINAL, *PINPUT_START_REMOTE_TERMINAL;

typedef struct _OUTPUT_START_REMOTE_TERMINAL {
    VIRTUAL1553B_STATUS Status;
} OUTPUT_START_REMOTE_TERMINAL, *POUTPUT_START_REMOTE_TERMINAL;


#define IOCTL_VIRTUAL1553B_START_BUS_MONITOR \
    CTL_CODE(FILE_DEVICE_UNKNOWN, \
    VIRTUAL1553B_FUNCTION_BASE + FUNCTION_START_BUS_MONITOR, \
    METHOD_BUFFERED, FILE_ANY_ACCESS)

typedef struct _INPUT_START_BUS_MONITOR {
    TERMINAL_HANDLE TerminalHandle;
} INPUT_START_BUS_MONITOR, *PINPUT_START_BUS_MONITOR;

typedef struct _OUTPUT_START_BUS_MONITOR {
    VIRTUAL1553B_STATUS Status;
} OUTPUT_START_BUS_MONITOR, *POUTPUT_START_BUS_MONITOR;


#define IOCTL_VIRTUAL1553B_STOP_TERMINAL \
    CTL_CODE(FILE_DEVICE_UNKNOWN, \
    VIRTUAL1553B_FUNCTION_BASE + FUNCTION_STOP_TERMINAL, \
    METHOD_BUFFERED, FILE_ANY_ACCESS)

typedef struct _INPUT_STOP_TERMINAL {
    TERMINAL_HANDLE TerminalHandle;
} INPUT_STOP_TERMINAL, *PINPUT_STOP_TERMINAL;

typedef struct _OUTPUT_STOP_TERMINAL {
    VIRTUAL1553B_STATUS Status;
} OUTPUT_STOP_TERMINAL, *POUTPUT_STOP_TERMINAL;


#define IOCTL_VIRTUAL1553B_BC_EXECUTE_TRANSFER \
    CTL_CODE(FILE_DEVICE_UNKNOWN, \
    VIRTUAL1553B_FUNCTION_BASE + FUNCTION_BC_EXECUTE_TRANSFER, \
    METHOD_BUFFERED, FILE_ANY_ACCESS)

typedef struct _INPUT_BC_EXECUTE_TRANSFER {
    TERMINAL_HANDLE TerminalHandle;
    USHORT TransferLine;
    EXECUTE_TRANSFER_BUFFER ExecuteTransferBuffer;
} INPUT_BC_EXECUTE_TRANSFER, *PINPUT_BC_EXECUTE_TRANSFER;

typedef struct _OUTPUT_BC_EXECUTE_TRANSFER {
    VIRTUAL1553B_STATUS Status;
} OUTPUT_BC_EXECUTE_TRANSFER, *POUTPUT_BC_EXECUTE_TRANSFER;


#define IOCTL_VIRTUAL1553B_BC_GET_TRANSFER_RESULT \
    CTL_CODE(FILE_DEVICE_UNKNOWN, \
    VIRTUAL1553B_FUNCTION_BASE + FUNCTION_BC_GET_TRANSFER_RESULT, \
    METHOD_BUFFERED, FILE_ANY_ACCESS)

typedef struct _INPUT_BC_GET_TRANSFER_RESULT {
    TERMINAL_HANDLE TerminalHandle;
} INPUT_BC_GET_TRANSFER_RESULT, *PINPUT_BC_GET_TRANSFER_RESULT;

typedef struct _OUTPUT_BC_GET_TRANSFER_RESULT {
    VIRTUAL1553B_STATUS Status;
    BC_TRANSFER_RESULT TransferResult;
} OUTPUT_BC_GET_TRANSFER_RESULT, *POUTPUT_BC_GET_TRANSFER_RESULT;


#define IOCTL_VIRTUAL1553B_RT_SET_ADDRESS \
    CTL_CODE(FILE_DEVICE_UNKNOWN, \
    VIRTUAL1553B_FUNCTION_BASE + FUNCTION_RT_SET_ADDRESS, \
    METHOD_BUFFERED, FILE_ANY_ACCESS)

typedef struct _INPUT_RT_SET_ADDRESS {
    TERMINAL_HANDLE TerminalHandle;
    USHORT Address;
} INPUT_RT_SET_ADDRESS, *PINPUT_RT_SET_ADDRESS;

typedef struct _OUTPUT_RT_SET_ADDRESS {
    VIRTUAL1553B_STATUS Status;
} OUTPUT_RT_SET_ADDRESS, *POUTPUT_RT_SET_ADDRESS;


#define IOCTL_VIRTUAL1553B_RT_GET_ADDRESS \
    CTL_CODE(FILE_DEVICE_UNKNOWN, \
    VIRTUAL1553B_FUNCTION_BASE + FUNCTION_RT_GET_ADDRESS, \
    METHOD_BUFFERED, FILE_ANY_ACCESS)

typedef struct _INPUT_RT_GET_ADDRESS {
    TERMINAL_HANDLE TerminalHandle;
} INPUT_RT_GET_ADDRESS, *PINPUT_RT_GET_ADDRESS;

typedef struct _OUTPUT_RT_GET_ADDRESS {
    VIRTUAL1553B_STATUS Status;
    USHORT Address;
} OUTPUT_RT_GET_ADDRESS, *POUTPUT_RT_GET_ADDRESS;


#define IOCTL_VIRTUAL1553B_RT_WRITE_MEMORY \
    CTL_CODE(FILE_DEVICE_UNKNOWN, \
    VIRTUAL1553B_FUNCTION_BASE + FUNCTION_RT_WRITE_MEMORY, \
    METHOD_BUFFERED, FILE_ANY_ACCESS)

typedef struct _INPUT_RT_WRITE_MEMORY {
    TERMINAL_HANDLE TerminalHandle;
    USHORT Direction;
    USHORT Subaddress;
    USHORT WordCountToWrite;
    USHORT Buffer[SUBADDRESS_LENGTH];
} INPUT_RT_WRITE_MEMORY, *PINPUT_RT_WRITE_MEMORY;

typedef struct _OUTPUT_RT_WRITE_MEMORY {
    VIRTUAL1553B_STATUS Status;
    USHORT WordCountWritten;
} OUTPUT_RT_WRITE_MEMORY, *POUTPUT_RT_WRITE_MEMORY;


#define IOCTL_VIRTUAL1553B_RT_READ_MEMORY \
    CTL_CODE(FILE_DEVICE_UNKNOWN, \
    VIRTUAL1553B_FUNCTION_BASE + FUNCTION_RT_READ_MEMORY, \
    METHOD_BUFFERED, FILE_ANY_ACCESS)

typedef struct _INPUT_RT_READ_MEMORY {
    TERMINAL_HANDLE TerminalHandle;
    USHORT Direction;
    USHORT Subaddress;
    USHORT WordCountToRead;
} INPUT_RT_READ_MEMORY, *PINPUT_RT_READ_MEMORY;

typedef struct _OUTPUT_RT_READ_MEMORY {
    VIRTUAL1553B_STATUS Status;
    USHORT WordCountRead;
    USHORT Buffer[SUBADDRESS_LENGTH];
} OUTPUT_RT_READ_MEMORY, *POUTPUT_RT_READ_MEMORY;


#define IOCTL_VIRTUAL1553B_RT_GET_TRANSFER_RESULT \
    CTL_CODE(FILE_DEVICE_UNKNOWN, \
    VIRTUAL1553B_FUNCTION_BASE + FUNCTION_RT_GET_TRANSFER_RESULT, \
    METHOD_BUFFERED, FILE_ANY_ACCESS)

typedef struct _INPUT_RT_GET_TRANSFER_RESULT {
    TERMINAL_HANDLE TerminalHandle;
} INPUT_RT_GET_TRANSFER_RESULT, *PINPUT_RT_GET_TRANSFER_RESULT;

typedef struct _OUTPUT_RT_GET_TRANSFER_RESULT {
    VIRTUAL1553B_STATUS Status;
    RT_TRANSFER_RESULT TransferResult;
} OUTPUT_RT_GET_TRANSFER_RESULT, *POUTPUT_RT_GET_TRANSFER_RESULT;


#define IOCTL_VIRTUAL1553B_BM_GET_CURRENT_BASE \
    CTL_CODE(FILE_DEVICE_UNKNOWN, \
    VIRTUAL1553B_FUNCTION_BASE + FUNCTION_BM_GET_CURRENT_BASE, \
    METHOD_BUFFERED, FILE_ANY_ACCESS)

typedef struct _INPUT_BM_GET_CURRENT_BASE {
    TERMINAL_HANDLE TerminalHandle;
} INPUT_BM_GET_CURRENT_BASE, *PINPUT_BM_GET_CURRENT_BASE;

typedef struct _OUTPUT_BM_GET_CURRENT_BASE {
    VIRTUAL1553B_STATUS Status;
    USHORT CurrentBase;
} OUTPUT_BM_GET_CURRENT_BASE, *POUTPUT_BM_GET_CURRENT_BASE;


#define IOCTL_VIRTUAL1553B_BM_GET_BASE_COUNT \
    CTL_CODE(FILE_DEVICE_UNKNOWN, \
    VIRTUAL1553B_FUNCTION_BASE + FUNCTION_BM_GET_BASE_COUNT, \
    METHOD_BUFFERED, FILE_ANY_ACCESS)

typedef struct _INPUT_BM_GET_BASE_COUNT {
    TERMINAL_HANDLE TerminalHandle;
} INPUT_BM_GET_BASE_COUNT, *PINPUT_BM_GET_BASE_COUNT;

typedef struct _OUTPUT_BM_GET_BASE_COUNT {
    VIRTUAL1553B_STATUS Status;
    USHORT BaseCount;
} OUTPUT_BM_GET_BASE_COUNT, *POUTPUT_BM_GET_BASE_COUNT;


#define IOCTL_VIRTUAL1553B_BM_GET_TRANSFER_RECORD \
    CTL_CODE(FILE_DEVICE_UNKNOWN, \
    VIRTUAL1553B_FUNCTION_BASE + FUNCTION_BM_GET_TRANSFER_RECORD, \
    METHOD_BUFFERED, FILE_ANY_ACCESS)

typedef struct _INPUT_BM_GET_TRANSFER_RECORD {
    TERMINAL_HANDLE TerminalHandle;
    USHORT TargetBase;
} INPUT_BM_GET_TRANSFER_RECORD, *PINPUT_BM_GET_TRANSFER_RECORD;

typedef struct _OUTPUT_BM_GET_TRANSFER_RECORD {
    VIRTUAL1553B_STATUS Status;
    TRANSFER_RECORD TransferRecord;
} OUTPUT_BM_GET_TRANSFER_RECORD, *POUTPUT_BM_GET_TRANSFER_RECORD;


#ifdef __cplusplus
}
#endif // __cplusplus


#include <poppack.h>
