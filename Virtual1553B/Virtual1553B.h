
#pragma once

#include "stdafx.h"
#include "Extension.h"
#include "DbgPrint.h"


#ifdef __cplusplus
extern "C" NTSTATUS DriverEntry(IN PDRIVER_OBJECT DriverObject, IN PUNICODE_STRING RegistryPath);
#endif // __cplusplus

VOID DriverUnload(IN PDRIVER_OBJECT DriverObject);
NTSTATUS DriverCreate(IN PDEVICE_OBJECT DeviceObject, IN PIRP Irp);
NTSTATUS DriverClose(IN PDEVICE_OBJECT DeviceObject, IN PIRP Irp);
NTSTATUS DriverDeviceControl(IN PDEVICE_OBJECT DeviceObject, IN PIRP Irp);

NTSTATUS CompleteIrp(IN PIRP Irp, IN NTSTATUS Status, IN ULONG_PTR Information);

VOID InitializeExtension(IN PVIRTUAL1553B_EXTENSION Extension);
VOID InitializeIoControlFunction(IN PVIRTUAL1553B_IOCTL_HANDLER IoControlFunction[]);


#define VIRTUAL1553B_COMPLETE_RETURN(Irp, Status, Information)  \
{                                                               \
    CompleteIrp((Irp), (Status), (Information));                \
    Virtual1553B_DbgPrintExit();                                \
    return (Status);                                            \
}

#define VIRTUAL1553B_COMPLETE_RETURN_0(Irp, Status, Information, Message)   \
{                                                                           \
    CompleteIrp((Irp), (Status), (Information));                            \
    Virtual1553B_DbgPrintExit_0(Message);                                   \
    return (Status);                                                        \
}

#define VIRTUAL1553B_COMPLETE_RETURN_1(Irp, Status, Information, Format, Arg)   \
{                                                                               \
    CompleteIrp((Irp), (Status), (Information));                                \
    Virtual1553B_DbgPrintExit_1(Format, (Arg));                                 \
    return (Status);                                                            \
}

#define VIRTUAL1553B_COMPLETE_RETURN_2(Irp, Status, Information, Format, Arg1, Arg2)    \
{                                                                                       \
    CompleteIrp((Irp), (Status), (Information));                                        \
    Virtual1553B_DbgPrintExit_2(Format, (Arg1), (Arg2));                                \
    return (Status);                                                                    \
}


#define AcquireVirtualTerminal(VirtualTerminal) \
    ExAcquireFastMutex(&(VirtualTerminal)->SynqMutex)

#define ReleaseVirtualTerminal(VirtualTerminal) \
    ExReleaseFastMutex(&(VirtualTerminal)->SynqMutex)


#define Virtual1553B_LockTransferLine(Line, VirtualTerminal) \
    (InterlockedCompareExchangePointer(&(Line)->Signal, (VirtualTerminal)->TerminalPointer, NULL) == NULL ? TRUE : FALSE)

#define Virtual1553B_UnlockTransferLine(Line, VirtualTerminal) \
    InterlockedCompareExchangePointer(&(Line)->Signal, NULL, (VirtualTerminal)->TerminalPointer)
