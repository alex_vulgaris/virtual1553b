
#include "stdafx.h"
#include "IoControlHandler.h"
#include "WorkerThreads.h"


NTSTATUS DriverEntry(IN PDRIVER_OBJECT DriverObject, IN PUNICODE_STRING RegistryPath)
{
    Virtual1553B_DbgPrintSeparator();
    Virtual1553B_DbgPrintEntry();

    UNREFERENCED_PARAMETER(RegistryPath);

    DriverObject->DriverUnload = DriverUnload;
    DriverObject->MajorFunction[IRP_MJ_CREATE] = DriverCreate;
    DriverObject->MajorFunction[IRP_MJ_CLOSE] = DriverClose;
    DriverObject->MajorFunction[IRP_MJ_DEVICE_CONTROL] = DriverDeviceControl;

    NTSTATUS Status = STATUS_SUCCESS;

    UNICODE_STRING DeviceName = { 0 };
    RtlInitUnicodeString(&DeviceName, L"\\Device\\Virtual1553B0");

    PDEVICE_OBJECT DeviceObject = NULL;
    Status = IoCreateDevice(
        DriverObject,
        sizeof(VIRTUAL1553B_EXTENSION),
        &DeviceName,
        FILE_DEVICE_UNKNOWN,
        0,
        FALSE,
        &DeviceObject);

    if (!NT_SUCCESS(Status))
    {
        Virtual1553B_DbgPrintExit_1("IoCreateDevice() failed. Error: %08X.", Status);
        return Status;
    }
    if (!DeviceObject)
    {
        Virtual1553B_DbgPrintExit_1("IoCreateDevice() failed. Error: %08X.", STATUS_UNEXPECTED_IO_ERROR);
        return STATUS_UNEXPECTED_IO_ERROR;
    }

    UNICODE_STRING Win32Device = { 0 };
    RtlInitUnicodeString(&Win32Device, L"\\DosDevices\\Virtual1553B0");

    Status = IoCreateSymbolicLink(&Win32Device, &DeviceName);

    if (!NT_SUCCESS(Status))
    {
        IoDeleteDevice(DeviceObject);

        Virtual1553B_DbgPrintExit_1("IoCreateSymbolicLink() failed. Error: %08X.", Status);
        return Status;
    }

    InitializeExtension((PVIRTUAL1553B_EXTENSION)(DeviceObject->DeviceExtension));

    Virtual1553B_DbgPrintExit_0("Success.");
    return STATUS_SUCCESS;
}


VOID DriverUnload(IN PDRIVER_OBJECT DriverObject)
{
    Virtual1553B_DbgPrintSeparator();
    Virtual1553B_DbgPrintEntry();

    UNICODE_STRING Win32Device = { 0 };
    RtlInitUnicodeString(&Win32Device, L"\\DosDevices\\Virtual1553B0");
    IoDeleteSymbolicLink(&Win32Device);
    IoDeleteDevice(DriverObject->DeviceObject);

    Virtual1553B_DbgPrintExit();
}


NTSTATUS DriverCreate(IN PDEVICE_OBJECT DeviceObject, IN PIRP Irp)
{
    Virtual1553B_DbgPrintSeparator();
    Virtual1553B_DbgPrintEntry();

    UNREFERENCED_PARAMETER(DeviceObject);

    Virtual1553B_DbgPrint_2("Process ID: 0x%p, Reference count: %ld.",
        (PVOID)PsGetCurrentProcessId(), DeviceObject->ReferenceCount);

    VIRTUAL1553B_COMPLETE_RETURN(Irp, STATUS_SUCCESS, 0);
}


NTSTATUS DriverClose(IN PDEVICE_OBJECT DeviceObject, IN PIRP Irp)
{
    Virtual1553B_DbgPrintSeparator();
    Virtual1553B_DbgPrintEntry();

    HANDLE ProcessId = PsGetCurrentProcessId();

    Virtual1553B_DbgPrint_2("Process ID: 0x%p, Reference count: %ld.",
        (PVOID)(ProcessId), DeviceObject->ReferenceCount);

    PVIRTUAL1553B_EXTENSION Extension = (PVIRTUAL1553B_EXTENSION)(DeviceObject->DeviceExtension);

    for (LONG_PTR TerminalIndex = 0; TerminalIndex < VIRTUAL_TERMINAL_NUMBER; ++TerminalIndex)
    {
        PVIRTUAL_TERMINAL VirtualTerminal = &Extension->VirtualTerminal[TerminalIndex];

        if (VirtualTerminal->ProcessIdOwner == ProcessId)
        {
            AcquireVirtualTerminal(VirtualTerminal);

            if (VirtualTerminal->Mode != UndefinedMode)
            {
                switch (VirtualTerminal->Mode)
                {
                case BusControllerMode:
                    Virtual1553B_CloseWorkerThreads(VirtualTerminal);
                    break;

                case RemoteTerminalMode:
                    Virtual1553B_RevokeRemoteTerminal(&Extension->TerminalMap, VirtualTerminal);
                    break;

                case BusMonitorMode:
                    Virtual1553B_RevokeBusMonitor(&Extension->TerminalMap, VirtualTerminal);
                    break;
                }

                VirtualTerminal->Mode = UndefinedMode;
                PVOID TerminalPointer = InterlockedExchangePointer(&VirtualTerminal->TerminalPointer, NULL);
                ExFreePoolWithTag(TerminalPointer, 0);
            }

            PVOID InterruptEvent = InterlockedExchangePointer(&VirtualTerminal->InterruptEvent, NULL);
            VirtualTerminal->ProcessIdOwner = NULL;

            ReleaseVirtualTerminal(VirtualTerminal);

            if (InterruptEvent)
            {
                ObDereferenceObject(InterruptEvent);
            }

            Virtual1553B_DbgPrint_2("Process ID: 0x%p, Terminal index: %ld.",
                (PVOID)(ProcessId), (LONG)(TerminalIndex));
        }
    }

    VIRTUAL1553B_COMPLETE_RETURN(Irp, STATUS_SUCCESS, 0);
}


NTSTATUS DriverDeviceControl(IN PDEVICE_OBJECT DeviceObject, IN PIRP Irp)
{
    Virtual1553B_DbgPrintSeparator();
    Virtual1553B_DbgPrintEntry();

    NTSTATUS Status = STATUS_SUCCESS;
    PVIRTUAL1553B_EXTENSION Extension = (PVIRTUAL1553B_EXTENSION)(DeviceObject->DeviceExtension);
    PIO_STACK_LOCATION IrpStackLocation = IoGetCurrentIrpStackLocation(Irp);
    ULONG IoControlCode = IrpStackLocation->Parameters.DeviceIoControl.IoControlCode;

    switch (IoControlCode)
    {
    case IOCTL_VIRTUAL1553B_ALLOC_TERMINAL:
    case IOCTL_VIRTUAL1553B_FREE_TERMINAL:

    case IOCTL_VIRTUAL1553B_INSTALL_EVENT:
    case IOCTL_VIRTUAL1553B_REMOVE_EVENT:

    case IOCTL_VIRTUAL1553B_GET_TERMINAL_MODE:
    case IOCTL_VIRTUAL1553B_START_BUS_CONTROLLER:
    case IOCTL_VIRTUAL1553B_START_REMOTE_TERMINAL:
    case IOCTL_VIRTUAL1553B_START_BUS_MONITOR:
    case IOCTL_VIRTUAL1553B_STOP_TERMINAL:

    case IOCTL_VIRTUAL1553B_BC_EXECUTE_TRANSFER:
    case IOCTL_VIRTUAL1553B_BC_GET_TRANSFER_RESULT:

    case IOCTL_VIRTUAL1553B_RT_SET_ADDRESS:
    case IOCTL_VIRTUAL1553B_RT_GET_ADDRESS:
    case IOCTL_VIRTUAL1553B_RT_WRITE_MEMORY:
    case IOCTL_VIRTUAL1553B_RT_READ_MEMORY:
    case IOCTL_VIRTUAL1553B_RT_GET_TRANSFER_RESULT:

    case IOCTL_VIRTUAL1553B_BM_GET_CURRENT_BASE:
    case IOCTL_VIRTUAL1553B_BM_GET_BASE_COUNT:
    case IOCTL_VIRTUAL1553B_BM_GET_TRANSFER_RECORD:

        Status = Extension->IoControlFunction[VIRTUAL1553B_FUNCTION_FROM_IOCTL(IoControlCode)](
            Extension,
            IrpStackLocation->Parameters.DeviceIoControl.InputBufferLength,
            IrpStackLocation->Parameters.DeviceIoControl.OutputBufferLength,
            Irp);

        Virtual1553B_DbgPrintExit();
        return Status;

    default:
        break;
    }

    VIRTUAL1553B_COMPLETE_RETURN_0(Irp, STATUS_INVALID_DEVICE_REQUEST, 0, "Invalid device request.");
}


NTSTATUS CompleteIrp(IN PIRP Irp, IN NTSTATUS Status, IN ULONG_PTR Information)
{
    Irp->IoStatus.Status = Status;
    Irp->IoStatus.Information = Information;
    IoCompleteRequest(Irp, IO_NO_INCREMENT);
    return Status;
}


VOID InitializeExtension(IN PVIRTUAL1553B_EXTENSION Extension)
{
    Virtual1553B_DbgPrintEntry();

    RtlZeroMemory(Extension, sizeof(VIRTUAL1553B_EXTENSION));

    InitializeIoControlFunction(Extension->IoControlFunction);

    PVIRTUAL1553B_TRANSFER_LINE Line = Extension->Bus.Line;
    for (USHORT i = 0; i < TransferLineNumber; ++i)
    {
        Line[i].TransferLine = i;
    }

    PVIRTUAL_TERMINAL VirtualTerminal = Extension->VirtualTerminal;
    for (LONG_PTR TerminalIndex = 0; TerminalIndex < VIRTUAL_TERMINAL_NUMBER; ++TerminalIndex)
    {
        ExInitializeFastMutex(&VirtualTerminal[TerminalIndex].SynqMutex);
    }

    ExInitializeFastMutex(&Extension->TerminalMap.SynqMutex);

    Virtual1553B_DbgPrintExit();
}


VOID InitializeIoControlFunction(IN PVIRTUAL1553B_IOCTL_HANDLER IoControlFunction[])
{
    Virtual1553B_DbgPrintEntry();

    IoControlFunction[FUNCTION_ALLOC_TERMINAL] = Virtual1553B_AllocTerminal;
    IoControlFunction[FUNCTION_FREE_TERMINAL] = Virtual1553B_FreeTerminal;

    IoControlFunction[FUNCTION_INSTALL_EVENT] = Virtual1553B_InstallEvent;
    IoControlFunction[FUNCTION_REMOVE_EVENT] = Virtual1553B_RemoveEvent;

    IoControlFunction[FUNCTION_GET_TERMINAL_MODE] = Virtual1553B_GetTerminalMode;
    IoControlFunction[FUNCTION_START_BUS_CONTROLLER] = Virtual1553B_StartBusController;
    IoControlFunction[FUNCTION_START_REMOTE_TERMINAL] = Virtual1553B_StartRemoteTerminal;
    IoControlFunction[FUNCTION_START_BUS_MONITOR] = Virtual1553B_StartBusMonitor;
    IoControlFunction[FUNCTION_STOP_TERMINAL] = Virtual1553B_StopTerminal;

    IoControlFunction[FUNCTION_BC_EXECUTE_TRANSFER] = VirtualBC_ExecuteTransfer;
    IoControlFunction[FUNCTION_BC_GET_TRANSFER_RESULT] = VirtualBC_GetTransferResult;

    IoControlFunction[FUNCTION_RT_SET_ADDRESS] = VirtualRT_SetAddress;
    IoControlFunction[FUNCTION_RT_GET_ADDRESS] = VirtualRT_GetAddress;
    IoControlFunction[FUNCTION_RT_WRITE_MEMORY] = VirtualRT_WriteMemory;
    IoControlFunction[FUNCTION_RT_READ_MEMORY] = VirtualRT_ReadMemory;
    IoControlFunction[FUNCTION_RT_GET_TRANSFER_RESULT] = VirtualRT_GetTransferResult;

    IoControlFunction[FUNCTION_BM_GET_CURRENT_BASE] = VirtualBM_GetCurrentBase;
    IoControlFunction[FUNCTION_BM_GET_BASE_COUNT] = VirtualBM_GetBaseCount;
    IoControlFunction[FUNCTION_BM_GET_TRANSFER_RECORD] = VirtualBM_GetTransferRecord;

    Virtual1553B_DbgPrintExit();
}
