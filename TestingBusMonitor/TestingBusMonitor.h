
#pragma once

#include "stdafx.h"
#include "..\VirtualTerminalDriver\VirtualTerminalDriver.h"
#include "..\VirtualTerminalDriver\Utils.h"

#define STRSAFE_NO_DEPRECATE
#include <strsafe.h>


#define BUS_MONITOR_OUTPUT_FILES_DIRECTORY  ".\\BusMonitor_OutputFiles\\"


void PrintHelp(void);
BOOL RunCommand(LPCWSTR lpCommand);
BOOL CreateOutputFilesDirectory(void);
void MakeOutputFileName(LPSTR lpFileName, size_t nBufferLen);
BOOL CreateOutputFile(void);
void InterruptProcessing(USHORT wTargetBase);

DWORD WINAPI ListenThread(LPVOID lpParameter);
