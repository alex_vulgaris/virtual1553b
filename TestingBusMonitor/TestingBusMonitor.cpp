// TestingBusMonitor.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "TestingBusMonitor.h"


volatile HANDLE g_hOutputFile = INVALID_HANDLE_VALUE;
HANDLE g_hInterrupt;
HANDLE g_hTerminate;
TERMINAL_HANDLE g_hTerminal = INVALID_TERMINAL_HANDLE;
USHORT g_wPrevBase;
USHORT g_wMaxBase;
DWORD g_dwCounter;
LONGLONG g_qwTimeStart;
LONGLONG g_qwFrequency;


int _tmain(int argc, _TCHAR* argv[])
{
    int nExitCode = EXIT_SUCCESS;

    BOOL bResult = CreateOutputFilesDirectory();
    if (!bResult)
    {
        nExitCode = EXIT_FAILURE;
        goto just_exit;
    }

    bResult = CreateOutputFile();
    if (!bResult)
    {
        nExitCode = EXIT_FAILURE;
        goto just_exit;
    }

    g_hInterrupt = CreateEvent(NULL, FALSE, FALSE, NULL);
    if (!g_hInterrupt)
    {
        _cprintf("CreateEvent() failed.\n");
        nExitCode = EXIT_FAILURE;
        goto close_output_file;
    }

    g_hTerminate = CreateEvent(NULL, TRUE, FALSE, NULL);
    if (!g_hTerminate)
    {
        _cprintf("CreateEvent() failed.\n");
        nExitCode = EXIT_FAILURE;
        goto close_interrupt_event_handle;
    }

    bResult = Virtual1553B_DriverOpen();
    if (!bResult)
    {
        _cprintf("Virtual1553B_DriverOpen() failed.\n");
        nExitCode = EXIT_FAILURE;
        goto close_event_handles;
    }

    VIRTUAL1553B_STATUS status = Virtual1553B_AllocTerminal(&g_hTerminal);
    if (VIRTUAL1553B_ERROR(status))
    {
        _cprintf("Virtual1553B_AllocTerminal() failed.\n");
        nExitCode = EXIT_FAILURE;
        goto driver_close;
    }

    status = Virtual1553B_StartBusMonitor(g_hTerminal);
    if (VIRTUAL1553B_ERROR(status))
    {
        _cprintf("Virtual1553B_StartBusMonitor() failed.\n");
        nExitCode = EXIT_FAILURE;
        goto free_terminal;
    }

    USHORT mode = UndefinedMode;
    status = Virtual1553B_GetTerminalMode(g_hTerminal, &mode);

    status = VirtualBM_GetBaseCount(g_hTerminal, &g_wMaxBase);
    if (VIRTUAL1553B_ERROR(status))
    {
        _cprintf("VirtualBM_GetBaseCount() failed.\n");
        nExitCode = EXIT_FAILURE;
        goto stop_terminal;
    }

    status = Virtual1553B_InstallEvent(g_hTerminal, g_hInterrupt);
    if (VIRTUAL1553B_ERROR(status))
    {
        _cprintf("Virtual1553B_InstallEvent() failed.\n");
        nExitCode = EXIT_FAILURE;
        goto stop_terminal;
    }

    HANDLE hReceiveThread = CreateThread(NULL, 0, ListenThread, NULL, 0, NULL);
    if (!hReceiveThread)
    {
        _cprintf("CreateThread() failed.\n");
        nExitCode = EXIT_FAILURE;
        goto full_closing;
    }

    while (true)
    {
        WCHAR szBuffer[256];
        size_t nSizeRead = 0;

        _cprintf("Enter the command -> ");
        _cgetws_s(szBuffer, (sizeof(szBuffer) / sizeof(*szBuffer)), &nSizeRead);

        if (!RunCommand(szBuffer))
        {
            break;
        }
    }

    SetEvent(g_hTerminate);
    WaitForSingleObject(hReceiveThread, INFINITE);
    CloseHandle(hReceiveThread);

full_closing:
    status = Virtual1553B_RemoveEvent(g_hTerminal);
    if (VIRTUAL1553B_ERROR(status))
    {
        _cprintf("Virtual1553B_RemoveEvent() failed.\n");
        nExitCode = EXIT_FAILURE;
    }

stop_terminal:
    status = Virtual1553B_StopTerminal(g_hTerminal);
    if (VIRTUAL1553B_ERROR(status))
    {
        _cprintf("Virtual1553B_StopTerminal() failed.\n");
        nExitCode = EXIT_FAILURE;
    }

free_terminal:
    status = Virtual1553B_FreeTerminal(g_hTerminal);
    if (VIRTUAL1553B_ERROR(status))
    {
        _cprintf("Virtual1553B_FreeTerminal() failed.\n");
        nExitCode = EXIT_FAILURE;
    }

driver_close:
    bResult = Virtual1553B_DriverClose();
    if (!bResult)
    {
        _cprintf("Virtual1553B_DriverClose() failed.\n");
        nExitCode = EXIT_FAILURE;
    }

close_event_handles:
    CloseHandle(g_hTerminate);

close_interrupt_event_handle:
    CloseHandle(g_hInterrupt);

close_output_file:
    CloseHandle(g_hOutputFile);

just_exit:
    _cprintf("Press any key to exit...");
    _getch();
    return nExitCode;
}


void PrintHelp(void)
{
    _cprintf(
        "Commands:\n"
        "COUNT\t\tInterrupt count.\n"
        "HELP\t\tReference information.\n"
        "NEWFILE\t\tCreate a new output file.\n"
        "EXIT\t\tShutdown the bus monitor and quit the program.\n\n");
}


BOOL RunCommand(LPCWSTR lpCommand)
{
    if (!lstrcmpiW(lpCommand, L"COUNT"))
    {
        _cprintf("Interrupt count: %lu.\n\n", g_dwCounter);
        return TRUE;
    }
    if (!lstrcmpiW(lpCommand, L"HELP"))
    {
        PrintHelp();
        return TRUE;
    }
    if (!lstrcmpiW(lpCommand, L"NEWFILE"))
    {
        CreateOutputFile();
        return TRUE;
    }
    if (!lstrcmpiW(lpCommand, L"EXIT"))
    {
        return FALSE;
    }

    _cprintf("Incorrect command.\n\n");
    return TRUE;
}


BOOL CreateOutputFilesDirectory(void)
{
    if (CreateDirectoryA(BUS_MONITOR_OUTPUT_FILES_DIRECTORY, NULL) ||
        GetLastError() == ERROR_ALREADY_EXISTS)
    {
        return TRUE;
    }

    _cprintf("CreateDirectoryA() failed.\n");
    return FALSE;
}


void MakeOutputFileName(LPSTR lpFileName, size_t nBufferLen)
{
    SYSTEMTIME sysTime = { 0 };
    GetLocalTime(&sysTime);

    StringCbPrintfA(lpFileName, nBufferLen,
        "%04u%02u%02u-%02u%02u%02u.txt",
        sysTime.wYear, sysTime.wMonth, sysTime.wDay,
        sysTime.wHour, sysTime.wMinute, sysTime.wSecond);
}


BOOL CreateOutputFile(void)
{
    CHAR szFileName[64];
    MakeOutputFileName(szFileName, sizeof(szFileName));

    CHAR szFullPath[128] = BUS_MONITOR_OUTPUT_FILES_DIRECTORY;
    StringCbCatA(szFullPath, sizeof(szFullPath), szFileName);

    HANDLE hFile = CreateFileA(szFullPath, GENERIC_WRITE, FILE_SHARE_READ, NULL, CREATE_NEW, 0, NULL);
    if (hFile == INVALID_HANDLE_VALUE)
    {
        _cprintf("CreateFileA() failed.\n");
        return FALSE;
    }

    hFile = InterlockedExchangePointer(&g_hOutputFile, hFile);
    if (hFile != INVALID_HANDLE_VALUE)
    {
        CloseHandle(hFile);
    }

    _cprintf("Output file name: \"%hs\".\n\n", szFileName);
    return TRUE;
}


void InterruptProcessing(USHORT wTargetBase)
{
    TRANSFER_RECORD record = { 0 };
    VIRTUAL1553B_STATUS status = VirtualBM_GetTransferRecord(g_hTerminal, wTargetBase, &record);
    if (VIRTUAL1553B_ERROR(status))
    {
        return;
    }

    if (g_qwTimeStart)
    {
        record.Time.QuadPart = (LONGLONG)(1.0e6 * (record.Time.QuadPart - g_qwTimeStart) / g_qwFrequency);
    }
    else
    {
        g_qwTimeStart = record.Time.QuadPart;
        record.Time.QuadPart = 0;
    }

    CHAR szHexBuffer[8];
    CHAR szOutputBuffer[512];
    CHAR *pOutBuf = szOutputBuffer;
    pOutBuf += wsprintfA(pOutBuf, "%10lu\t%ld\t", record.Time.LowPart, record.Time.HighPart);
    pOutBuf += wsprintfA(pOutBuf, "%s\t", WordDigitToCharactersHex(record.Error, szHexBuffer));
    pOutBuf += wsprintfA(pOutBuf, "%hu\t%hu\t", record.TransferLine, record.Format);
    pOutBuf += wsprintfA(pOutBuf, "%s\t", WordDigitToCharactersHex(record.CW1.Raw, szHexBuffer));
    pOutBuf += wsprintfA(pOutBuf, "%s\t", WordDigitToCharactersHex(record.CW2.Raw, szHexBuffer));
    pOutBuf += wsprintfA(pOutBuf, "%s\t", WordDigitToCharactersHex(record.SW1.Raw, szHexBuffer));
    pOutBuf += wsprintfA(pOutBuf, "%s\t", WordDigitToCharactersHex(record.SW2.Raw, szHexBuffer));
    for (int i = 0; i < DATA_WORD_COUNT_MAXIMUM; ++i)
    {
        pOutBuf += wsprintfA(pOutBuf, "%s\t", WordDigitToCharactersHex(record.Data[i], szHexBuffer));
    }
    pOutBuf += wsprintfA(pOutBuf, "#\t%s\t", WordDigitToCharactersHex(wTargetBase, szHexBuffer));
    pOutBuf += wsprintfA(pOutBuf, "%s\n", WordDigitToCharactersHex((WORD)(g_dwCounter++), szHexBuffer));

    DWORD dwWritten = 0;
    WriteFile(g_hOutputFile, szOutputBuffer, (pOutBuf - szOutputBuffer), &dwWritten, NULL);
}


DWORD WINAPI ListenThread(LPVOID lpParameter)
{
    QueryPerformanceFrequency((LARGE_INTEGER *)(&g_qwFrequency));

    for ( ; ; )
    {
        HANDLE hObjects[] = { g_hInterrupt, g_hTerminate };
        if (WaitForMultipleObjects((sizeof(hObjects) / sizeof(*hObjects)), hObjects, FALSE, INFINITE) != WAIT_OBJECT_0)
        {
            break;
        }

        USHORT wCurrentBase = (USHORT)(-1);
        VIRTUAL1553B_STATUS status = VirtualBM_GetCurrentBase(g_hTerminal, &wCurrentBase);
        if (VIRTUAL1553B_ERROR(status))
        {
            break;
        }

        if (wCurrentBase < g_wPrevBase)
        {
            wCurrentBase += g_wMaxBase;
        }

        while (g_wPrevBase < wCurrentBase)
        {
            InterruptProcessing(g_wPrevBase++ % g_wMaxBase);
        }

        g_wPrevBase %= g_wMaxBase;
    }

    return 0;
}
