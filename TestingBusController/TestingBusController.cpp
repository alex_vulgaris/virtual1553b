// TestingBusController.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "TestingBusController.h"


HANDLE g_hInterrupt;
TERMINAL_HANDLE g_hTerminal = INVALID_TERMINAL_HANDLE;

double g_dblDiffTime = 0.0;
DWORD g_dwCount;
DWORD g_dwSuccessfully;


int _tmain(int argc, _TCHAR* argv[])
{
    int nExitCode = EXIT_SUCCESS;

    g_hInterrupt = CreateEvent(NULL, FALSE, FALSE, NULL);
    if (!g_hInterrupt)
    {
        _cprintf("CreateEvent() failed.\n");
        nExitCode = EXIT_FAILURE;
        goto just_exit;
    }

    BOOL bResult = Virtual1553B_DriverOpen();
    if (!bResult)
    {
        _cprintf("Virtual1553B_DriverOpen() failed.\n");
        nExitCode = EXIT_FAILURE;
        goto close_interrupt_event_handle;
    }

    VIRTUAL1553B_STATUS status = Virtual1553B_AllocTerminal(&g_hTerminal);
    if (VIRTUAL1553B_ERROR(status))
    {
        _cprintf("Virtual1553B_AllocTerminal() failed.\n");
        nExitCode = EXIT_FAILURE;
        goto driver_close;
    }

    status = Virtual1553B_StartBusController(g_hTerminal);
    if (VIRTUAL1553B_ERROR(status))
    {
        _cprintf("Virtual1553B_StartBusController() failed.\n");
        nExitCode = EXIT_FAILURE;
        goto free_terminal;
    }

    USHORT mode = UndefinedMode;
    status = Virtual1553B_GetTerminalMode(g_hTerminal, &mode);

    status = Virtual1553B_InstallEvent(g_hTerminal, g_hInterrupt);
    if (VIRTUAL1553B_ERROR(status))
    {
        _cprintf("Virtual1553B_InstallEvent() failed.\n");
        nExitCode = EXIT_FAILURE;
        goto stop_terminal;
    }

    if (!Format01()) // TransferBCRT
    {
        goto full_closing;
    }
    if (!Format02()) // TransferRTBC
    {
        goto full_closing;
    }
    if (!Format03()) // TransferRTRT
    {
        goto full_closing;
    }
    if (!Format04()) // ModeCommandWithoutData
    {
        goto full_closing;
    }
    if (!Format05()) // ModeCommandWithDataTransmit
    {
        goto full_closing;
    }
    if (!Format06()) // ModeCommandWithDataReceive
    {
        goto full_closing;
    }
    if (!Format07()) // TransferBCRTBroadcast
    {
        goto full_closing;
    }
    if (!Format08()) // TransferRTRTBroadcast
    {
        goto full_closing;
    }
    if (!Format09()) // ModeCommandWithoutDataBroadcast
    {
        goto full_closing;
    }
    if (!Format10()) // ModeCommandWithDataReceiveBroadcast
    {
        goto full_closing;
    }

full_closing:
    status = Virtual1553B_RemoveEvent(g_hTerminal);
    if (VIRTUAL1553B_ERROR(status))
    {
        _cprintf("Virtual1553B_RemoveEvent() failed.\n");
        nExitCode = EXIT_FAILURE;
    }

stop_terminal:
    status = Virtual1553B_StopTerminal(g_hTerminal);
    if (VIRTUAL1553B_ERROR(status))
    {
        _cprintf("Virtual1553B_StopTerminal() failed.\n");
        nExitCode = EXIT_FAILURE;
    }

free_terminal:
    status = Virtual1553B_FreeTerminal(g_hTerminal);
    if (VIRTUAL1553B_ERROR(status))
    {
        _cprintf("Virtual1553B_FreeTerminal() failed.\n");
        nExitCode = EXIT_FAILURE;
    }

driver_close:
    bResult = Virtual1553B_DriverClose();
    if (!bResult)
    {
        _cprintf("Virtual1553B_DriverClose() failed.\n");
        nExitCode = EXIT_FAILURE;
    }

close_interrupt_event_handle:
    CloseHandle(g_hInterrupt);

just_exit:
    _cprintf("\nPress any key to exit...");
    _getch();
    return nExitCode;
}


BOOL Format01(void)
{
    _cprintf("TransferBCRT.\nPress any key to continue...");
    if (_getch() == 27)
    {
        return FALSE;
    }

    g_dblDiffTime = 0.0;
    g_dwCount = 0;
    g_dwSuccessfully = 0;

    COMMAND_WORD CW1 = { 0 };
    USHORT data[DATA_WORD_COUNT_MAXIMUM] = {
         1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16,
        17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32
    };
    EXECUTE_TRANSFER_BUFFER buffer = { 0 };
    BC_TRANSFER_RESULT result = { 0 };

    CW1.Raw = BUILD_COMMAND_WORD_RECEIVE(20, SUBADDRESS_MAXIMUM, DATA_WORD_COUNT_MAXIMUM);
    VirtualBC_BuildExecuteTransferBuffer(&buffer, TransferBCRT, CW1.Raw, INVALID_COMMAND_WORD, data);
    if (VirtualBC_IsValidExecuteTransferBuffer(&buffer))
    {
        for (int i = 0; i < NUMBER_OF_ITERATIONS; ++i)
        {
            TIME_MEASURE_CALL(TimeResultCall);
            VIRTUAL1553B_STATUS status = VirtualBC_ExecuteTransfer(g_hTerminal, TransferLineA, &buffer);
            if (VIRTUAL1553B_SUCCESS(status))
            {
                WaitForSingleObject(g_hInterrupt, INFINITE);
                status = VirtualBC_GetTransferResult(g_hTerminal, &result);
                if (VIRTUAL1553B_SUCCESS(status) && (result.Error == VIRTUAL1553B_ERROR_SUCCESS))
                {
                    ++g_dwSuccessfully;
                }
            }
        }
    }

    _cprintf(
        "\n\nDelay: %lf\n"
        "Successfully: %lu\n\n",
        (g_dwCount ? g_dblDiffTime / g_dwCount : 0.0),
        g_dwSuccessfully);

    _cprintf(
        "Command word: %X\n"
        "Error: %X\n"
        "Status word: %X\n"
        "RT address: %hu\n"
        "Flags: %X\n\n",
        CW1.Raw, result.Error,
        result.SW1.Raw, result.SW1.RemoteTerminalAddress, result.SW1.Flags);

    return TRUE;
}


BOOL Format02(void)
{
    _cprintf("TransferRTBC.\nPress any key to continue...");
    if (_getch() == 27)
    {
        return FALSE;
    }

    g_dblDiffTime = 0.0;
    g_dwCount = 0;
    g_dwSuccessfully = 0;

    COMMAND_WORD CW1 = { 0 };
    EXECUTE_TRANSFER_BUFFER buffer = { 0 };
    BC_TRANSFER_RESULT result = { 0 };

    CW1.Raw = BUILD_COMMAND_WORD_TRANSMIT(20, SUBADDRESS_MAXIMUM, DATA_WORD_COUNT_MAXIMUM);
    VirtualBC_BuildExecuteTransferBuffer(&buffer, TransferRTBC, CW1.Raw, INVALID_COMMAND_WORD, NULL);
    if (VirtualBC_IsValidExecuteTransferBuffer(&buffer))
    {
        for (int i = 0; i < NUMBER_OF_ITERATIONS; ++i)
        {
            TIME_MEASURE_CALL(TimeResultCall);
            VIRTUAL1553B_STATUS status = VirtualBC_ExecuteTransfer(g_hTerminal, TransferLineA, &buffer);
            if (VIRTUAL1553B_SUCCESS(status))
            {
                WaitForSingleObject(g_hInterrupt, INFINITE);
                status = VirtualBC_GetTransferResult(g_hTerminal, &result);
                if (VIRTUAL1553B_SUCCESS(status) && (result.Error == VIRTUAL1553B_ERROR_SUCCESS))
                {
                    ++g_dwSuccessfully;
                }
            }
        }
    }

    _cprintf(
        "\n\nDelay: %lf\n"
        "Successfully: %lu\n\n",
        (g_dwCount ? g_dblDiffTime / g_dwCount : 0.0),
        g_dwSuccessfully);

    CHAR szBuffer[256] = { 0 };
    int nBufLen = 0;
    WordDigitsToCharactersHexArray(result.Data, DATA_WORD_COUNT_MAXIMUM, szBuffer, &nBufLen);

    _cprintf(
        "Command word: %X\n"
        "Error: %X\n"
        "Status word: %X\n"
        "RT address: %hu\n"
        "Flags: %X\n"
        "Data words: %hs\n",
        CW1.Raw, result.Error,
        result.SW1.Raw, result.SW1.RemoteTerminalAddress, result.SW1.Flags,
        szBuffer);

    return TRUE;
}


BOOL Format03(void)
{
    _cprintf("TransferRTRT.\nPress any key to continue...");
    if (_getch() == 27)
    {
        return FALSE;
    }

    g_dblDiffTime = 0.0;
    g_dwCount = 0;
    g_dwSuccessfully = 0;

    COMMAND_WORD CW1 = { 0 };
    COMMAND_WORD CW2 = { 0 };
    EXECUTE_TRANSFER_BUFFER buffer = { 0 };
    BC_TRANSFER_RESULT result = { 0 };

    CW1.Raw = BUILD_COMMAND_WORD_RECEIVE(20, SUBADDRESS_MAXIMUM, DATA_WORD_COUNT_MAXIMUM);
    CW2.Raw = BUILD_COMMAND_WORD_TRANSMIT(10, SUBADDRESS_MAXIMUM, DATA_WORD_COUNT_MAXIMUM);
    VirtualBC_BuildExecuteTransferBuffer(&buffer, TransferRTRT, CW1.Raw, CW2.Raw, NULL);
    if (VirtualBC_IsValidExecuteTransferBuffer(&buffer))
    {
        for (int i = 0; i < NUMBER_OF_ITERATIONS; ++i)
        {
            TIME_MEASURE_CALL(TimeResultCall);
            VIRTUAL1553B_STATUS status = VirtualBC_ExecuteTransfer(g_hTerminal, TransferLineA, &buffer);
            if (VIRTUAL1553B_SUCCESS(status))
            {
                WaitForSingleObject(g_hInterrupt, INFINITE);
                status = VirtualBC_GetTransferResult(g_hTerminal, &result);
                if (VIRTUAL1553B_SUCCESS(status) && (result.Error == VIRTUAL1553B_ERROR_SUCCESS))
                {
                    ++g_dwSuccessfully;
                }
            }
        }
    }

    _cprintf(
        "\n\nDelay: %lf\n"
        "Successfully: %lu\n\n",
        (g_dwCount ? g_dblDiffTime / g_dwCount : 0.0),
        g_dwSuccessfully);

    CHAR szBuffer[256] = { 0 };
    int nBufLen = 0;
    WordDigitsToCharactersHexArray(result.Data, DATA_WORD_COUNT_MAXIMUM, szBuffer, &nBufLen);

    _cprintf(
        "Command word 1: %X\n"
        "Command word 2: %X\n"
        "Error: %X\n"
        "Status word 1: %X\n"
        "RT address 1: %hu\n"
        "Flags 1: %X\n"
        "Status word 2: %X\n"
        "RT address 2: %hu\n"
        "Flags 2: %X\n"
        "Data words: %hs\n",
        CW1.Raw, CW2.Raw, result.Error,
        result.SW1.Raw, result.SW1.RemoteTerminalAddress, result.SW1.Flags,
        result.SW2.Raw, result.SW2.RemoteTerminalAddress, result.SW2.Flags,
        szBuffer);

    return TRUE;
}


BOOL Format04(void)
{
    _cprintf("ModeCommandWithoutData.\nPress any key to continue...");
    if (_getch() == 27)
    {
        return FALSE;
    }

    g_dblDiffTime = 0.0;
    g_dwCount = 0;
    g_dwSuccessfully = 0;

    COMMAND_WORD CW1 = { 0 };
    EXECUTE_TRANSFER_BUFFER buffer = { 0 };
    BC_TRANSFER_RESULT result = { 0 };

    CW1.Raw = BUILD_COMMAND_WORD_MODE_CONTROL(20, ModeControl31, CommandTransmitStatusWord);
    VirtualBC_BuildExecuteTransferBuffer(&buffer, ModeCommandWithoutData, CW1.Raw, INVALID_COMMAND_WORD, NULL);
    if (VirtualBC_IsValidExecuteTransferBuffer(&buffer))
    {
        for (int i = 0; i < NUMBER_OF_ITERATIONS; ++i)
        {
            TIME_MEASURE_CALL(TimeResultCall);
            VIRTUAL1553B_STATUS status = VirtualBC_ExecuteTransfer(g_hTerminal, TransferLineA, &buffer);
            if (VIRTUAL1553B_SUCCESS(status))
            {
                WaitForSingleObject(g_hInterrupt, INFINITE);
                status = VirtualBC_GetTransferResult(g_hTerminal, &result);
                if (VIRTUAL1553B_SUCCESS(status) && (result.Error == VIRTUAL1553B_ERROR_SUCCESS))
                {
                    ++g_dwSuccessfully;
                }
            }
        }
    }

    _cprintf(
        "\n\nDelay: %lf\n"
        "Successfully: %lu\n\n",
        (g_dwCount ? g_dblDiffTime / g_dwCount : 0.0),
        g_dwSuccessfully);

    _cprintf(
        "Command word: %X\n"
        "Error: %X\n"
        "Status word: %X\n"
        "RT address: %hu\n"
        "Flags: %X\n\n",
        CW1.Raw, result.Error,
        result.SW1.Raw, result.SW1.RemoteTerminalAddress, result.SW1.Flags);

    return TRUE;
}


BOOL Format05(void)
{
    _cprintf("ModeCommandWithDataTransmit.\nPress any key to continue...");
    if (_getch() == 27)
    {
        return FALSE;
    }

    g_dblDiffTime = 0.0;
    g_dwCount = 0;
    g_dwSuccessfully = 0;

    COMMAND_WORD CW1 = { 0 };
    EXECUTE_TRANSFER_BUFFER buffer = { 0 };
    BC_TRANSFER_RESULT result = { 0 };

    CW1.Raw = BUILD_COMMAND_WORD_MODE_CONTROL(20, ModeControl31, CommandTransmitLastCommandWord);
    VirtualBC_BuildExecuteTransferBuffer(&buffer, ModeCommandWithDataTransmit, CW1.Raw, INVALID_COMMAND_WORD, NULL);
    if (VirtualBC_IsValidExecuteTransferBuffer(&buffer))
    {
        for (int i = 0; i < NUMBER_OF_ITERATIONS; ++i)
        {
            TIME_MEASURE_CALL(TimeResultCall);
            VIRTUAL1553B_STATUS status = VirtualBC_ExecuteTransfer(g_hTerminal, TransferLineA, &buffer);
            if (VIRTUAL1553B_SUCCESS(status))
            {
                WaitForSingleObject(g_hInterrupt, INFINITE);
                status = VirtualBC_GetTransferResult(g_hTerminal, &result);
                if (VIRTUAL1553B_SUCCESS(status) && (result.Error == VIRTUAL1553B_ERROR_SUCCESS))
                {
                    ++g_dwSuccessfully;
                }
            }
        }
    }

    _cprintf(
        "\n\nDelay: %lf\n"
        "Successfully: %lu\n\n",
        (g_dwCount ? g_dblDiffTime / g_dwCount : 0.0),
        g_dwSuccessfully);

    _cprintf(
        "Command word: %X\n"
        "Error: %X\n"
        "Status word: %X\n"
        "RT address: %hu\n"
        "Flags: %X\n"
        "Data word: %X\n\n",
        CW1.Raw, result.Error,
        result.SW1.Raw, result.SW1.RemoteTerminalAddress, result.SW1.Flags,
        result.Data[0]);

    return TRUE;
}


BOOL Format06(void)
{
    _cprintf("ModeCommandWithDataReceive.\nPress any key to continue...");
    if (_getch() == 27)
    {
        return FALSE;
    }

    g_dblDiffTime = 0.0;
    g_dwCount = 0;
    g_dwSuccessfully = 0;

    COMMAND_WORD CW1 = { 0 };
    EXECUTE_TRANSFER_BUFFER buffer = { 0 };
    BC_TRANSFER_RESULT result = { 0 };
    USHORT data = 0xDEAD;

    CW1.Raw = BUILD_COMMAND_WORD_MODE_CONTROL(20, ModeControl31, CommandSelectedTransmitterShutdown);
    VirtualBC_BuildExecuteTransferBuffer(&buffer, ModeCommandWithDataReceive, CW1.Raw, INVALID_COMMAND_WORD, &data);
    if (VirtualBC_IsValidExecuteTransferBuffer(&buffer))
    {
        for (int i = 0; i < NUMBER_OF_ITERATIONS; ++i)
        {
            TIME_MEASURE_CALL(TimeResultCall);
            VIRTUAL1553B_STATUS status = VirtualBC_ExecuteTransfer(g_hTerminal, TransferLineA, &buffer);
            if (VIRTUAL1553B_SUCCESS(status))
            {
                WaitForSingleObject(g_hInterrupt, INFINITE);
                status = VirtualBC_GetTransferResult(g_hTerminal, &result);
                if (VIRTUAL1553B_SUCCESS(status) && (result.Error == VIRTUAL1553B_ERROR_SUCCESS))
                {
                    ++g_dwSuccessfully;
                }
            }
        }
    }

    _cprintf(
        "\n\nDelay: %lf\n"
        "Successfully: %lu\n\n",
        (g_dwCount ? g_dblDiffTime / g_dwCount : 0.0),
        g_dwSuccessfully);

    _cprintf(
        "Command word: %X\n"
        "Error: %X\n"
        "Status word: %X\n"
        "RT address: %hu\n"
        "Flags: %X\n\n",
        CW1.Raw, result.Error,
        result.SW1.Raw, result.SW1.RemoteTerminalAddress, result.SW1.Flags);

    return TRUE;
}


BOOL Format07(void)
{
    _cprintf("TransferBCRTBroadcast.\nPress any key to continue...");
    if (_getch() == 27)
    {
        return FALSE;
    }

    g_dblDiffTime = 0.0;
    g_dwCount = 0;
    g_dwSuccessfully = 0;

    COMMAND_WORD CW1 = { 0 };
    USHORT data[DATA_WORD_COUNT_MAXIMUM] = {
         1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16,
        17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32
    };
    EXECUTE_TRANSFER_BUFFER buffer = { 0 };
    BC_TRANSFER_RESULT result = { 0 };

    CW1.Raw = BUILD_COMMAND_WORD_RECEIVE(BROADCAST_REMOTE_TERMINAL_ADDRESS, SUBADDRESS_MINIMUM, DATA_WORD_COUNT_MAXIMUM);
    VirtualBC_BuildExecuteTransferBuffer(&buffer, TransferBCRTBroadcast, CW1.Raw, INVALID_COMMAND_WORD, data);
    if (VirtualBC_IsValidExecuteTransferBuffer(&buffer))
    {
        for (int i = 0; i < NUMBER_OF_ITERATIONS; ++i)
        {
            TIME_MEASURE_CALL(TimeResultCall);
            VIRTUAL1553B_STATUS status = VirtualBC_ExecuteTransfer(g_hTerminal, TransferLineA, &buffer);
            if (VIRTUAL1553B_SUCCESS(status))
            {
                WaitForSingleObject(g_hInterrupt, INFINITE);
                status = VirtualBC_GetTransferResult(g_hTerminal, &result);
                if (VIRTUAL1553B_SUCCESS(status) && (result.Error == VIRTUAL1553B_ERROR_SUCCESS))
                {
                    ++g_dwSuccessfully;
                }
            }
        }
    }

    _cprintf(
        "\n\nDelay: %lf\n"
        "Successfully: %lu\n\n",
        (g_dwCount ? g_dblDiffTime / g_dwCount : 0.0),
        g_dwSuccessfully);

    _cprintf(
        "Command word: %X\n"
        "Error: %X\n\n",
        CW1.Raw, result.Error);

    return TRUE;
}


BOOL Format08(void)
{
    _cprintf("TransferRTRTBroadcast.\nPress any key to continue...");
    if (_getch() == 27)
    {
        return FALSE;
    }

    g_dblDiffTime = 0.0;
    g_dwCount = 0;
    g_dwSuccessfully = 0;

    COMMAND_WORD CW1 = { 0 };
    COMMAND_WORD CW2 = { 0 };
    EXECUTE_TRANSFER_BUFFER buffer = { 0 };
    BC_TRANSFER_RESULT result = { 0 };

    CW1.Raw = BUILD_COMMAND_WORD_RECEIVE(BROADCAST_REMOTE_TERMINAL_ADDRESS, SUBADDRESS_MINIMUM, DATA_WORD_COUNT_MAXIMUM);
    CW2.Raw = BUILD_COMMAND_WORD_TRANSMIT(10, SUBADDRESS_MINIMUM, DATA_WORD_COUNT_MAXIMUM);
    VirtualBC_BuildExecuteTransferBuffer(&buffer, TransferRTRTBroadcast, CW1.Raw, CW2.Raw, NULL);
    if (VirtualBC_IsValidExecuteTransferBuffer(&buffer))
    {
        for (int i = 0; i < NUMBER_OF_ITERATIONS; ++i)
        {
            TIME_MEASURE_CALL(TimeResultCall);
            VIRTUAL1553B_STATUS status = VirtualBC_ExecuteTransfer(g_hTerminal, TransferLineA, &buffer);
            if (VIRTUAL1553B_SUCCESS(status))
            {
                WaitForSingleObject(g_hInterrupt, INFINITE);
                status = VirtualBC_GetTransferResult(g_hTerminal, &result);
                if (VIRTUAL1553B_SUCCESS(status) && (result.Error == VIRTUAL1553B_ERROR_SUCCESS))
                {
                    ++g_dwSuccessfully;
                }
            }
        }
    }

    _cprintf(
        "\n\nDelay: %lf\n"
        "Successfully: %lu\n\n",
        (g_dwCount ? g_dblDiffTime / g_dwCount : 0.0),
        g_dwSuccessfully);

    CHAR szBuffer[256] = { 0 };
    int nBufLen = 0;
    WordDigitsToCharactersHexArray(result.Data, DATA_WORD_COUNT_MAXIMUM, szBuffer, &nBufLen);

    _cprintf(
        "Command word 1: %X\n"
        "Command word 2: %X\n"
        "Error: %X\n"
        "Status word 1: %X\n"
        "RT address 1: %hu\n"
        "Flags 1: %X\n"
        "Data words: %hs\n",
        CW1.Raw, CW2.Raw, result.Error,
        result.SW1.Raw, result.SW1.RemoteTerminalAddress, result.SW1.Flags,
        szBuffer);

    return TRUE;
}


BOOL Format09(void)
{
    _cprintf("ModeCommandWithoutDataBroadcast.\nPress any key to continue...");
    if (_getch() == 27)
    {
        return FALSE;
    }

    g_dblDiffTime = 0.0;
    g_dwCount = 0;
    g_dwSuccessfully = 0;

    COMMAND_WORD CW1 = { 0 };
    EXECUTE_TRANSFER_BUFFER buffer = { 0 };
    BC_TRANSFER_RESULT result = { 0 };

    CW1.Raw = BUILD_COMMAND_WORD_MODE_CONTROL(BROADCAST_REMOTE_TERMINAL_ADDRESS, ModeControl0, CommandSynchronize);
    VirtualBC_BuildExecuteTransferBuffer(&buffer, ModeCommandWithoutDataBroadcast, CW1.Raw, INVALID_COMMAND_WORD, NULL);
    if (VirtualBC_IsValidExecuteTransferBuffer(&buffer))
    {
        for (int i = 0; i < NUMBER_OF_ITERATIONS; ++i)
        {
            TIME_MEASURE_CALL(TimeResultCall);
            VIRTUAL1553B_STATUS status = VirtualBC_ExecuteTransfer(g_hTerminal, TransferLineA, &buffer);
            if (VIRTUAL1553B_SUCCESS(status))
            {
                WaitForSingleObject(g_hInterrupt, INFINITE);
                status = VirtualBC_GetTransferResult(g_hTerminal, &result);
                if (VIRTUAL1553B_SUCCESS(status) && (result.Error == VIRTUAL1553B_ERROR_SUCCESS))
                {
                    ++g_dwSuccessfully;
                }
            }
        }
    }

    _cprintf(
        "\n\nDelay: %lf\n"
        "Successfully: %lu\n\n",
        (g_dwCount ? g_dblDiffTime / g_dwCount : 0.0),
        g_dwSuccessfully);

    _cprintf(
        "Command word: %X\n"
        "Error: %X\n\n",
        CW1.Raw, result.Error);

    return TRUE;
}


BOOL Format10(void)
{
    _cprintf("ModeCommandWithDataReceiveBroadcast.\nPress any key to continue...");
    if (_getch() == 27)
    {
        return FALSE;
    }

    g_dblDiffTime = 0.0;
    g_dwCount = 0;
    g_dwSuccessfully = 0;

    COMMAND_WORD CW1 = { 0 };
    EXECUTE_TRANSFER_BUFFER buffer = { 0 };
    BC_TRANSFER_RESULT result = { 0 };
    USHORT data = 0xBEAF;

    CW1.Raw = BUILD_COMMAND_WORD_MODE_CONTROL(BROADCAST_REMOTE_TERMINAL_ADDRESS, ModeControl0, CommandSynchronizeWithDataWord);
    VirtualBC_BuildExecuteTransferBuffer(&buffer, ModeCommandWithDataReceiveBroadcast, CW1.Raw, INVALID_COMMAND_WORD, &data);
    if (VirtualBC_IsValidExecuteTransferBuffer(&buffer))
    {
        for (int i = 0; i < NUMBER_OF_ITERATIONS; ++i)
        {
            TIME_MEASURE_CALL(TimeResultCall);
            VIRTUAL1553B_STATUS status = VirtualBC_ExecuteTransfer(g_hTerminal, TransferLineA, &buffer);
            if (VIRTUAL1553B_SUCCESS(status))
            {
                WaitForSingleObject(g_hInterrupt, INFINITE);
                status = VirtualBC_GetTransferResult(g_hTerminal, &result);
                if (VIRTUAL1553B_SUCCESS(status) && (result.Error == VIRTUAL1553B_ERROR_SUCCESS))
                {
                    ++g_dwSuccessfully;
                }
            }
        }
    }

    _cprintf(
        "\n\nDelay: %lf\n"
        "Successfully: %lu\n\n",
        (g_dwCount ? g_dblDiffTime / g_dwCount : 0.0),
        g_dwSuccessfully);

    _cprintf(
        "Command word: %X\n"
        "Error: %X\n\n",
        CW1.Raw, result.Error);

    return TRUE;
}


void CALLBACK TimeResultCall(double Result)
{
    ++g_dwCount;
    g_dblDiffTime += Result;
}
