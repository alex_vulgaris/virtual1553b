
#pragma once

#include "stdafx.h"
#include "..\VirtualTerminalDriver\VirtualTerminalDriver.h"
#include "..\VirtualTerminalDriver\Utils.h"

#define USE_TIME_MEASURE
#include "..\VirtualTerminalDriver\TimeMeasure.h"


#define NUMBER_OF_ITERATIONS (1000)


BOOL Format01(void); // TransferBCRT
BOOL Format02(void); // TransferRTBC
BOOL Format03(void); // TransferRTRT
BOOL Format04(void); // ModeCommandWithoutData
BOOL Format05(void); // ModeCommandWithDataTransmit
BOOL Format06(void); // ModeCommandWithDataReceive
BOOL Format07(void); // TransferBCRTBroadcast
BOOL Format08(void); // TransferRTRTBroadcast
BOOL Format09(void); // ModeCommandWithoutDataBroadcast
BOOL Format10(void); // ModeCommandWithDataReceiveBroadcast

void CALLBACK TimeResultCall(double Result);
