// VirtualTerminalDriver.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "VirtualTerminalDriver.h"
#include "..\Virtual1553B\IoControlCode.h"
#include "SynqLocker.h"


HANDLE hDriverVirtual1553B;
HANDLE hSynqVirtual1553B;
LONG   nReferenceCount;


VIRTUAL_TERMINAL_DRIVER_API
BOOL
__stdcall
Virtual1553B_DriverOpen(
    VOID
    )
{
    SYNQLOCKER(hSynqVirtual1553B);

    if (hDriverVirtual1553B)
    {
        InterlockedIncrement(&nReferenceCount);
        return TRUE;
    }

    HANDLE hDriver = CreateFile(
        TEXT("\\\\.\\Virtual1553B0"),
        GENERIC_READ | GENERIC_WRITE,
        0,
        NULL,
        OPEN_EXISTING,
        FILE_ATTRIBUTE_NORMAL,
        NULL);

    if (hDriver == INVALID_HANDLE_VALUE)
    {
        return FALSE;
    }

    hDriverVirtual1553B = hDriver;
    InterlockedIncrement(&nReferenceCount);
    return TRUE;
}


VIRTUAL_TERMINAL_DRIVER_API
BOOL
__stdcall
Virtual1553B_DriverClose(
    VOID
    )
{
    SYNQLOCKER(hSynqVirtual1553B);

    if (hDriverVirtual1553B)
    {
        if (!InterlockedDecrement(&nReferenceCount))
        {
            HANDLE hDriver = InterlockedExchangePointer(&hDriverVirtual1553B, NULL);
            CloseHandle(hDriver);
        }
        return TRUE;
    }
    return FALSE;
}


#define VIRTUAL1553B_VERIFY_DRIVER_IS_OPEN(hDriverVirtual1553B) \
{                                                               \
    if (!(hDriverVirtual1553B))                                 \
    {                                                           \
        return VIRTUAL1553B_STATUS_DRIVER_HANDLE_IS_NOT_OPENED; \
    }                                                           \
}


VIRTUAL_TERMINAL_DRIVER_API
VIRTUAL1553B_STATUS
__stdcall
Virtual1553B_AllocTerminal(
    __out PTERMINAL_HANDLE pTerminalHandle
    )
{
    SYNQLOCKER(hSynqVirtual1553B);

    *pTerminalHandle = INVALID_TERMINAL_HANDLE;

    VIRTUAL1553B_VERIFY_DRIVER_IS_OPEN(hDriverVirtual1553B);

    OUTPUT_ALLOC_TERMINAL outBuffer = { 0 };

    DWORD cbBytesReturned = 0;
    BOOL bResult = DeviceIoControl(
        hDriverVirtual1553B,
        IOCTL_VIRTUAL1553B_ALLOC_TERMINAL,
        NULL, 0,
        &outBuffer, sizeof(outBuffer),
        &cbBytesReturned,
        NULL);

    if (bResult && (cbBytesReturned == sizeof(outBuffer)))
    {
        if (VIRTUAL1553B_SUCCESS(outBuffer.Status))
        {
            *pTerminalHandle = outBuffer.TerminalHandle;
        }

        return outBuffer.Status;
    }

    return VIRTUAL1553B_STATUS_DEVICE_IO_CONTROL_FAILED;
}


#define VIRTUAL1553B_VERIFY_TERMINAL_HANDLE(TerminalHandle) \
{                                                           \
    if ((TerminalHandle) == INVALID_TERMINAL_HANDLE)        \
    {                                                       \
        return VIRTUAL1553B_STATUS_INVALID_TERMINAL_HANDLE; \
    }                                                       \
}


VIRTUAL_TERMINAL_DRIVER_API
VIRTUAL1553B_STATUS
__stdcall
Virtual1553B_FreeTerminal(
    __in TERMINAL_HANDLE TerminalHandle
    )
{
    SYNQLOCKER(hSynqVirtual1553B);

    VIRTUAL1553B_VERIFY_DRIVER_IS_OPEN(hDriverVirtual1553B);
    VIRTUAL1553B_VERIFY_TERMINAL_HANDLE(TerminalHandle);

    INPUT_FREE_TERMINAL inBuffer = { 0 };
    inBuffer.TerminalHandle = TerminalHandle;

    OUTPUT_FREE_TERMINAL outBuffer = { 0 };

    DWORD cbBytesReturned = 0;    
    BOOL bResult = DeviceIoControl(
        hDriverVirtual1553B,
        IOCTL_VIRTUAL1553B_FREE_TERMINAL,
        &inBuffer, sizeof(inBuffer),
        &outBuffer, sizeof(outBuffer),
        &cbBytesReturned,
        NULL);

    if (bResult && (cbBytesReturned == sizeof(outBuffer)))
    {
        return outBuffer.Status;
    }

    return VIRTUAL1553B_STATUS_DEVICE_IO_CONTROL_FAILED;
}


VIRTUAL_TERMINAL_DRIVER_API
VIRTUAL1553B_STATUS
__stdcall
Virtual1553B_InstallEvent(
    __in TERMINAL_HANDLE TerminalHandle,
    __in HANDLE Event
    )
{
    SYNQLOCKER(hSynqVirtual1553B);

    VIRTUAL1553B_VERIFY_DRIVER_IS_OPEN(hDriverVirtual1553B);
    VIRTUAL1553B_VERIFY_TERMINAL_HANDLE(TerminalHandle);

    INPUT_INSTALL_EVENT inBuffer = { 0 };
    inBuffer.TerminalHandle = TerminalHandle;
    inBuffer.Event = Event;

    OUTPUT_INSTALL_EVENT outBuffer = { 0 };

    DWORD cbBytesReturned = 0;
    BOOL bResult = DeviceIoControl(
        hDriverVirtual1553B,
        IOCTL_VIRTUAL1553B_INSTALL_EVENT,
        &inBuffer, sizeof(inBuffer),
        &outBuffer, sizeof(outBuffer),
        &cbBytesReturned,
        NULL);

    if (bResult && (cbBytesReturned == sizeof(outBuffer)))
    {
        return outBuffer.Status;
    }

    return VIRTUAL1553B_STATUS_DEVICE_IO_CONTROL_FAILED;
}


VIRTUAL_TERMINAL_DRIVER_API
VIRTUAL1553B_STATUS
__stdcall
Virtual1553B_RemoveEvent(
    __in TERMINAL_HANDLE TerminalHandle
    )
{
    SYNQLOCKER(hSynqVirtual1553B);

    VIRTUAL1553B_VERIFY_DRIVER_IS_OPEN(hDriverVirtual1553B);
    VIRTUAL1553B_VERIFY_TERMINAL_HANDLE(TerminalHandle);

    INPUT_REMOVE_EVENT inBuffer = { 0 };
    inBuffer.TerminalHandle = TerminalHandle;

    OUTPUT_REMOVE_EVENT outBuffer = { 0 };

    DWORD cbBytesReturned = 0;
    BOOL bResult = DeviceIoControl(
        hDriverVirtual1553B,
        IOCTL_VIRTUAL1553B_REMOVE_EVENT,
        &inBuffer, sizeof(inBuffer),
        &outBuffer, sizeof(outBuffer),
        &cbBytesReturned,
        NULL);

    if (bResult && (cbBytesReturned == sizeof(outBuffer)))
    {
        return outBuffer.Status;
    }

    return VIRTUAL1553B_STATUS_DEVICE_IO_CONTROL_FAILED;
}


VIRTUAL_TERMINAL_DRIVER_API
VIRTUAL1553B_STATUS
__stdcall
Virtual1553B_GetTerminalMode(
    __in TERMINAL_HANDLE TerminalHandle,
    __out PUSHORT pTerminalMode
    )
{
    SYNQLOCKER(hSynqVirtual1553B);

    *pTerminalMode = UndefinedMode;

    VIRTUAL1553B_VERIFY_DRIVER_IS_OPEN(hDriverVirtual1553B);
    VIRTUAL1553B_VERIFY_TERMINAL_HANDLE(TerminalHandle);

    INPUT_GET_TERMINAL_MODE inBuffer = { 0 };
    inBuffer.TerminalHandle = TerminalHandle;

    OUTPUT_GET_TERMINAL_MODE outBuffer = { 0 };

    DWORD cbBytesReturned = 0;
    BOOL bResult = DeviceIoControl(
        hDriverVirtual1553B,
        IOCTL_VIRTUAL1553B_GET_TERMINAL_MODE,
        &inBuffer, sizeof(inBuffer),
        &outBuffer, sizeof(outBuffer),
        &cbBytesReturned,
        NULL);

    if (bResult && (cbBytesReturned == sizeof(outBuffer)))
    {
        if (VIRTUAL1553B_SUCCESS(outBuffer.Status))
        {
            *pTerminalMode = outBuffer.TerminalMode;
        }

        return outBuffer.Status;
    }

    return VIRTUAL1553B_STATUS_DEVICE_IO_CONTROL_FAILED;
}


VIRTUAL_TERMINAL_DRIVER_API
VIRTUAL1553B_STATUS
__stdcall
Virtual1553B_StartBusController(
    __in TERMINAL_HANDLE TerminalHandle
    )
{
    SYNQLOCKER(hSynqVirtual1553B);

    VIRTUAL1553B_VERIFY_DRIVER_IS_OPEN(hDriverVirtual1553B);
    VIRTUAL1553B_VERIFY_TERMINAL_HANDLE(TerminalHandle);

    INPUT_START_BUS_CONTROLLER inBuffer = { 0 };
    inBuffer.TerminalHandle = TerminalHandle;

    OUTPUT_START_BUS_CONTROLLER outBuffer = { 0 };

    DWORD cbBytesReturned = 0;
    BOOL bResult = DeviceIoControl(
        hDriverVirtual1553B,
        IOCTL_VIRTUAL1553B_START_BUS_CONTROLLER,
        &inBuffer, sizeof(inBuffer),
        &outBuffer, sizeof(outBuffer),
        &cbBytesReturned,
        NULL);

    if (bResult && (cbBytesReturned == sizeof(outBuffer)))
    {
        return outBuffer.Status;
    }

    return VIRTUAL1553B_STATUS_DEVICE_IO_CONTROL_FAILED;
}


VIRTUAL_TERMINAL_DRIVER_API
VIRTUAL1553B_STATUS
__stdcall
Virtual1553B_StartRemoteTerminal(
    __in TERMINAL_HANDLE TerminalHandle,
    __in USHORT Address
    )
{
    SYNQLOCKER(hSynqVirtual1553B);

    VIRTUAL1553B_VERIFY_DRIVER_IS_OPEN(hDriverVirtual1553B);
    VIRTUAL1553B_VERIFY_TERMINAL_HANDLE(TerminalHandle);

    INPUT_START_REMOTE_TERMINAL inBuffer = { 0 };
    inBuffer.TerminalHandle = TerminalHandle;
    inBuffer.Address = Address;

    OUTPUT_START_REMOTE_TERMINAL outBuffer = { 0 };

    DWORD cbBytesReturned = 0;
    BOOL bResult = DeviceIoControl(
        hDriverVirtual1553B,
        IOCTL_VIRTUAL1553B_START_REMOTE_TERMINAL,
        &inBuffer, sizeof(inBuffer),
        &outBuffer, sizeof(outBuffer),
        &cbBytesReturned,
        NULL);

    if (bResult && (cbBytesReturned == sizeof(outBuffer)))
    {
        return outBuffer.Status;
    }

    return VIRTUAL1553B_STATUS_DEVICE_IO_CONTROL_FAILED;
}


VIRTUAL_TERMINAL_DRIVER_API
VIRTUAL1553B_STATUS
__stdcall
Virtual1553B_StartBusMonitor(
    __in TERMINAL_HANDLE TerminalHandle
    )
{
    SYNQLOCKER(hSynqVirtual1553B);

    VIRTUAL1553B_VERIFY_DRIVER_IS_OPEN(hDriverVirtual1553B);
    VIRTUAL1553B_VERIFY_TERMINAL_HANDLE(TerminalHandle);

    INPUT_START_BUS_MONITOR inBuffer = { 0 };
    inBuffer.TerminalHandle = TerminalHandle;

    OUTPUT_START_BUS_MONITOR outBuffer = { 0 };

    DWORD cbBytesReturned = 0;
    BOOL bResult = DeviceIoControl(
        hDriverVirtual1553B,
        IOCTL_VIRTUAL1553B_START_BUS_MONITOR,
        &inBuffer, sizeof(inBuffer),
        &outBuffer, sizeof(outBuffer),
        &cbBytesReturned,
        NULL);

    if (bResult && (cbBytesReturned == sizeof(outBuffer)))
    {
        return outBuffer.Status;
    }

    return VIRTUAL1553B_STATUS_DEVICE_IO_CONTROL_FAILED;
}


VIRTUAL_TERMINAL_DRIVER_API
VIRTUAL1553B_STATUS
__stdcall
Virtual1553B_StopTerminal(
    __in TERMINAL_HANDLE TerminalHandle
    )
{
    SYNQLOCKER(hSynqVirtual1553B);

    VIRTUAL1553B_VERIFY_DRIVER_IS_OPEN(hDriverVirtual1553B);
    VIRTUAL1553B_VERIFY_TERMINAL_HANDLE(TerminalHandle);

    INPUT_STOP_TERMINAL inBuffer = { 0 };
    inBuffer.TerminalHandle = TerminalHandle;

    OUTPUT_STOP_TERMINAL outBuffer = { 0 };

    DWORD cbBytesReturned = 0;
    BOOL bResult = DeviceIoControl(
        hDriverVirtual1553B,
        IOCTL_VIRTUAL1553B_STOP_TERMINAL,
        &inBuffer, sizeof(inBuffer),
        &outBuffer, sizeof(outBuffer),
        &cbBytesReturned,
        NULL);

    if (bResult && (cbBytesReturned == sizeof(outBuffer)))
    {
        return outBuffer.Status;
    }

    return VIRTUAL1553B_STATUS_DEVICE_IO_CONTROL_FAILED;
}


VIRTUAL_TERMINAL_DRIVER_API
VOID
__stdcall
VirtualBC_BuildExecuteTransferBuffer(
    __out PEXECUTE_TRANSFER_BUFFER pExecuteTransferBuffer,
    __in MESSAGE_FORMAT Format,
    __in USHORT CW1,
    __in USHORT CW2,
    __in_opt CONST USHORT *pData
    )
{
    pExecuteTransferBuffer->Format = Format;

    switch (Format)
    {
    case TransferBCRT:
    case TransferBCRTBroadcast:
        pExecuteTransferBuffer->CW1.Raw = CW1;
        pExecuteTransferBuffer->CW2.Raw = INVALID_COMMAND_WORD;
        RtlCopyMemory(pExecuteTransferBuffer->Data, pData, GET_DATA_WORD_COUNT(CW1) * sizeof(USHORT));
        break;

    case TransferRTBC:
        pExecuteTransferBuffer->CW1.Raw = CW1;
        pExecuteTransferBuffer->CW2.Raw = INVALID_COMMAND_WORD;
        break;

    case TransferRTRT:
    case TransferRTRTBroadcast:
        pExecuteTransferBuffer->CW1.Raw = CW1;
        pExecuteTransferBuffer->CW2.Raw = CW2;
        break;

    case ModeCommandWithoutData:
    case ModeCommandWithoutDataBroadcast:
        pExecuteTransferBuffer->CW1.Raw = CW1;
        pExecuteTransferBuffer->CW2.Raw = INVALID_COMMAND_WORD;
        break;

    case ModeCommandWithDataReceive:
    case ModeCommandWithDataReceiveBroadcast:
        pExecuteTransferBuffer->CW1.Raw = CW1;
        pExecuteTransferBuffer->CW2.Raw = INVALID_COMMAND_WORD;
        pExecuteTransferBuffer->Data[0] = pData[0];
        break;

    case ModeCommandWithDataTransmit:
        pExecuteTransferBuffer->CW1.Raw = CW1;
        pExecuteTransferBuffer->CW2.Raw = INVALID_COMMAND_WORD;
        break;

    default:
        break;
    }
}


VIRTUAL_TERMINAL_DRIVER_API
BOOL
__stdcall
VirtualBC_IsValidExecuteTransferBuffer(
    __in PEXECUTE_TRANSFER_BUFFER pExecuteTransferBuffer
    )
{
    USHORT CW1 = pExecuteTransferBuffer->CW1.Raw;
    USHORT CW2 = pExecuteTransferBuffer->CW2.Raw;

    switch (pExecuteTransferBuffer->Format)
    {
    case TransferBCRT:
        if (VALIDATE_TRANSFER_BC_RT(CW1))
        {
            return TRUE;
        }
        break;

    case TransferBCRTBroadcast:
        if (VALIDATE_TRANSFER_BC_RT_BROADCAST(CW1))
        {
            return TRUE;
        }
        break;

    case TransferRTBC:
        if (VALIDATE_TRANSFER_RT_BC(CW1))
        {
            return TRUE;
        }
        break;

    case TransferRTRT:
        if (VALIDATE_TRANSFER_RT_RT(CW1, CW2))
        {
            return TRUE;
        }
        break;

    case TransferRTRTBroadcast:
        if (VALIDATE_TRANSFER_RT_RT_BROADCAST(CW1, CW2))
        {
            return TRUE;
        }
        break;

    case ModeCommandWithoutData:
        if (VALIDATE_MODE_COMMAND_WITHOUT_DATA(CW1))
        {
            return TRUE;
        }
        break;

    case ModeCommandWithoutDataBroadcast:
        if (VALIDATE_MODE_COMMAND_WITHOUT_DATA_BROADCAST(CW1))
        {
            return TRUE;
        }
        break;

    case ModeCommandWithDataReceive:
        if (VALIDATE_MODE_COMMAND_WITH_DATA_RECEIVE(CW1))
        {
            return TRUE;
        }
        break;

    case ModeCommandWithDataReceiveBroadcast:
        if (VALIDATE_MODE_COMMAND_WITH_DATA_RECEIVE_BROADCAST(CW1))
        {
            return TRUE;
        }
        break;

    case ModeCommandWithDataTransmit:
        if (VALIDATE_MODE_COMMAND_WITH_DATA_TRANSMIT(CW1))
        {
            return TRUE;
        }
        break;

    default:
        break;
    }

    return FALSE;
}


VIRTUAL_TERMINAL_DRIVER_API
VIRTUAL1553B_STATUS
__stdcall
VirtualBC_ExecuteTransfer(
    __in TERMINAL_HANDLE TerminalHandle,
    __in TRANSFER_LINE_NUMBER TransferLine,
    __in PEXECUTE_TRANSFER_BUFFER pExecuteTransferBuffer
    )
{
    SYNQLOCKER(hSynqVirtual1553B);

    VIRTUAL1553B_VERIFY_DRIVER_IS_OPEN(hDriverVirtual1553B);
    VIRTUAL1553B_VERIFY_TERMINAL_HANDLE(TerminalHandle);

    INPUT_BC_EXECUTE_TRANSFER inBuffer = { 0 };
    inBuffer.TerminalHandle = TerminalHandle;
    inBuffer.TransferLine = TransferLine;
    inBuffer.ExecuteTransferBuffer = *pExecuteTransferBuffer;

    OUTPUT_BC_EXECUTE_TRANSFER outBuffer = { 0 };

    DWORD cbBytesReturned = 0;
    BOOL bResult = DeviceIoControl(
        hDriverVirtual1553B,
        IOCTL_VIRTUAL1553B_BC_EXECUTE_TRANSFER,
        &inBuffer, sizeof(inBuffer),
        &outBuffer, sizeof(outBuffer),
        &cbBytesReturned,
        NULL);

    if (bResult && (cbBytesReturned == sizeof(outBuffer)))
    {
        return outBuffer.Status;
    }

    return VIRTUAL1553B_STATUS_DEVICE_IO_CONTROL_FAILED;
}


VIRTUAL_TERMINAL_DRIVER_API
VIRTUAL1553B_STATUS
__stdcall
VirtualBC_GetTransferResult(
    __in TERMINAL_HANDLE TerminalHandle,
    __out PBC_TRANSFER_RESULT pTransferResult
    )
{
    SYNQLOCKER(hSynqVirtual1553B);

    VIRTUAL1553B_VERIFY_DRIVER_IS_OPEN(hDriverVirtual1553B);
    VIRTUAL1553B_VERIFY_TERMINAL_HANDLE(TerminalHandle);

    INPUT_BC_GET_TRANSFER_RESULT inBuffer = { 0 };
    inBuffer.TerminalHandle = TerminalHandle;

    OUTPUT_BC_GET_TRANSFER_RESULT outBuffer = { 0 };

    DWORD cbBytesReturned = 0;
    BOOL bResult = DeviceIoControl(
        hDriverVirtual1553B,
        IOCTL_VIRTUAL1553B_BC_GET_TRANSFER_RESULT,
        &inBuffer, sizeof(inBuffer),
        &outBuffer, sizeof(outBuffer),
        &cbBytesReturned,
        NULL);

    if (bResult && (cbBytesReturned == sizeof(outBuffer)))
    {
        if (VIRTUAL1553B_SUCCESS(outBuffer.Status))
        {
            *pTransferResult = outBuffer.TransferResult;
        }

        return outBuffer.Status;
    }

    return VIRTUAL1553B_STATUS_DEVICE_IO_CONTROL_FAILED;
}


VIRTUAL_TERMINAL_DRIVER_API
VIRTUAL1553B_STATUS
__stdcall
VirtualRT_SetAddress(
    __in TERMINAL_HANDLE TerminalHandle,
    __in USHORT Address
    )
{
    SYNQLOCKER(hSynqVirtual1553B);

    VIRTUAL1553B_VERIFY_DRIVER_IS_OPEN(hDriverVirtual1553B);
    VIRTUAL1553B_VERIFY_TERMINAL_HANDLE(TerminalHandle);

    INPUT_RT_SET_ADDRESS inBuffer = { 0 };
    inBuffer.TerminalHandle = TerminalHandle;
    inBuffer.Address = Address;

    OUTPUT_RT_SET_ADDRESS outBuffer = { 0 };

    DWORD cbBytesReturned = 0;
    BOOL bResult = DeviceIoControl(
        hDriverVirtual1553B,
        IOCTL_VIRTUAL1553B_RT_SET_ADDRESS,
        &inBuffer, sizeof(inBuffer),
        &outBuffer, sizeof(outBuffer),
        &cbBytesReturned,
        NULL);

    if (bResult && (cbBytesReturned == sizeof(outBuffer)))
    {
        return outBuffer.Status;
    }

    return VIRTUAL1553B_STATUS_DEVICE_IO_CONTROL_FAILED;
}


VIRTUAL_TERMINAL_DRIVER_API
VIRTUAL1553B_STATUS
__stdcall
VirtualRT_GetAddress(
    __in TERMINAL_HANDLE TerminalHandle,
    __out PUSHORT pAddress
    )
{
    SYNQLOCKER(hSynqVirtual1553B);

    *pAddress = INVALID_REMOTE_TERMINAL_ADDRESS;

    VIRTUAL1553B_VERIFY_DRIVER_IS_OPEN(hDriverVirtual1553B);
    VIRTUAL1553B_VERIFY_TERMINAL_HANDLE(TerminalHandle);

    INPUT_RT_GET_ADDRESS inBuffer = { 0 };
    inBuffer.TerminalHandle = TerminalHandle;

    OUTPUT_RT_GET_ADDRESS outBuffer = { 0 };

    DWORD cbBytesReturned = 0;
    BOOL bResult = DeviceIoControl(
        hDriverVirtual1553B,
        IOCTL_VIRTUAL1553B_RT_GET_ADDRESS,
        &inBuffer, sizeof(inBuffer),
        &outBuffer, sizeof(outBuffer),
        &cbBytesReturned,
        NULL);

    if (bResult && (cbBytesReturned == sizeof(outBuffer)))
    {
        if (VIRTUAL1553B_SUCCESS(outBuffer.Status))
        {
            *pAddress = outBuffer.Address;
        }

        return outBuffer.Status;
    }

    return VIRTUAL1553B_STATUS_DEVICE_IO_CONTROL_FAILED;
}


VIRTUAL_TERMINAL_DRIVER_API
VIRTUAL1553B_STATUS
__stdcall
VirtualRT_WriteMemory(
    __in TERMINAL_HANDLE TerminalHandle,
    __in TRANSFER_DIRECTION Direction,
    __in USHORT Subaddress,
    __in_ecount(WordCountToWrite) CONST USHORT *pBuffer,
    __in USHORT WordCountToWrite,
    __out_opt PUSHORT pWordCountWritten
    )
{
    SYNQLOCKER(hSynqVirtual1553B);

    if (pWordCountWritten)
    {
        *pWordCountWritten = 0;
    }

    VIRTUAL1553B_VERIFY_DRIVER_IS_OPEN(hDriverVirtual1553B);
    VIRTUAL1553B_VERIFY_TERMINAL_HANDLE(TerminalHandle);

    INPUT_RT_WRITE_MEMORY inBuffer = { 0 };
    inBuffer.TerminalHandle = TerminalHandle;
    inBuffer.Direction = Direction;
    inBuffer.Subaddress = Subaddress;
    inBuffer.WordCountToWrite = (WordCountToWrite > SUBADDRESS_LENGTH) ? SUBADDRESS_LENGTH : WordCountToWrite;
    RtlCopyMemory(inBuffer.Buffer, pBuffer, inBuffer.WordCountToWrite * sizeof(USHORT));

    OUTPUT_RT_WRITE_MEMORY outBuffer = { 0 };

    DWORD cbBytesReturned = 0;
    BOOL bResult = DeviceIoControl(
        hDriverVirtual1553B,
        IOCTL_VIRTUAL1553B_RT_WRITE_MEMORY,
        &inBuffer, sizeof(inBuffer),
        &outBuffer, sizeof(outBuffer),
        &cbBytesReturned,
        NULL);

    if (bResult && (cbBytesReturned == sizeof(outBuffer)))
    {
        if (VIRTUAL1553B_SUCCESS(outBuffer.Status))
        {
            if (pWordCountWritten)
            {
                *pWordCountWritten = outBuffer.WordCountWritten;
            }
        }

        return outBuffer.Status;
    }

    return VIRTUAL1553B_STATUS_DEVICE_IO_CONTROL_FAILED;
}


VIRTUAL_TERMINAL_DRIVER_API
VIRTUAL1553B_STATUS
__stdcall
VirtualRT_ReadMemory(
    __in TERMINAL_HANDLE TerminalHandle,
    __in TRANSFER_DIRECTION Direction,
    __in USHORT Subaddress,
    __out_ecount_part(WordCountToRead, *pWordCountRead) PUSHORT pBuffer,
    __in USHORT WordCountToRead,
    __out_opt PUSHORT pWordCountRead
    )
{
    SYNQLOCKER(hSynqVirtual1553B);

    if (pWordCountRead)
    {
        *pWordCountRead = 0;
    }

    VIRTUAL1553B_VERIFY_DRIVER_IS_OPEN(hDriverVirtual1553B);
    VIRTUAL1553B_VERIFY_TERMINAL_HANDLE(TerminalHandle);

    INPUT_RT_READ_MEMORY inBuffer = { 0 };
    inBuffer.TerminalHandle = TerminalHandle;
    inBuffer.Direction = Direction;
    inBuffer.Subaddress = Subaddress;
    inBuffer.WordCountToRead = (WordCountToRead > SUBADDRESS_LENGTH) ? SUBADDRESS_LENGTH : WordCountToRead;

    OUTPUT_RT_READ_MEMORY outBuffer = { 0 };

    DWORD cbBytesReturned = 0;
    BOOL bResult = DeviceIoControl(
        hDriverVirtual1553B,
        IOCTL_VIRTUAL1553B_RT_READ_MEMORY,
        &inBuffer, sizeof(inBuffer),
        &outBuffer, sizeof(outBuffer),
        &cbBytesReturned,
        NULL);

    if (bResult && (cbBytesReturned == sizeof(outBuffer)))
    {
        if (VIRTUAL1553B_SUCCESS(outBuffer.Status))
        {
            RtlCopyMemory(pBuffer, outBuffer.Buffer, outBuffer.WordCountRead * sizeof(USHORT));

            if (pWordCountRead)
            {
                *pWordCountRead = outBuffer.WordCountRead;
            }
        }

        return outBuffer.Status;
    }

    return VIRTUAL1553B_STATUS_DEVICE_IO_CONTROL_FAILED;
}


VIRTUAL_TERMINAL_DRIVER_API
VIRTUAL1553B_STATUS
__stdcall
VirtualRT_GetTransferResult(
    __in TERMINAL_HANDLE TerminalHandle,
    __out PRT_TRANSFER_RESULT pTransferResult
    )
{
    SYNQLOCKER(hSynqVirtual1553B);

    VIRTUAL1553B_VERIFY_DRIVER_IS_OPEN(hDriverVirtual1553B);
    VIRTUAL1553B_VERIFY_TERMINAL_HANDLE(TerminalHandle);

    INPUT_RT_GET_TRANSFER_RESULT inBuffer = { 0 };
    inBuffer.TerminalHandle = TerminalHandle;

    OUTPUT_RT_GET_TRANSFER_RESULT outBuffer = { 0 };

    DWORD cbBytesReturned = 0;
    BOOL bResult = DeviceIoControl(
        hDriverVirtual1553B,
        IOCTL_VIRTUAL1553B_RT_GET_TRANSFER_RESULT,
        &inBuffer, sizeof(inBuffer),
        &outBuffer, sizeof(outBuffer),
        &cbBytesReturned,
        NULL);

    if (bResult && (cbBytesReturned == sizeof(outBuffer)))
    {
        if (VIRTUAL1553B_SUCCESS(outBuffer.Status))
        {
            *pTransferResult = outBuffer.TransferResult;
        }

        return outBuffer.Status;
    }

    return VIRTUAL1553B_STATUS_DEVICE_IO_CONTROL_FAILED;
}


VIRTUAL_TERMINAL_DRIVER_API
VIRTUAL1553B_STATUS
__stdcall
VirtualBM_GetCurrentBase(
    __in TERMINAL_HANDLE TerminalHandle,
    __out PUSHORT pCurrentBase
    )
{
    SYNQLOCKER(hSynqVirtual1553B);

    *pCurrentBase = (USHORT)(-1);

    VIRTUAL1553B_VERIFY_DRIVER_IS_OPEN(hDriverVirtual1553B);
    VIRTUAL1553B_VERIFY_TERMINAL_HANDLE(TerminalHandle);

    INPUT_BM_GET_CURRENT_BASE inBuffer = { 0 };
    inBuffer.TerminalHandle = TerminalHandle;

    OUTPUT_BM_GET_CURRENT_BASE outBuffer = { 0 };

    DWORD cbBytesReturned = 0;
    BOOL bResult = DeviceIoControl(
        hDriverVirtual1553B,
        IOCTL_VIRTUAL1553B_BM_GET_CURRENT_BASE,
        &inBuffer, sizeof(inBuffer),
        &outBuffer, sizeof(outBuffer),
        &cbBytesReturned,
        NULL);

    if (bResult && (cbBytesReturned == sizeof(outBuffer)))
    {
        if (VIRTUAL1553B_SUCCESS(outBuffer.Status))
        {
            *pCurrentBase = outBuffer.CurrentBase;
        }

        return outBuffer.Status;
    }

    return VIRTUAL1553B_STATUS_DEVICE_IO_CONTROL_FAILED;
}


VIRTUAL_TERMINAL_DRIVER_API
VIRTUAL1553B_STATUS
__stdcall
VirtualBM_GetBaseCount(
    __in TERMINAL_HANDLE TerminalHandle,
    __out PUSHORT pBaseCount
    )
{
    SYNQLOCKER(hSynqVirtual1553B);

    *pBaseCount = (USHORT)(-1);

    VIRTUAL1553B_VERIFY_DRIVER_IS_OPEN(hDriverVirtual1553B);
    VIRTUAL1553B_VERIFY_TERMINAL_HANDLE(TerminalHandle);

    INPUT_BM_GET_BASE_COUNT inBuffer = { 0 };
    inBuffer.TerminalHandle = TerminalHandle;

    OUTPUT_BM_GET_BASE_COUNT outBuffer = { 0 };

    DWORD cbBytesReturned = 0;
    BOOL bResult = DeviceIoControl(
        hDriverVirtual1553B,
        IOCTL_VIRTUAL1553B_BM_GET_BASE_COUNT,
        &inBuffer, sizeof(inBuffer),
        &outBuffer, sizeof(outBuffer),
        &cbBytesReturned,
        NULL);

    if (bResult && (cbBytesReturned == sizeof(outBuffer)))
    {
        if (VIRTUAL1553B_SUCCESS(outBuffer.Status))
        {
            *pBaseCount = outBuffer.BaseCount;
        }

        return outBuffer.Status;
    }

    return VIRTUAL1553B_STATUS_DEVICE_IO_CONTROL_FAILED;
}


VIRTUAL_TERMINAL_DRIVER_API
VIRTUAL1553B_STATUS
__stdcall
VirtualBM_GetTransferRecord(
    __in TERMINAL_HANDLE TerminalHandle,
    __in USHORT TargetBase,
    __out PTRANSFER_RECORD pTransferRecord
    )
{
    SYNQLOCKER(hSynqVirtual1553B);

    VIRTUAL1553B_VERIFY_DRIVER_IS_OPEN(hDriverVirtual1553B);
    VIRTUAL1553B_VERIFY_TERMINAL_HANDLE(TerminalHandle);

    INPUT_BM_GET_TRANSFER_RECORD inBuffer = { 0 };
    inBuffer.TerminalHandle = TerminalHandle;
    inBuffer.TargetBase = TargetBase;

    OUTPUT_BM_GET_TRANSFER_RECORD outBuffer = { 0 };

    DWORD cbBytesReturned = 0;
    BOOL bResult = DeviceIoControl(
        hDriverVirtual1553B,
        IOCTL_VIRTUAL1553B_BM_GET_TRANSFER_RECORD,
        &inBuffer, sizeof(inBuffer),
        &outBuffer, sizeof(outBuffer),
        &cbBytesReturned,
        NULL);

    if (bResult && (cbBytesReturned == sizeof(outBuffer)))
    {
        if (VIRTUAL1553B_SUCCESS(outBuffer.Status))
        {
            *pTransferRecord = outBuffer.TransferRecord;
        }

        return outBuffer.Status;
    }

    return VIRTUAL1553B_STATUS_DEVICE_IO_CONTROL_FAILED;
}
