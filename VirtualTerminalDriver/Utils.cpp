
#include "stdafx.h"
#include "Utils.h"


VIRTUALTERMINALUTILS_API BYTE CharacterHexToByteDigit(CHAR chChar)
{
    return ((chChar >= 0x41) ? (BYTE)(chChar -= 0x37) : (BYTE)(chChar -= 0x30));
}


VIRTUALTERMINALUTILS_API CHAR ByteDigitToCharacterHex(BYTE cDigit)
{
    static CHAR szDigits[] = "0123456789ABCDEF";
    return szDigits[cDigit & 0x0F];
}


VIRTUALTERMINALUTILS_API WORD CharactersHexToWordDigit(LPCSTR lpBuffer)
{
    return MAKEWORD(
        (CharacterHexToByteDigit(lpBuffer[3]) & 0x0F) | ((CharacterHexToByteDigit(lpBuffer[2]) & 0x0F) << 4),
        (CharacterHexToByteDigit(lpBuffer[1]) & 0x0F) | ((CharacterHexToByteDigit(lpBuffer[0]) & 0x0F) << 4));
}


VIRTUALTERMINALUTILS_API LPSTR WordDigitToCharactersHex(WORD wData, LPSTR lpBuffer)
{
    lpBuffer[0] = ByteDigitToCharacterHex(HIBYTE(wData) >> 4);
    lpBuffer[1] = ByteDigitToCharacterHex(HIBYTE(wData));
    lpBuffer[2] = ByteDigitToCharacterHex(LOBYTE(wData) >> 4);
    lpBuffer[3] = ByteDigitToCharacterHex(LOBYTE(wData));
    lpBuffer[4] = '\0';
    return lpBuffer;
}


VIRTUALTERMINALUTILS_API void CharactersHexArrayToWordDigits(LPCSTR lpBuffer, int cbBufferLen, WORD *lpData, int *lpDataLen)
{
    int nDataLen = 0;
    *lpDataLen = 0;
    for(int i = 0; i < cbBufferLen; i += 5)
    {
        lpData[nDataLen++] = CharactersHexToWordDigit(lpBuffer + i);
    }
    *lpDataLen = nDataLen;
}


VIRTUALTERMINALUTILS_API void WordDigitsToCharactersHexArray(const WORD *lpData, int cwDataLen, LPSTR lpBuffer, int *lpBufferLen)
{
    int nBufferLen = 0;
    *lpBufferLen = 0;
    for(int i = 0; i < cwDataLen; ++i)
    {
        WordDigitToCharactersHex(lpData[i], (lpBuffer + nBufferLen));
        lpBuffer[nBufferLen + 4] = ' ';
        nBufferLen += 5;
    }

    if( !nBufferLen )
    {
        return;
    }

    lpBuffer[nBufferLen-1] = '\r';
    lpBuffer[nBufferLen++] = '\n';
    lpBuffer[nBufferLen++] = '\0';
    *lpBufferLen = nBufferLen;
}
