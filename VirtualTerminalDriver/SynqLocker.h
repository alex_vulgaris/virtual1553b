
#pragma once

#include "stdafx.h"


class SynqLocker
{

private:

    HANDLE hSynqMutex;

public:

    SynqLocker(
        __in HANDLE hMutex);

    ~SynqLocker(void);

}; // end class SynqLocker


#define SYNQLOCKER(hMutex) \
    SynqLocker __synqLocker(hMutex)
