// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the VIRTUAL_TERMINAL_DRIVER_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// VIRTUAL_TERMINAL_DRIVER_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.

#pragma once

#include "stdafx.h"
#include "..\Virtual1553B\Status.h"
#include "..\Virtual1553B\VirtualTerminalDef.h"

#ifdef VIRTUAL_TERMINAL_DRIVER_EXPORTS
#define VIRTUAL_TERMINAL_DRIVER_API __declspec(dllexport)
#else
#define VIRTUAL_TERMINAL_DRIVER_API __declspec(dllimport)
#endif


#ifdef __cplusplus
extern "C" {
#endif // __cplusplus


VIRTUAL_TERMINAL_DRIVER_API
BOOL
__stdcall
Virtual1553B_DriverOpen(
    VOID
    );

VIRTUAL_TERMINAL_DRIVER_API
BOOL
__stdcall
Virtual1553B_DriverClose(
    VOID
    );


VIRTUAL_TERMINAL_DRIVER_API
VIRTUAL1553B_STATUS
__stdcall
Virtual1553B_AllocTerminal(
    __out PTERMINAL_HANDLE pTerminalHandle
    );

VIRTUAL_TERMINAL_DRIVER_API
VIRTUAL1553B_STATUS
__stdcall
Virtual1553B_FreeTerminal(
    __in TERMINAL_HANDLE TerminalHandle
    );


VIRTUAL_TERMINAL_DRIVER_API
VIRTUAL1553B_STATUS
__stdcall
Virtual1553B_InstallEvent(
    __in TERMINAL_HANDLE TerminalHandle,
    __in HANDLE Event
    );

VIRTUAL_TERMINAL_DRIVER_API
VIRTUAL1553B_STATUS
__stdcall
Virtual1553B_RemoveEvent(
    __in TERMINAL_HANDLE TerminalHandle
    );


VIRTUAL_TERMINAL_DRIVER_API
VIRTUAL1553B_STATUS
__stdcall
Virtual1553B_GetTerminalMode(
    __in TERMINAL_HANDLE TerminalHandle,
    __out PUSHORT pTerminalMode
    );


VIRTUAL_TERMINAL_DRIVER_API
VIRTUAL1553B_STATUS
__stdcall
Virtual1553B_StartBusController(
    __in TERMINAL_HANDLE TerminalHandle
    );

VIRTUAL_TERMINAL_DRIVER_API
VIRTUAL1553B_STATUS
__stdcall
Virtual1553B_StartRemoteTerminal(
    __in TERMINAL_HANDLE TerminalHandle,
    __in USHORT Address
    );

VIRTUAL_TERMINAL_DRIVER_API
VIRTUAL1553B_STATUS
__stdcall
Virtual1553B_StartBusMonitor(
    __in TERMINAL_HANDLE TerminalHandle
    );

VIRTUAL_TERMINAL_DRIVER_API
VIRTUAL1553B_STATUS
__stdcall
Virtual1553B_StopTerminal(
    __in TERMINAL_HANDLE TerminalHandle
    );


VIRTUAL_TERMINAL_DRIVER_API
VOID
__stdcall
VirtualBC_BuildExecuteTransferBuffer(
    __out PEXECUTE_TRANSFER_BUFFER pExecuteTransferBuffer,
    __in MESSAGE_FORMAT Format,
    __in USHORT CW1,
    __in USHORT CW2,
    __in_opt CONST USHORT *pData
    );

VIRTUAL_TERMINAL_DRIVER_API
BOOL
__stdcall
VirtualBC_IsValidExecuteTransferBuffer(
    __in PEXECUTE_TRANSFER_BUFFER pExecuteTransferBuffer
    );


VIRTUAL_TERMINAL_DRIVER_API
VIRTUAL1553B_STATUS
__stdcall
VirtualBC_ExecuteTransfer(
    __in TERMINAL_HANDLE TerminalHandle,
    __in TRANSFER_LINE_NUMBER TransferLine,
    __in PEXECUTE_TRANSFER_BUFFER pExecuteTransferBuffer
    );

VIRTUAL_TERMINAL_DRIVER_API
VIRTUAL1553B_STATUS
__stdcall
VirtualBC_GetTransferResult(
    __in TERMINAL_HANDLE TerminalHandle,
    __out PBC_TRANSFER_RESULT pTransferResult
    );


VIRTUAL_TERMINAL_DRIVER_API
VIRTUAL1553B_STATUS
__stdcall
VirtualRT_SetAddress(
    __in TERMINAL_HANDLE TerminalHandle,
    __in USHORT Address
    );

VIRTUAL_TERMINAL_DRIVER_API
VIRTUAL1553B_STATUS
__stdcall
VirtualRT_GetAddress(
    __in TERMINAL_HANDLE TerminalHandle,
    __out PUSHORT pAddress
    );

VIRTUAL_TERMINAL_DRIVER_API
VIRTUAL1553B_STATUS
__stdcall
VirtualRT_WriteMemory(
    __in TERMINAL_HANDLE TerminalHandle,
    __in TRANSFER_DIRECTION Direction,
    __in USHORT Subaddress,
    __in_ecount(WordCountToWrite) CONST USHORT *pBuffer,
    __in USHORT WordCountToWrite,
    __out_opt PUSHORT pWordCountWritten
    );

VIRTUAL_TERMINAL_DRIVER_API
VIRTUAL1553B_STATUS
__stdcall
VirtualRT_ReadMemory(
    __in TERMINAL_HANDLE TerminalHandle,
    __in TRANSFER_DIRECTION Direction,
    __in USHORT Subaddress,
    __out_ecount_part(WordCountToRead, *pWordCountRead) PUSHORT pBuffer,
    __in USHORT WordCountToRead,
    __out_opt PUSHORT pWordCountRead
    );

VIRTUAL_TERMINAL_DRIVER_API
VIRTUAL1553B_STATUS
__stdcall
VirtualRT_GetTransferResult(
    __in TERMINAL_HANDLE TerminalHandle,
    __out PRT_TRANSFER_RESULT pTransferResult
    );


VIRTUAL_TERMINAL_DRIVER_API
VIRTUAL1553B_STATUS
__stdcall
VirtualBM_GetCurrentBase(
    __in TERMINAL_HANDLE TerminalHandle,
    __out PUSHORT pCurrentBase
    );

VIRTUAL_TERMINAL_DRIVER_API
VIRTUAL1553B_STATUS
__stdcall
VirtualBM_GetBaseCount(
    __in TERMINAL_HANDLE TerminalHandle,
    __out PUSHORT pBaseCount
    );

VIRTUAL_TERMINAL_DRIVER_API
VIRTUAL1553B_STATUS
__stdcall
VirtualBM_GetTransferRecord(
    __in TERMINAL_HANDLE TerminalHandle,
    __in USHORT TargetBase,
    __out PTRANSFER_RECORD pTransferRecord
    );


#ifdef __cplusplus
}
#endif // __cplusplus
