
#pragma once

#define TIMEMEASURE_EXPORTS
#ifdef TIMEMEASURE_EXPORTS
#define TIMEMEASURE_API __declspec(dllexport)
#else
#define TIMEMEASURE_API __declspec(dllimport)
#endif

#include "stdafx.h"

#define STRSAFE_NO_DEPRECATE
#include <strsafe.h>


typedef void (CALLBACK *TimeResultCallback)(
    __in double dblResult);


class TIMEMEASURE_API TimeMeasure
{

private:

    LONGLONG qwTimeStart;
    TimeResultCallback lpfnTimeResultCall;

public:

    TimeMeasure(
        __in_opt TimeResultCallback lpfnCallback = NULL)
        : qwTimeStart()
        , lpfnTimeResultCall(lpfnCallback)
    {
        QueryPerformanceCounter((LARGE_INTEGER *)(&qwTimeStart));
    }

    ~TimeMeasure(void)
    {
        LONGLONG qwTimeEnd = 0, qwFrequency = 0;
        QueryPerformanceCounter((LARGE_INTEGER *)(&qwTimeEnd));
        QueryPerformanceFrequency((LARGE_INTEGER *)(&qwFrequency));
        double dblDelta = (double)(qwTimeEnd - qwTimeStart) / qwFrequency;
        if(lpfnTimeResultCall)
        {
            lpfnTimeResultCall(dblDelta);
        }
        else
        {
            TCHAR szText[32];
            StringCbPrintf(szText, sizeof(szText), TEXT("%.6lf\n"), dblDelta);
            OutputDebugString(szText);
        }
    }

}; // end class TimeMeasure


#ifdef USE_TIME_MEASURE
#define TIME_MEASURE() TimeMeasure __timeMeasure
#define TIME_MEASURE_CALL(lpCallback) TimeMeasure __timeMeasure(lpCallback)
#else
#define TIME_MEASURE()
#define TIME_MEASURE_CALL(lpCallback)
#endif
