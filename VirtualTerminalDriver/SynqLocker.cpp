
#include "stdafx.h"
#include "SynqLocker.h"


SynqLocker::SynqLocker(HANDLE hMutex)
    : hSynqMutex(hMutex)
{
    WaitForSingleObject(hSynqMutex, INFINITE);
}


SynqLocker::~SynqLocker(void)
{
    ReleaseMutex(hSynqMutex);
}
