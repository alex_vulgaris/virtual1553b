
#pragma once

#define VIRTUALTERMINALUTILS_EXPORTS
#ifdef VIRTUALTERMINALUTILS_EXPORTS
#define VIRTUALTERMINALUTILS_API __declspec(dllexport)
#else
#define VIRTUALTERMINALUTILS_API __declspec(dllimport)
#endif

#include "stdafx.h"


/* ����������� ������ ����� 16-������ ������� ��������� � ����� */
VIRTUALTERMINALUTILS_API BYTE CharacterHexToByteDigit(
    __in CHAR chChar);

/* ����������� ����� � ������ ����� 16-������ ������� ��������� */
VIRTUALTERMINALUTILS_API CHAR ByteDigitToCharacterHex(
    __in BYTE cDigit);

/* ����������� ������ �� 4-� �������� ���� 16-������ ������� ��������� � ����� */
VIRTUALTERMINALUTILS_API WORD CharactersHexToWordDigit(
    __in LPCSTR lpBuffer);

/* ����������� ����� � ������ �� 4-� �������� ���� 16-������ ������� ���������
� ��������� ������ ����-�������� */
VIRTUALTERMINALUTILS_API LPSTR WordDigitToCharactersHex(
    __in WORD wData,
    __out LPSTR lpBuffer);

/* ����������� ������, ���������� ������ �� 4-� �������� ���� 16-������ ������� ���������,
����������� ����� ����� ���������, � ������ ����� */
VIRTUALTERMINALUTILS_API void CharactersHexArrayToWordDigits(
    __in_bcount(cbBufferLen) LPCSTR lpBuffer,
    __in int cbBufferLen,
    __out_ecount(*lpDataLen) WORD *lpData,
    __out int *lpDataLen);

/* ����������� ������ ����� � ������ �� 4-� �������� ���� 16-������ ������� ���������,
����������� ����� ����� ���������, � ��������� ������ ��������� �������� �������, �������� ������ � ����-�������� */
VIRTUALTERMINALUTILS_API void WordDigitsToCharactersHexArray(
    __in_ecount(cwDataLen) const WORD *lpData,
    __in int cwDataLen,
    __out_bcount(*lpBufferLen) LPSTR lpBuffer,
    __out int *lpBufferLen);
