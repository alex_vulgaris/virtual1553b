// dllmain.cpp : Defines the entry point for the DLL application.
#include "stdafx.h"


extern HANDLE hSynqVirtual1553B;


BOOL Virtual1553B_CreateSynq(void)
{
    HANDLE hMutex = CreateMutex(NULL, FALSE, NULL);
    if (hMutex)
    {
        hSynqVirtual1553B = hMutex;
        return TRUE;
    }
    return FALSE;
}


void Virtual1553B_CloseSynq(void)
{
    HANDLE hMutex = InterlockedExchangePointer(&hSynqVirtual1553B, NULL);
    if (hMutex)
    {
        CloseHandle(hMutex);
    }
}


BOOL APIENTRY DllMain(HMODULE hModule, DWORD dwReason, LPVOID lpReserved)
{
    switch (dwReason)
    {
    case DLL_PROCESS_ATTACH:
        return Virtual1553B_CreateSynq();

    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
        break;

    case DLL_PROCESS_DETACH:
        Virtual1553B_CloseSynq();
        break;
    }
    return TRUE;
}
