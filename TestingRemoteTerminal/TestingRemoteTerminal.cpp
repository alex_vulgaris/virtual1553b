// TestingRemoteTerminal.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "TestingRemoteTerminal.h"


HANDLE g_hInterrupt;
HANDLE g_hTerminate;
TERMINAL_HANDLE g_hTerminal = INVALID_TERMINAL_HANDLE;

DWORD g_dwDataTransferCount;
DWORD g_dwModeCommandCount;
DWORD g_dwErrorCount;


int _tmain(int argc, _TCHAR* argv[])
{
    int nExitCode = EXIT_SUCCESS;

    g_hInterrupt = CreateEvent(NULL, FALSE, FALSE, NULL);
    if (!g_hInterrupt)
    {
        _cprintf("CreateEvent() failed.\n");
        nExitCode = EXIT_FAILURE;
        goto just_exit;
    }

    g_hTerminate = CreateEvent(NULL, TRUE, FALSE, NULL);
    if (!g_hTerminate)
    {
        _cprintf("CreateEvent() failed.\n");
        nExitCode = EXIT_FAILURE;
        goto close_interrupt_event_handle;
    }

    BOOL bResult = Virtual1553B_DriverOpen();
    if (!bResult)
    {
        _cprintf("Virtual1553B_DriverOpen() failed.\n");
        nExitCode = EXIT_FAILURE;
        goto close_event_handles;
    }

    VIRTUAL1553B_STATUS status = Virtual1553B_AllocTerminal(&g_hTerminal);
    if (VIRTUAL1553B_ERROR(status))
    {
        _cprintf("Virtual1553B_AllocTerminal() failed.\n");
        nExitCode = EXIT_FAILURE;
        goto driver_close;
    }

    _cprintf("Enter the RT address -> ");
    USHORT address = INVALID_REMOTE_TERMINAL_ADDRESS;
    scanf_s("%hu", &address);
    fflush(stdin);

    status = Virtual1553B_StartRemoteTerminal(g_hTerminal, address);
    if (VIRTUAL1553B_ERROR(status))
    {
        _cprintf("Virtual1553B_StartRemoteTerminal() failed.\n");
        nExitCode = EXIT_FAILURE;
        goto free_terminal;
    }

    USHORT mode = UndefinedMode;
    status = Virtual1553B_GetTerminalMode(g_hTerminal, &mode);

    status = Virtual1553B_InstallEvent(g_hTerminal, g_hInterrupt);
    if (VIRTUAL1553B_ERROR(status))
    {
        _cprintf("Virtual1553B_InstallEvent() failed.\n");
        nExitCode = EXIT_FAILURE;
        goto stop_terminal;
    }

    USHORT data[DATA_WORD_COUNT_MAXIMUM] = {
         1,  2,  3,  4,  0,  6,  7,  8,  9,  0, 11, 12, 13, 14,  0, 16,
        17, 18, 19,  0, 21, 22, 23, 24,  0, 26, 27, 28, 29,  0, 31, 32
    };

    status = VirtualRT_WriteMemory(g_hTerminal, DirectionTransmit, SUBADDRESS_MINIMUM, data, DATA_WORD_COUNT_MAXIMUM, NULL);
    if (VIRTUAL1553B_ERROR(status))
    {
        _cprintf("VirtualRT_WriteMemory() failed.\n");
        nExitCode = EXIT_FAILURE;
        goto full_closing;
    }

    HANDLE hReceiveThread = CreateThread(NULL, 0, ReceiveThread, NULL, 0, NULL);
    if (!hReceiveThread)
    {
        _cprintf("CreateThread() failed.\n");
        nExitCode = EXIT_FAILURE;
        goto full_closing;
    }

    while (true)
    {
        _cprintf("\nPress any key to view the statistics...");
        if (_getch() == 27)
        {
            break;
        }

        _cprintf(
            "\nData transfer count = %lu\n"
            "Mode command count = %lu\n"
            "Error count = %lu\n",
            g_dwDataTransferCount, g_dwModeCommandCount, g_dwErrorCount);
    }

    SetEvent(g_hTerminate);
    WaitForSingleObject(hReceiveThread, INFINITE);
    CloseHandle(hReceiveThread);

full_closing:
    status = Virtual1553B_RemoveEvent(g_hTerminal);
    if (VIRTUAL1553B_ERROR(status))
    {
        _cprintf("Virtual1553B_RemoveEvent() failed.\n");
        nExitCode = EXIT_FAILURE;
    }

stop_terminal:
    status = Virtual1553B_StopTerminal(g_hTerminal);
    if (VIRTUAL1553B_ERROR(status))
    {
        _cprintf("Virtual1553B_StopTerminal() failed.\n");
        nExitCode = EXIT_FAILURE;
    }

free_terminal:
    status = Virtual1553B_FreeTerminal(g_hTerminal);
    if (VIRTUAL1553B_ERROR(status))
    {
        _cprintf("Virtual1553B_FreeTerminal() failed.\n");
        nExitCode = EXIT_FAILURE;
    }

driver_close:
    bResult = Virtual1553B_DriverClose();
    if (!bResult)
    {
        _cprintf("Virtual1553B_DriverClose() failed.\n");
        nExitCode = EXIT_FAILURE;
    }

close_event_handles:
    CloseHandle(g_hTerminate);

close_interrupt_event_handle:
    CloseHandle(g_hInterrupt);

just_exit:
    _cprintf("\nPress any key to exit...");
    _getch();
    return nExitCode;
}


DWORD WINAPI ReceiveThread(LPVOID lpParameter)
{
    for ( ; ; )
    {
        HANDLE hObjects[] = { g_hInterrupt, g_hTerminate };
        if (WaitForMultipleObjects((sizeof(hObjects) / sizeof(*hObjects)), hObjects, FALSE, INFINITE) != WAIT_OBJECT_0)
        {
            break;
        }

        for ( ; ; )
        {
            RT_TRANSFER_RESULT result = { 0 };
            VIRTUAL1553B_STATUS status = VirtualRT_GetTransferResult(g_hTerminal, &result);
            if (VIRTUAL1553B_ERROR(status))
            {
                break;
            }

            switch (result.Error)
            {
            case VIRTUAL1553B_ERROR_SUCCESS:
                {
                    switch (result.Format)
                    {
                    case TransferBCRT:
                    case TransferRTBC:
                    case TransferRTRT:
                    case TransferBCRTBroadcast:
                    case TransferRTRTBroadcast:
                        ++g_dwDataTransferCount;
                        break;

                    case ModeCommandWithoutData:
                    case ModeCommandWithDataTransmit:
                    case ModeCommandWithDataReceive:
                    case ModeCommandWithoutDataBroadcast:
                    case ModeCommandWithDataReceiveBroadcast:
                        ++g_dwModeCommandCount;
                        break;

                    default:
                        break;
                    }
                }
                break;

            case VIRTUAL1553B_ERROR_MESSAGE_ERROR:
                ++g_dwErrorCount;
                break;

            default:
                break;
            }
        }
    }

    return 0;
}
