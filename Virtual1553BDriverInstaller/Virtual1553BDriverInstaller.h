
#pragma once

#include "stdafx.h"
#include "resource.h"


INT_PTR CALLBACK DlgMainProc(HWND, UINT, WPARAM, LPARAM);
BOOL DlgMain_OnInitDialog(HWND, HWND, LPARAM);
void DlgMain_OnCommand(HWND, int, HWND, UINT);

void OnExit(HWND);
void OnCurrentState(HWND);
void OnInstallDriver(HWND);
void OnStartService(HWND);
void OnStopService(HWND);
void OnRemoveDriver(HWND);
void OnGetDriverPath(HWND);

BOOL GetFilePath(HWND, LPTSTR, LPCTSTR);

BOOL GetRegistryKeyRootProgram(HKEY *, DWORD *);
BOOL GetRegistryDwordValueSavePath(HKEY, DWORD *);
void SetRegistryDwordValueSavePath(HKEY, DWORD);
BOOL GetRegistryStringValueDriverPath(HKEY, LPTSTR);
void SetRegistryStringValueDriverPath(HKEY, LPCTSTR);

void ApplyRegistryValues(HWND);
void UpdateRegistryValues(HWND);
