========================================================================
    WIN32 APPLICATION : Virtual1553BDriverInstaller Project Overview
========================================================================

AppWizard has created this Virtual1553BDriverInstaller application for you.

This file contains a summary of what you will find in each of the files that
make up your Virtual1553BDriverInstaller application.


Virtual1553BDriverInstaller.vcproj
    This is the main project file for VC++ projects generated using an Application Wizard.
    It contains information about the version of Visual C++ that generated the file, and
    information about the platforms, configurations, and project features selected with the
    Application Wizard.

Virtual1553BDriverInstaller.cpp
    This is the main application source file.

/////////////////////////////////////////////////////////////////////////////
AppWizard has created the following resources:

Virtual1553BDriverInstaller.rc
    This is a listing of all of the Microsoft Windows resources that the
    program uses.  It includes the icons, bitmaps, and cursors that are stored
    in the RES subdirectory.  This file can be directly edited in Microsoft
    Visual C++.

Resource.h
    This is the standard header file, which defines new resource IDs.
    Microsoft Visual C++ reads and updates this file.

Virtual1553BDriverInstaller.ico
    This is an icon file, which is used as the application's icon (32x32).
    This icon is included by the main resource file Virtual1553BDriverInstaller.rc.

small.ico
    This is an icon file, which contains a smaller version (16x16)
    of the application's icon. This icon is included by the main resource
    file Virtual1553BDriverInstaller.rc.

/////////////////////////////////////////////////////////////////////////////
Other standard files:

StdAfx.h, StdAfx.cpp
    These files are used to build a precompiled header (PCH) file
    named Virtual1553BDriverInstaller.pch and a precompiled types file named StdAfx.obj.

/////////////////////////////////////////////////////////////////////////////
Other notes:

AppWizard uses "TODO:" comments to indicate parts of the source code you
should add to or customize.

/////////////////////////////////////////////////////////////////////////////
