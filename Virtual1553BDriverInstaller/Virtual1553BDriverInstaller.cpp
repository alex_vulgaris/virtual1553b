// Virtual1553BDriverInstaller.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "Virtual1553BDriverInstaller.h"


HICON hIconBig;
HICON hIconSmall;


int APIENTRY _tWinMain(
    __in HINSTANCE hInstance,
    __in_opt HINSTANCE hPrevInstance,
    __in_opt LPTSTR lpCmdLine,
    __in int nShowCmd
    )
{
    hIconBig = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON_BIG));
    if (!hIconBig)
    {
        return EXIT_FAILURE;
    }

    hIconSmall = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON_SMALL));
    if (!hIconSmall)
    {
        DestroyIcon(hIconBig);
        return EXIT_FAILURE;
    }

    DialogBox(hInstance,
        MAKEINTRESOURCE(IDD_MAIN),
        NULL, DlgMainProc);

    DestroyIcon(hIconBig);
    DestroyIcon(hIconSmall);

    return EXIT_SUCCESS;
}


INT_PTR CALLBACK DlgMainProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch(uMsg)
    {
        HANDLE_MSG(hWnd, WM_INITDIALOG, DlgMain_OnInitDialog);
        HANDLE_MSG(hWnd, WM_COMMAND, DlgMain_OnCommand);
    }
    return (INT_PTR)(FALSE);
}


BOOL DlgMain_OnInitDialog(HWND hWnd, HWND hWndFocus, LPARAM lParam)
{
    SendMessage(hWnd, WM_SETICON, ICON_BIG, (LPARAM)(hIconBig));
    SendMessage(hWnd, WM_SETICON, ICON_SMALL, (LPARAM)(hIconSmall));

    ApplyRegistryValues(hWnd);

    return TRUE;
}


void DlgMain_OnCommand(HWND hWnd, int id, HWND hWndCtl, UINT uCodeNotify)
{
    switch(id)
    {
    case IDOK:
    case IDCANCEL:
        OnExit(hWnd);
        break;

    case IDC_BUTTON_CURRENT_STATE:
        OnCurrentState(hWnd);
        break;

    case IDC_BUTTON_INSTALL_DRIVER:
        OnInstallDriver(hWnd);
        break;

    case IDC_BUTTON_START_SERVICE:
        OnStartService(hWnd);
        break;

    case IDC_BUTTON_STOP_SERVICE:
        OnStopService(hWnd);
        break;

    case IDC_BUTTON_REMOVE_DRIVER:
        OnRemoveDriver(hWnd);
        break;

    case IDC_BUTTON_GET_DRIVER_PATH:
        OnGetDriverPath(hWnd);
        break;
    }
}


void OnExit(HWND hWnd)
{
    UpdateRegistryValues(hWnd);

    EndDialog(hWnd, 0);
}


void OnCurrentState(HWND hWnd)
{
    SC_HANDLE hSCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
    if (!hSCManager)
    {
        SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("Unable to access the service control manager."));
        return;
    }

    TCHAR szDriverName[] = TEXT("Virtual1553B");

    SC_HANDLE hService = OpenService(hSCManager, szDriverName, SERVICE_ALL_ACCESS);

    CloseServiceHandle(hSCManager);

    DWORD dwError = ERROR_SUCCESS;
    SERVICE_STATUS serviceStatus = { 0 };

    if (hService)
    {
        if (QueryServiceStatus(hService, &serviceStatus))
        {
            switch (serviceStatus.dwCurrentState)
            {
            case SERVICE_STOPPED:
                SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("The driver service is not started."));
                break;

            case SERVICE_START_PENDING:
                SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("The driver service is starting."));
                break;

            case SERVICE_STOP_PENDING:
                SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("The driver service is stopping."));
                break;

            case SERVICE_RUNNING:
                SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("The driver service is started."));
                break;

            case SERVICE_CONTINUE_PENDING:
                SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("The driver service continue is pending."));
                break;

            case SERVICE_PAUSE_PENDING:
                SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("The driver service pause is pending."));
                break;

            case SERVICE_PAUSED:
                SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("The driver service is paused."));
                break;

            default:
                SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("The driver service is installed but it is unable to get information about its current state."));
                break;
            }
        }
        else
        {
            SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("The driver service is installed but it is unable to get information about its current state."));
        }

        CloseServiceHandle(hService);
    }
    else
    {
        dwError = GetLastError();

        switch (dwError)
        {
        case ERROR_SERVICE_DOES_NOT_EXIST:
            SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("The driver service is not installed."));
            break;

        default:
            SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("Unable to access the driver service."));
            break;
        }
    }
}


void OnInstallDriver(HWND hWnd)
{
    TCHAR szDriverPath[MAX_PATH] = { 0 };
    if (!GetDlgItemText(hWnd, IDC_EDIT_DRIVER_PATH, szDriverPath, MAX_PATH))
    {
        SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("The path to the driver file was not specified."));
        return;
    }

    SC_HANDLE hSCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
    if (!hSCManager)
    {
        SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("Unable to access the service control manager."));
        return;
    }

    TCHAR szDriverName[] = TEXT("Virtual1553B");

    SC_HANDLE hService = CreateService(
        hSCManager,
        szDriverName,
        szDriverName,
        SERVICE_ALL_ACCESS,
        SERVICE_KERNEL_DRIVER,
        SERVICE_AUTO_START,
        SERVICE_ERROR_NORMAL,
        szDriverPath,
        NULL, NULL, NULL, NULL, NULL);

    CloseServiceHandle(hSCManager);

    DWORD dwError = ERROR_SUCCESS;

    if (hService)
    {
        SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("The driver service was successfully installed."));

        CloseServiceHandle(hService);
    }
    else
    {
        dwError = GetLastError();

        switch (dwError)
        {
        case ERROR_SERVICE_MARKED_FOR_DELETE:
            SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("The driver service is already installed and marked for deletion."));
            break;

        case ERROR_SERVICE_EXISTS:
            SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("The driver service is already installed."));
            break;

        default:
            SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("Unable to install the driver service."));
            break;
        }
    }
}


void OnStartService(HWND hWnd)
{
    SC_HANDLE hSCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
    if (!hSCManager)
    {
        SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("Unable to access the service control manager."));
        return;
    }

    TCHAR szDriverName[] = TEXT("Virtual1553B");

    SC_HANDLE hService = OpenService(hSCManager, szDriverName, SERVICE_ALL_ACCESS);

    CloseServiceHandle(hSCManager);

    DWORD dwError = ERROR_SUCCESS;

    if (hService)
    {
        if (StartService(hService, 0, NULL))
        {
            SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("The driver service was successfully started."));
        }
        else
        {
            dwError = GetLastError();

            switch (dwError)
            {
            case ERROR_PATH_NOT_FOUND:
                SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("The driver service binary file cannot be found."));
                break;

            case ERROR_SERVICE_ALREADY_RUNNING:
                SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("The driver service is already started."));
                break;

            case ERROR_SERVICE_DISABLED:
                SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("The driver service is disabled."));
                break;

            default:
                SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("Unable to start the driver service."));
                break;
            }
        }

        CloseServiceHandle(hService);
    }
    else
    {
        dwError = GetLastError();

        switch (dwError)
        {
        case ERROR_SERVICE_DOES_NOT_EXIST:
            SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("The driver service is not installed."));
            break;

        default:
            SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("Unable to access the driver service."));
            break;
        }
    }
}


void OnStopService(HWND hWnd)
{
    SC_HANDLE hSCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
    if (!hSCManager)
    {
        SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("Unable to access the service control manager."));
        return;
    }

    TCHAR szDriverName[] = TEXT("Virtual1553B");

    SC_HANDLE hService = OpenService(hSCManager, szDriverName, SERVICE_ALL_ACCESS);

    CloseServiceHandle(hSCManager);

    DWORD dwError = ERROR_SUCCESS;
    SERVICE_STATUS serviceStatus = { 0 };

    if (hService)
    {
        if (ControlService(hService, SERVICE_CONTROL_STOP, &serviceStatus))
        {
            switch (serviceStatus.dwCurrentState)
            {
            case SERVICE_STOPPED:
                SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("The driver service was successfully stopped."));
                break;

            case SERVICE_STOP_PENDING:
                SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("The driver service is stopping."));
                break;

            default:
                SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("Unable to stop the driver service."));
                break;
            }
        }
        else
        {
            dwError = GetLastError();

            switch (dwError)
            {
            case ERROR_SERVICE_CANNOT_ACCEPT_CTRL:
                SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("The driver service cannot accept the requested control code."));
                break;

            case ERROR_SERVICE_NOT_ACTIVE:
                SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("The driver service is not started."));
                break;

            default:
                SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("Unable to stop the driver service."));
                break;
            }
        }

        CloseServiceHandle(hService);
    }
    else
    {
        dwError = GetLastError();

        switch (dwError)
        {
        case ERROR_SERVICE_DOES_NOT_EXIST:
            SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("The driver service is not installed."));
            break;

        default:
            SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("Unable to access the driver service."));
            break;
        }
    }
}


void OnRemoveDriver(HWND hWnd)
{
    SC_HANDLE hSCManager = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
    if (!hSCManager)
    {
        SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("Unable to access the service control manager."));
        return;
    }

    TCHAR szDriverName[] = TEXT("Virtual1553B");

    SC_HANDLE hService = OpenService(hSCManager, szDriverName, SERVICE_ALL_ACCESS);

    CloseServiceHandle(hSCManager);

    DWORD dwError = ERROR_SUCCESS;
    SERVICE_STATUS serviceStatus = { 0 };

    if (hService)
    {
        if (ControlService(hService, SERVICE_CONTROL_STOP, &serviceStatus))
        {
            switch (serviceStatus.dwCurrentState)
            {
            case SERVICE_STOPPED:
                if (DeleteService(hService))
                {
                    SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("The driver service was successfully marked for deletion."));
                }
                else
                {
                    SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("Unable to delete the driver service."));
                }
                break;

            case SERVICE_STOP_PENDING:
                SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("The driver service is stopping."));
                break;

            default:
                SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("Unable to stop the driver service."));
                break;
            }
        }
        else
        {
            dwError = GetLastError();

            switch (dwError)
            {
            case ERROR_SERVICE_CANNOT_ACCEPT_CTRL:
                SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("The driver service cannot accept the requested control code."));
                break;

            case ERROR_SERVICE_NOT_ACTIVE:
                if (DeleteService(hService))
                {
                    SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("The driver service was successfully marked for deletion."));
                }
                else
                {
                    SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("Unable to delete the driver service."));
                }
                break;

            default:
                SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("Unable to stop the driver service."));
                break;
            }
        }

        CloseServiceHandle(hService);
    }
    else
    {
        dwError = GetLastError();

        switch (dwError)
        {
        case ERROR_SERVICE_DOES_NOT_EXIST:
            SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("The driver service is not installed."));
            break;

        default:
            SetDlgItemText(hWnd, IDC_STATIC_STATE, TEXT("Unable to access the driver service."));
            break;
        }
    }
}


void OnGetDriverPath(HWND hWnd)
{
    TCHAR szDriverPath[MAX_PATH] = { 0 };
    TCHAR szFilter[] = TEXT("Driver file Virtual1553B.sys\0Virtual1553B.sys\0");

    if (GetFilePath(hWnd, szDriverPath, szFilter))
    {
        SetDlgItemText(hWnd, IDC_EDIT_DRIVER_PATH, szDriverPath);
    }
}


BOOL GetFilePath(HWND hWnd, LPTSTR lpFile, LPCTSTR lpFilter)
{
    OPENFILENAME ofn = { 0 };
    ofn.lStructSize  = sizeof(OPENFILENAME);
    ofn.hwndOwner    = hWnd;
    ofn.lpstrFilter  = lpFilter;
    ofn.lpstrFile    = lpFile;
    ofn.nMaxFile     = MAX_PATH;
    ofn.Flags        = OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST;

    return GetOpenFileName(&ofn);
}


BOOL GetRegistryKeyRootProgram(HKEY *lpKeyRoot, DWORD *lpDisposition)
{
    TCHAR szSubKey[] = TEXT("Software\\Virtual1553B Driver Installer");

    LSTATUS lStatus = RegCreateKeyEx(
        HKEY_CURRENT_USER,
        szSubKey,
        0,
        NULL,
        REG_OPTION_NON_VOLATILE,
        KEY_ALL_ACCESS,
        NULL,
        lpKeyRoot,
        lpDisposition);

    if (lStatus != ERROR_SUCCESS)
    {
        return FALSE;
    }
    return TRUE;
}


BOOL GetRegistryDwordValueSavePath(HKEY hKeyRoot, DWORD *lpSavePath)
{
    TCHAR szValueName[] = TEXT("SavePath");
    DWORD dwType = REG_DWORD;
    DWORD cbData = sizeof(DWORD);

    LSTATUS lStatus = RegQueryValueEx(
        hKeyRoot,
        szValueName,
        NULL,
        &dwType,
        (BYTE *)(lpSavePath),
        &cbData);

    if (lStatus != ERROR_SUCCESS)
    {
        return FALSE;
    }
    return TRUE;
}


void SetRegistryDwordValueSavePath(HKEY hKeyRoot, DWORD dwSavePath)
{
    TCHAR szValueName[] = TEXT("SavePath");
    DWORD dwType = REG_DWORD;
    DWORD cbData = sizeof(DWORD);

    RegSetValueEx(
        hKeyRoot,
        szValueName,
        0,
        dwType,
        (BYTE *)(&dwSavePath),
        cbData);
}


BOOL GetRegistryStringValueDriverPath(HKEY hKeyRoot, LPTSTR lpDriverPath)
{
    TCHAR szValueName[] = TEXT("DriverPath");
    DWORD dwType = REG_SZ;
    DWORD cbData = MAX_PATH * sizeof(TCHAR);

    LSTATUS lStatus = RegQueryValueEx(
        hKeyRoot,
        szValueName,
        NULL,
        &dwType,
        (BYTE *)(lpDriverPath),
        &cbData);

    if (lStatus != ERROR_SUCCESS)
    {
        return FALSE;
    }
    return TRUE;
}


void SetRegistryStringValueDriverPath(HKEY hKeyRoot, LPCTSTR lpDriverPath)
{
    TCHAR szValueName[] = TEXT("DriverPath");
    DWORD dwType = REG_SZ;
    DWORD cbData = lpDriverPath ? (_tcslen(lpDriverPath) + 1) * sizeof(TCHAR) : 0;

    RegSetValueEx(
        hKeyRoot,
        szValueName,
        0,
        dwType,
        (BYTE *)(lpDriverPath),
        cbData);
}


void ApplyRegistryValues(HWND hWnd)
{
    HKEY hKeyRoot = NULL;
    DWORD dwDisposition = 0;

    if (!GetRegistryKeyRootProgram(&hKeyRoot, &dwDisposition))
    {
        return;
    }

    DWORD dwSavePath = FALSE;

    switch (dwDisposition)
    {
    case REG_OPENED_EXISTING_KEY:
        if (GetRegistryDwordValueSavePath(hKeyRoot, &dwSavePath))
        {
            if (dwSavePath)
            {
                SendDlgItemMessage(hWnd, IDC_CHECK_SAVE_PATH, BM_SETCHECK, BST_CHECKED, 0);

                TCHAR szDriverPath[MAX_PATH] = { 0 };
                if (GetRegistryStringValueDriverPath(hKeyRoot, szDriverPath))
                {
                    SetDlgItemText(hWnd, IDC_EDIT_DRIVER_PATH, szDriverPath);
                }
                else
                {
                    SetRegistryStringValueDriverPath(hKeyRoot, NULL);
                }
            }
            else
            {
                SetRegistryStringValueDriverPath(hKeyRoot, NULL);
            }
        }
        else
        {
            SetRegistryDwordValueSavePath(hKeyRoot, FALSE);
            SetRegistryStringValueDriverPath(hKeyRoot, NULL);
        }
        break;

    default:
        SetRegistryDwordValueSavePath(hKeyRoot, FALSE);
        SetRegistryStringValueDriverPath(hKeyRoot, NULL);
        break;
    }

    RegCloseKey(hKeyRoot);
}


void UpdateRegistryValues(HWND hWnd)
{
    HKEY hKeyRoot = NULL;

    if (!GetRegistryKeyRootProgram(&hKeyRoot, NULL))
    {
        return;
    }

    if (SendDlgItemMessage(hWnd, IDC_CHECK_SAVE_PATH, BM_GETCHECK, 0, 0))
    {
        SetRegistryDwordValueSavePath(hKeyRoot, TRUE);

        TCHAR szDriverPath[MAX_PATH] = { 0 };
        GetDlgItemText(hWnd, IDC_EDIT_DRIVER_PATH, szDriverPath, MAX_PATH);

        SetRegistryStringValueDriverPath(hKeyRoot, szDriverPath);
    } 
    else
    {
        SetRegistryDwordValueSavePath(hKeyRoot, FALSE);
        SetRegistryStringValueDriverPath(hKeyRoot, NULL);
    }

    RegCloseKey(hKeyRoot);
}
