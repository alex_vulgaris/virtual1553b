//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Virtual1553BDriverInstaller.rc
//
#define IDI_ICON_BIG                    101
#define IDI_ICON_SMALL                  102
#define IDD_MAIN                        103
#define IDC_BUTTON_CURRENT_STATE        1001
#define IDC_BUTTON_INSTALL_DRIVER       1002
#define IDC_BUTTON_START_SERVICE        1003
#define IDC_BUTTON_STOP_SERVICE         1004
#define IDC_BUTTON_REMOVE_DRIVER        1005
#define IDC_STATIC_STATE                1006
#define IDC_BUTTON_GET_DRIVER_PATH      1007
#define IDC_EDIT_DRIVER_PATH            1008
#define IDC_CHECK_SAVE_PATH             1009

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        104
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1010
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
